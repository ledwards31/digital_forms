/* eslint-disable no-console */
const CACHE_NAME = "6bfda094-b06f-4a9c-89b0-333a510312fe";
const OFFLINE_URL = "index.html";
const RULES = [
  {
    method: "includes",
    condition: false,
    input: "/BRS_HANA/"
  }
]
const STRATEGY = [
  {
    method: "includes",
    condition: true,
    input: "index.html",
    strategy: "network_first"
  }
]
const CDN_URLS = [
  "https://openui5.hana.ondemand.com/resources/",
  "https://sapui5.hana.ondemand.com/resources/"
];
const RESOURCES = [
  //Index
  "/index.html",
  //Manifest
  "/manifest.json",
  //Component
  "/Component.js",
  "/Component-preload.js",
  //Controllers
  "/controller/App.controller.js",
  "/controller/BaseController.js",
  "/controller/home.controller.js",
  "/controller/list.controller.js",
  "/controller/script.controller.js",
  "/controller/pdf.controller.js",
  "/controller/notFound.controller.js",
  "/controller/offline.controller.js",
  //Common
  "/common/ActionClass.js",
  "/common/ControlDictionary.js",
  "/common/IndexedDB.js",
  "/common/Utilities.js",
  "/common/Order.js",
  "/common/fragment.definition.js",
  //DXC Library
  "/common/control/dxc/css/Signature.css",
  "/common/control/dxc/css/IconTab.css",    
  "/common/control/dxc/css/IconTabBar.css",    
  "/common/control/dxc/ui/Signature.js",
  "/common/control/dxc/ui/InputWithIcon.js",
  "/common/control/dxc/ui/IconTab.js",
  "/common/control/dxc/ui/IconTabBar.js",
  "/common/control/dxc/ui/Wizard/Wizard.js",
  "/common/control/dxc/ui/Wizard/WizardStep.js",
  "/common/control/dxc/ui/vobu/FileUploaderV4.js",
  //External Libraries
  "/common/control/vobu/ui/FileUploaderV4.js",
  //Dependencies
  "/common/dependencies/qr/qr-scanner-worker.min.js",
  "/common/dependencies/qr/qr-scanner.min.js",
  "/common/dependencies/qr/quagga.min.js",
  "/common/dependencies/qr/zxing.js",
  "/common/dependencies/uuid/uuidv4.min.js",
  "/common/dependencies/zip/jszip.js",
  //Helpers
  "/common/helper/Dynamic.js",
  "/common/helper/DialogMate.js",
  //Stylesheets
  "/css/style.css",
  "/css/theme.less",
  //Mainfest
  "/mainfest.webmanifest",
  //i18n
  "/i18n/i18n.properties",
  //"/i18n/i18n_en.properties",
  //"i18n/i18n_en_US.properties",
  //Models
  "/model/ExampleScript.json",
  "/model/models.js",
  //Resources
  //Views
  "/view/App.view.xml",
  "/view/home.view.xml",
  "/view/list.view.xml",
  "/view/script.view.xml",
  "/view/pdf.view.xml",
  "/view/notFound.view.xml",
  "/view/offline.view.xml"
 
];
for (const RESOURCE of RESOURCES) {
  let reg = /([\w\/]*\/)([\w]+)([.]*[\w]*.js)/g;
  let result = reg.exec(RESOURCE);
  if (result) {
		let directory = result[1], file = result[2], ext = result[3];
    RESOURCES.push(`${directory}${file}-dbg${ext}`);
  }
}
//SAPUI5 Endpoints
for (const URL of CDN_URLS) {
  RESOURCES.concat([
    `${URL}sap-ui-core.js`,
    `${URL}sap/ui/core/library-preload.js`,
    `${URL}sap/ui/core/themes/sap_belize_plus/library.css`,
    `${URL}sap/ui/core/themes/sap_fiori_3/library.css`,
    `${URL}sap/ui/layout/themes/sap_fiori_3/library.css`,
    `${URL}sap/m/themes/sap_fiori_3/library.css`,
    `${URL}sap/ui/core/themes/base/fonts/SAP-icons.woff2`,
    `${URL}sap/ui/core/themes/sap_fiori_3/fonts/72-Regular.woff2`,
    `${URL}sap/ui/core/themes/sap_fiori_3/fonts/72-Bold.woff2`,
    `${URL}sap/ui/core/themes/sap_fiori_3/fonts/72-Light.woff2`,
    `${URL}sap/m/library-preload.js`,
    `${URL}sap/m/themes/sap_belize_plus/library.css`,
    `${URL}sap/ui/core/themes/sap_fiori_3/library.css`,
    `${URL}ui5loader-dbg.js`,
    `${URL}sap/ui/thirdparty/jquery-dbg.js`,
    `https://cdn.jsdelivr.net/npm/less`,
    `https://code.jquery.com/jquery-1.12.4.js`,
    `https://code.jquery.com/ui/1.12.1/jquery-ui.js`
  ]);
}

self.addEventListener("install", (event) => {
  event.waitUntil(caches.open(CACHE_NAME).then(cache => {
    let jobs = [];
    RESOURCES.forEach(RESOURCE => {
      jobs.push(cache.add(RESOURCE));
    })
    return Promise.all(jobs);
  }).catch(error => {
    console.error(error);
  }));
});

self.addEventListener("activate", (event) => {
  event.waitUntil(caches.keys().then((keys) => {
    let jobs = [];
    for (let key of keys) {
      if (key !== CACHE_NAME) {
        jobs.push(caches.delete(key));
      }
    }
    return Promise.all(jobs);
  }).catch(error => {
    console.error(error);
  }));
});

function rules(request) {
  return new Promise(resolve => {
    let url = request.url;
    let response = true;
    for (let RULE of RULES) {
      response = url[RULE.method](RULE.input) === RULE.condition;
    }
    resolve(response);
  })
}

function strategy(request) {
  return new Promise(resolve => {
    let url = request.url;
    let response = true;
    for (let RULE of RULES) {
      response = url[RULE.method](RULE.input) === RULE.condition;
    }
    resolve(response);
  })
}

function respondWith(event) {
  return new Promise((resolve, reject) => {
    caches.match(event.request).then((resp) => {
      let url = event.request.url;
      let strategy = false;
      STRATEGY.forEach(element => {
        if (element.strategy === "network_first" && url[element.method](element.input) === element.condition) strategy = true;
      });
      if (navigator.onLine && strategy) {
        let copy = event.request.clone();
        return fetch(copy);
      } else if (resp) {
        resolve(resp);
      } else {
        let copy = event.request.clone();
        return fetch(copy);
      }
    }).then((resp) => {
      if (resp.type === "opaque") {
        resolve(resp);
      } else if (!resp.ok) {
        reject(resp.statusText);
      } else {
        return Promise.all([
          caches.open(CACHE_NAME),
          resp,
          rules(event.request)
        ]);
      }
    }).then((values) => {
      let cache = values[0];
      let resp = values[1];
      let result = values[2];
      if (result) cache.put(event.request, resp.clone()); 
      resolve(resp);
    }).catch(error => {
      reject(error);
    });
  });
}
self.addEventListener("fetch", (event) => {
  if (event.request.method === "GET") {
    let resp = respondWith(event).catch(async error => {
      console.error(error);
    });
    event.respondWith(resp);
  }
});
