sap.ui.define([
    "com/ven/iris/FormRenderer/common/Utilities",
    "sap/m/ButtonType",
    "sap/ui/model/json/JSONModel"
], function (
    Utilities,
    ButtonType,
    JSONModel
) {
    class DialogMate {
        constructor(oList, oController, data, multi, source) {
            this.source = source;
            this.multi = multi;
            this.list = oList;
            this.data = data;
            this.stepId = data.stepId;
            this.controller = oController;
            this.serviceUrl = data.dataServiceUrl;
            if (data.dataServiceUrl) {
                this.targets = {
                    title: data.selectTargetColumnTitle,
                    description: data.selectTargetColumnDescription,
                    id: data.selectTargetColumnID
                }
            } else {
                this.targets = {
                    title: "ValidValueDescription",
                    description: "ValidValueShortDesc",
                    id: "ValidValueShortDesc"
                }
            }
            this.pId = data.pId;
            this.entity = data.entity;
            this.insert = oController.getModel("insert");
            this.form = oController.getModel("form");
        }
        onInit() {
            this.items = this.form.getProperty(`/${this.stepId}/parameters`);
            this.hIndex = this.items.findIndex(e => e.ParameterType === "FieldParameter" && e.Counter === undefined);
            this.head = this.form.getProperty(`/${this.stepId}/parameters/${this.hIndex}`);
            this.rVals = this.items.filter(e => e.ParameterType === "FieldParameter" && e.Counter >= 0);
        }
        setSource(source) {
            this.source = source;
        }
        setList(list) {
            this.list = list;
        }
        setControl(control) {
            this.control = control;
        }
        onData(e) {
            let items = this.oDialog_SD1.getItems();
            if (this.multi) {
                for (var item of items) {
                    let id = item.data("id");
                    if (this.rVals.includes(id)) {
                        item.setSelected(true);
                    }
                }
            }
        }
        onOpenDialog() {
            let items;
            if (this.serviceUrl) {
                items = {
                    path: `script-read>${this.serviceUrl}`,
                    parameters: {
                        $$operationMode: "Server"
                    },
                    template: new sap.m.StandardListItem({
                        title: `{script-read>${this.targets.title}}`,
                        description: `{script-read>${this.targets.description}}`,
                        customData: [
                            new sap.ui.core.CustomData({
                                key: "id",
                                value: `{script-read>${this.targets.id}}`
                            }),
                            new sap.ui.core.CustomData({
                                key: "title",
                                value: `{script-read>${this.targets.title}}`
                            }),
                            new sap.ui.core.CustomData({
                                key: "description",
                                value: `{script-read>${this.targets.description}}`
                            })
                        ]
                    }),
                    events: {
                        dataReceived: (e) => this.onData(e)
                    }
                }
            } else {
                items = {
                    path: `script-read>/getValidValues(parameterId='${this.pId}',entity='${this.entity}')`,
                    parameters: {
                        $$operationMode: "Server"
                    },
                    template: new sap.m.StandardListItem({
                        title: `{script-read>ValidValueDescription}`,
                        description: `{script-read>ValidValueShortDesc}`,
                        customData: [
                            new sap.ui.core.CustomData({
                                key: "id",
                                value: `{script-read>ValidValueShortDesc}`
                            }),
                            new sap.ui.core.CustomData({
                                key: "title",
                                value: `{script-read>ValidValueDescription}`
                            }),
                            new sap.ui.core.CustomData({
                                key: "description",
                                value: `{script-read>ValidValueShortDesc}`
                            })
                        ]
                    }),
                    events: {
                        dataReceived: (e) => this.onData(e)
                    }
                }
                if (this.data.filterParameters && this.data.filterParameters.length > 0) {
                    // let filterParameters = this.data.filterParameters;
                    // let filtersArray = new Array();
                    // filterParameters.forEach(filterParam => {
                    //     let paramValue = this.controller.getParameterValue(filterParam.ParameterID);
                    //     if (filterParam && paramValue) {
                    //         var filter = new sap.ui.model.Filter({
                    //             path: filterParam.ParameterType.replace("Parameter", ""),
                    //             operator: sap.ui.model.FilterOperator.EQ,
                    //             value1: paramValue
                    //         });
                    //         filtersArray.push(filter);
                    //     }
                    // });
                    // if (filtersArray && filtersArray.length > 0) {
                    //     let filters = new sap.ui.model.Filter({
                    //         filters: filtersArray,
                    //         and: true
                    //     });
                    items.filters = this.getFilters();
                    // }
                }
            }
            if (!this.oDialog_SD1) {
                this.oDialog_SD1 = new sap.m.SelectDialog({
                    title: this.controller.i18n("SEL"),
                    multiSelect: this.multi,
                    items: items,
                    rememberSelections: true,
                    search: (e) => this.onSearch(e),
                    confirm: (e) => this.onDialogSave(e),
                    cancel: () => { },
                }).addStyleClass("dxcUiDialog");
            } else if (this.data.filterParameters && this.data.filterParameters.length > 0) {
                var itemsBinding = this.oDialog_SD1.getBinding("items");
                itemsBinding.filter(this.getFilters());
            }
            this.controller.getView().addDependent(this.oDialog_SD1);
            this.oDialog_SD1.open();
        }
        getFilters() {
            if (this.data.filterParameters && this.data.filterParameters.length > 0) {
                let filterParameters = this.data.filterParameters;
                let filtersArray = new Array();
                filterParameters.forEach(filterParam => {
                    let paramValue = this.controller.getParameterValue(filterParam.ParameterID);
                    if (filterParam && paramValue) {
                        let filterColumnName = filterParam.ParameterType.replace("Parameter", "");
                        if (this.serviceUrl) {
                            if (filterParam.TargetParameter) {
                                filterColumnName = filterParam.TargetParameter;
                            } else {
                                filterColumnName = filterParam.ParameterDesc.ParameterDesc.replace(/ /g, "");
                            }                            
                        }
                        var filter = new sap.ui.model.Filter({
                            path: filterParam.ParameterType.replace("Parameter", ""),
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: paramValue
                        });
                        filtersArray.push(filter);
                    }
                });
                if (filtersArray && filtersArray.length > 0) {
                    let filters = new sap.ui.model.Filter({
                        filters: filtersArray,
                        and: true
                    });
                    // items.filters = filters
                    return filters;
                } else {
                    return null;
                }
            }
        }
        getDiff(before, after) {
            return new Promise(resolve => {
                let res = {
                    new: [],
                    del: [],
                    same: []
                }
                after.forEach(e => {
                    let includes = before.includes(e);
                    if (!includes) {
                        res.new.push(e);
                    }
                });
                before.forEach(e => {
                    let includes = after.includes(e);
                    if (!includes) {
                        res.del.push(e);
                    } else {
                        res.same.push(e);
                    }
                });
                resolve(res);
            })
        }
        onDialogSave(oEvent) {
            let oParameters = oEvent.getParameters();
            //let oModel = this.getModel("form");
            //let data = oModel.getData();
            if (this.multi) {
                let selectedItems = oParameters.selectedItems;
                //let property = this.form.getProperty(`/${this.stepId}/parameters`);
                let insert = this.items.filter(e => e.ParameterType !== "FieldParameter");
                insert.push(this.head);
                /*let before = property.map(e => {
                    if (e.ParameterType === "FieldParameter" && e.Counter) return e.ParameterValue
                });
                let after = selectedItems.map(e =>  e.getTitle());*/
                for (var i = 0; i < selectedItems.length; i++) {
                    let item = selectedItems[i];
                    let value = item.data("id") || item.data("title") || item.data("description");
                    insert.push({
                        ParameterID: `${this.pId}_${i}`,
                        ParameterType: "FieldParameter",
                        ParameterValue: value,
                        Counter: i,
                        Version: 2,
                        displayValue: item.getTitle()
                    })
                }
                this.form.setProperty(`/${this.stepId}/parameters`, insert);
            } else {
                let item = oParameters.selectedItem;
                let value = item.data("id") || item.data("title") || item.data("description");
                this.form.setProperty(`/${this.stepId}/parameters/${this.hIndex}/ParameterValue`, value);
                if (this.source && this.source.setSelectedKey) this.source.setSelectedKey(value);
            }
            this.rVals = [];
        }
        onSearch(oEvent) {
            let value = oEvent.getParameters().value;
            let items = Object.keys(this.targets);
            let filters = [];
            for (let k of items) {
                filters.push(new sap.ui.model.Filter(this.targets[k], sap.ui.model.FilterOperator.Contains, value))
            }
            var oBinding = oEvent.getParameter("itemsBinding");
            var orFilters = new sap.ui.model.Filter({
                filters: filters,
                and: false
            });
            oBinding.filter(orFilters);
        }
        async onPressDelete(oEvent) {
            let listItem = oEvent.getParameter("listItem");
            let oBindingContext = listItem.getBindingContext("form");
            console.log(oBindingContext);
            let data = oBindingContext.getObject();
            let property = this.form.getProperty(`/${this.stepId}/parameters`);
            let field = property.findIndex(e => e.ParameterID === data.ParameterID);
            property.splice(field, 1);
            this.form.setProperty(`/${this.stepId}/parameters`, property);
        }
        formatter(data) { }
    }
    return DialogMate;
});