sap.ui.define([
    "com/ven/iris/FormRenderer/common/Utilities",
    "sap/m/ButtonType",
    "sap/ui/model/json/JSONModel",
    "com/ven/iris/FormRenderer/common/dependencies/uuid/uuidv4.min",
    "com/ven/iris/FormRenderer/common/control/dxc/ui/InputWithIcon"
], function (
    Utilities,
    ButtonType,
    JSONModel,
    UUID,
    InputWithIcon
) {
    class Dynamic {
        constructor(oTable, oController, data) {
            this.table = oTable;
            this.data = data;
            this.stepId = data.stepId;
            this.controller = oController;
            this.insert = oController.getModel("insert");
            this.form = oController.getModel("form");
            this.input = Object.freeze({
                Radio: "radio",
                ComboBox: "dropdown",
                CheckBox: "checkbox",
                Input: "input",
                QR: "qr",
                Date: "date",
                Number: "number"
            });
        }
        onInit() {
            let items = this.form.getProperty(`/${this.stepId}/parameters`);
            this.columns = items.filter(e => e.ParameterType === "FieldParameter" && (e.Counter === -1 || !e.Counter) && e.TargetParameter).sort((A, B) => A.ParameterValue - B.ParameterValue);
        }
        onChange() {
            this.insert = this.controller.getModel("insert");
            this.form = this.controller.getModel("form");
        }
        setTable(table) {
            this.table = table;
        }
        getInput(type, template) {
            if (type === this.input.Input) {
                return new sap.m.Input(template);
            } else if (type === this.input.ComboBox) {
                return new sap.m.ComboBox(template);
            } else if (type === this.input.CheckBox) {
                return new sap.m.VBox(template);
            } else if (type === this.input.Radio) {
                return new sap.m.RadioButtonGroup(template);
            } else if (type === this.input.Date) {
                return new sap.m.DatePicker(template).setDateValue(new Date());
            } else if (type === this.input.Number) {
                return new sap.m.Input(template).setType("Tel").attachLiveChange(function (oEvent) {
                    var value = oEvent.getSource().getValue();
                    var bNotnumber = isNaN(value);
                    var sNumber = "";
                    if (bNotnumber == false) sNumber = value;
                    else oEvent.getSource().setValue(sNumber);
                });
            } else if (type === this.input.QR) {
                template.endButtonPress = (e) => this.controller.getQRDialog(e);
                return new InputWithIcon(template);
            }
        }
        onSelect(oEvent, path, fgid) {
            let bSelected = oEvent.getParameters().selected;
            let model = this.controller.getModel("insert");
            let source = oEvent.getSource();
            let data = source.data("value");
            let text = source.getText();
            let value = data ? data : text;
            let controls = sap.ui.getCore().byFieldGroupId(fgid);
            if (bSelected) {
                controls.forEach(control => {
                    let type = control.getMetadata().getName();
                    if (type === "sap.m.CheckBox" && control !== source) {
                        let selected = this.controller.getSelected(path, value);
                        control.setSelected(selected);
                    }
                });
                model.setProperty(path, value);
            } else {
                model.setProperty(path, null);
            }
        }
        getItems(fields, type, template, i, path) {
            if (type === this.input.ComboBox) {
                let items = [];
                for (let field of fields) {
                    items.push(new sap.ui.core.Item({
                        key: field.content,
                        text: field.content
                    }))
                }
                template.items = items;
                template.value = `{${path.binding}}`;
            } else if (type === this.input.CheckBox) {
                let items = [];
                let fgid = uuidv4();
                for (var i = 0; i < fields.length; i++) {
                    let field = fields[i];
                    items.push(new sap.m.CheckBox({
                        text: field.content,
                        select: (e) => this.onSelect(e, path.relative, fgid),
                        fieldGroupIds: [fgid]
                    }))
                }
                template.items = items;
            } else if (type === this.input.Radio) {
                let buttons = [];
                for (let field of fields) {
                    buttons.push(new sap.m.RadioButton({
                        text: field.content,
                        selected: false
                    }))
                }
                template.buttons = buttons;
                template.selectedIndex = `{${path.binding}}`;
            }
        }
        getContent() {
            let data = this.data;
            return new Promise(resolve => {
                let model = {
                    data: data,
                    values: []
                }
                let template = {
                    content: []
                };
                for (var i = 0; i < data.columns.length; i++) {
                    let column = data.columns[i];
                    let _template = {}
                    let path = {
                        binding: `insert>/values/${i}/value`,
                        relative: `/values/${i}/value`
                    };
                    if (![this.input.Input, this.input.QR, this.input.Number, this.input.Date].includes(column.type)) {
                        this.getItems(data.fields[i], column.type, _template, i, path);
                    } else {
                        _template.value = `{${path.binding}}`;
                    }
                    var tempVal = null;
                    if (column.type === this.input.Date) {
                        function formatDate(date) {
                            var d = new Date(date),
                                month = '' + (d.getMonth() + 1),
                                day = '' + d.getDate(),
                                year = d.getFullYear();

                            if (month.length < 2)
                                month = '0' + month;
                            if (day.length < 2)
                                day = '0' + day;

                            return [year, month, day].join('-');
                        }
                        tempVal = formatDate(new Date());
                    }
                    model.values.push({
                        title: column.title,
                        value: tempVal,
                        type: column.type
                    });
                    var input = this.getInput(column.type, _template);
                    if (input) {
                        template.content.push(new sap.m.Label({
                            text: column.title
                        }))
                        template.content.push(input);
                    }
                }
                let oModel = new JSONModel(model);
                this.controller.setModel(oModel, "insert");
                this.onChange();
                resolve(new sap.ui.layout.form.SimpleForm(template));
            })
        }
        onPressInsertRow() {
            let items = this.insert.getData();
            let p = this.form.getProperty(`/${this.stepId}/parameters/`).length;
            let length = this.table.getItems().length;
            for (var i = 0; i < items.values.length; i++) {
                let item = items.values[i];
                this.form.setProperty(`/${this.stepId}/parameters/${p + i}`, {
                    ParameterID: `${this.columns[i].ParameterID}_${length}`,
                    ParameterType: "FieldParameter",
                    ParameterValue: item.value,
                    Counter: length,
                    Version: 2,
                    field: item.type
                })
            }
            this.getDynamics();
            this.controller.setModel(null, "insert");
        }
        getBindedContent(row) {
            return new Promise(resolve => {
                let data = this.data;
                let property = this.form.getProperty(`/${this.stepId}/parameters`);
                let template = {
                    content: []
                };
                for (var i = 0; i < this.columns.length; i++) {
                    let column = this.columns[i];
                    let field = property.findIndex(e => e.ParameterID === `${column.ParameterID}_${row}`);
                    let item = this.form.getProperty(`/${this.stepId}/parameters/${field}`);
                    let _template = {};
                    let path = `form>/${this.stepId}/parameters/${field}/ParameterValue`;
                    if (![this.input.Input, this.input.QR].includes(data.columns[i].type)) {
                        this.getItems(data.fields[i], data.columns[i].type, _template, i, path);
                    } else {
                        _template.value = `{${path}}`;
                    }
                    template.content.push(new sap.m.Label({
                        text: this.table.getColumns()[i].getHeader().getText()
                    }))
                    template.content.push(this.getInput(data.columns[i].type, _template));
                }
                this.onChange();
                resolve(new sap.ui.layout.form.SimpleForm(template));
            })
        }
        onPressDelete(row) {
            let property = this.form.getProperty(`/${this.stepId}/parameters`);
            for (var i = 0; i < this.columns.length; i++) {
                let column = this.columns[i];
                let field = property.findIndex(e => e.ParameterID === `${column.ParameterID}_${row}`);
                property.splice(field, 1);
            }
            this.form.setProperty(`/${this.stepId}/parameters`, property);
        }
        async onPressEditRow(row) {
            if (!this.oDialog_ER1) {
                this.oDialog_ER1 = new sap.m.Dialog({
                    title: this.controller.i18n("EditRow"),
                    content: await this.getBindedContent(row),
                    endButton: new sap.m.Button({
                        type: ButtonType.Reject,
                        text: this.controller.i18n("Delete"),
                        press: () => {
                            this.onPressDelete(row);
                            this.getDynamics();
                            this.onClose("oDialog_ER1");
                        }
                    }),
                    beginButton: new sap.m.Button({
                        type: ButtonType.Emphasized,
                        text: this.controller.i18n("Done"),
                        press: () => {
                            this.getDynamics();
                            this.onClose("oDialog_ER1");
                        }
                    })
                }).addStyleClass("dxcUiDialog");
                this.controller.getView().addDependent(this.oDialog_ER1);
            }
            this.oDialog_ER1.open();
        }
        getDynamics() {
            return new Promise(resolve => {
                let property = this.form.getProperty(`/${this.stepId}/parameters`);
                let items = [];
                let exp = /(?:[A-Za-z0-9\_\-])+_(\d)+_(\d)+/;
                for (var i = 0; i < property.length; i++) {
                    let item = property[i];
                    let result = exp.exec(item.ParameterID);
                    if (result) {
                        let row = result[1];
                        let column = result[2];
                        if (!items[column]) items[column] = [];
                        items[column][row] = new sap.m.Text({
                            text: item.ParameterValue
                        })
                    }
                }
                this.table.removeAllItems();
                for (var i = 0; i < items.length; i++) {
                    let item = items[i];
                    let itemId = i;
                    if (item) {
                        this.table.addItem(new sap.m.ColumnListItem({
                            type: "Navigation",
                            cells: item,
                            press: () => {
                                this.onPressEditRow(itemId)
                            }
                        }))
                    }
                }
                resolve(items);
            })
        }
        onClose(name) {
            this[name].close();
            this[name].destroy();
            this[name] = null;
        }
        async onPressNewRow() {
            if (!this.oDialog_NR1) {
                this.oDialog_NR1 = new sap.m.Dialog({
                    title: this.controller.i18n("InsertRow"),
                    content: await this.getContent(),
                    endButton: new sap.m.Button({
                        type: ButtonType.Emphasized,
                        text: this.controller.i18n("Insert"),
                        press: () => {
                            this.onPressInsertRow();
                            this.onClose("oDialog_NR1");
                        }
                    }),
                    beginButton: new sap.m.Button({
                        text: this.controller.i18n("Close"),
                        press: () => {
                            this.onClose("oDialog_NR1");
                        }
                    })
                }).addStyleClass("dxcUiDialog");
                this.controller.getView().addDependent(this.oDialog_NR1);
            }
            this.oDialog_NR1.open();
        }
    }
    return Dynamic;
});