sap.ui.define([
    "com/ven/iris/FormRenderer/common/Utilities",
], function (
    Utilities
) {
    class IndexedDB {
        constructor() {
            this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
            this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction ||
                this.msIDBTransaction;
            this.IDBKeyRange = window.IDBKeyRange ||
                this.webkitIDBKeyRange || window.msIDBKeyRange
            // this.stores = ["Downloads", "Pending", "Files", "__Request"];
            this.stores = [
                "Downloads", 
                "Pending", 
                "Files", 
                "Orders", 
                "Config", 
                "OrdersSAP", 
                "Request", 
                "_Request", 
                "__Request", 
                "_Attachments", 
                "_Fetch", 
                "Users", 
                "UserMeta",
                "OrdersList",
                "Batch",
                "DisplayConfig",
                "UserPreferences",
                "Details"
            ];
            if (!this.indexedDB) {
                console.error("Your browser doesn't support a stable version of IndexedDB.")
            } else {
                let request = this.indexedDB.open("IRIS", 1);
                request.onerror = (e) => {
                    console.error(e.target.error)
                }
                request.onsuccess = (e) => {
                    let db = request.result;
                }
                request.onupgradeneeded = (e) => {
                    let db = e.target.result;
                    this.stores.forEach(store => {
                        db.createObjectStore(store, {
                            keyPath: "id"
                        });
                    })
                }
            }
        }
        ReadSome(table, column, value) {
            return new Promise((resolve, reject) => {
                let request = indexedDB.open("IRIS", 1);
                let values = [];
                request.onsuccess = (e) => {
                    let db = request.result;
                    let tx = db.transaction(table).objectStore(table);
                    let cursor = tx.openCursor();
                    cursor.onsuccess = (e) => {
                        const cursor = e.target.result;
                        if (cursor) {
                            if (Array.isArray(column) && Array.isArray(value)) {
                                for (var i = 0; i < column.length; i++) {
                                    if (cursor.value[column[i]] === value[i]) {
                                        values.push(cursor.value);
                                    }
                                }
                            } else {
                                if (cursor.value[column] === value) {
                                    values.push(cursor.value);
                                }
                            }
                            cursor.continue();
                        } else {
                            resolve(values);
                        }
                    }
                    cursor.onerror = (e) => {
                        reject(e);
                    }
                }
                request.onerror = (e) => {
                    reject(e.target.error);
                }
            })
        }
        Read(table, id) {
            return new Promise((resolve, reject) => {
                let request = indexedDB.open("IRIS", 1);
                request.onsuccess = (e) => {
                    let db = request.result;
                    let tx = db.transaction(table).objectStore(table).get(id);
                    tx.onsuccess = (e) => {
                        if (request.result && e.target.result) {
                            resolve(e.target.result);
                        } else {
                            reject(`${id} does not exist in the local data store table "${table}".`);
                        }
                    }
                    tx.onerror = (e) => {
                        reject(e);
                    }
                }
                request.onerror = (e) => {
                    reject(e.target.error);
                }
            })
        }
        ReadAll(table) {
            return new Promise((resolve, reject) => {
                let request = indexedDB.open("IRIS", 1);
                request.onsuccess = (e) => {
                    let db = request.result;
                    let store = db.transaction(table).objectStore(table);
                    let values = [];
                    store.openCursor().onsuccess = (e) => {
                        var cursor = e.target.result;
                        if (cursor) {
                            values.push(cursor.value);
                            cursor.continue();
                        } else {
                            resolve(values);
                        }
                    };
                    store.openCursor().onerror = (e) => {
                        reject(e.target.error);
                    }
                }
                request.onerror = (e) => {
                    reject(e.target.error);
                }
            })
        }
        Update(table, object) {
            return new Promise((resolve, reject) => {
                let request = indexedDB.open("IRIS", 1);
                request.onsuccess = (e) => {
                    let db = request.result;
                    this._Update(table, object, db).then(() => {
                        resolve();
                    }).catch(error => {
                        reject(error);
                    })
                }
                request.onerror = (e) => {
                    reject(e.target.error);
                }
            })
        }
        _Update(table, object, db) {
            return new Promise((resolve, reject) => {
                let tx = db.transaction([table], "readwrite").objectStore(table).put(object);
                tx.onsuccess = (e) => {
                    resolve(e);
                }
                tx.onerror = (e) => {
                    reject(e.target.error);
                }
            })
        }
        Create(table, data) {
            return new Promise((resolve, reject) => {
                let request = indexedDB.open("IRIS", 1);
                request.onsuccess = (e) => {
                    let db = request.result;
                    let tx = db.transaction([table], "readwrite").objectStore(table).add(data);
                    tx.onsuccess = (e) => {
                        resolve(e);
                    }
                    tx.onerror = (e) => {
                        reject(e.target.error);
                    }
                }
                request.onerror = (e) => {
                    reject(e.target.error);
                }
            })

        }
        Delete(table, id) {
            return new Promise((resolve, reject) => {
                let request = indexedDB.open("IRIS", 1);
                request.onsuccess = (e) => {
                    let db = request.result;
                    let tx = db.transaction([table], "readwrite").objectStore(table).delete(id);
                    tx.onsuccess = (e) => {
                        resolve(e);
                    }
                    tx.onerror = (e) => {
                        reject(e.target.error);
                    }
                }
                request.onerror = (e) => {
                    reject(e.target.error);
                }
            })
        }
    }
    return IndexedDB;
});