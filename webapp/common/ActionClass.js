sap.ui.define([
    "com/ven/iris/FormRenderer/common/Utilities",
    "sap/ui/model/odata/v2/ODataModel",
    "sap/ui/model/odata/v4/ODataModel",
], function (Utilities, ODataModel, ODataModel4) {
    return {

        init: function (oController) {
            this.oController = oController;
            this.serviceURL = "BRS_HANA";
        },

        getForm: function (scriptID) {
            var scriptData = this.getWholeScript(scriptID || "BLUE100000");
            //get the Whole script and create a hierarchy of data 
            //Script Sections --> Section fields --> Firld Parameters


        },
        generatePDF: function () {
		    var me = this;
			// HTML force ZOOM:
			var scale = 'scale(1.4)';
			document.body.style.webkitTransform = scale; // Chrome, Opera, Safari
			document.body.style.msTransform = scale; // IE 9
			document.body.style.transform = scale; // General			
			// HTML Div to Canvas
            var element = document.getElementById("container-Scripting_FormRenderer---scriptNew--ContentWizard");
            //<div id="container-Scripting_FormRenderer---scriptNew--ContentWizard" 
            
            element.parentNode.style.overflow = 'visible';
			var divHeight = $('#container-Scripting_FormRenderer---scriptNew--ContentWizard').height();
			var divWidth = $('#container-Scripting_FormRenderer---scriptNew--ContentWizard').width();
			var ratio = divHeight / divWidth;
			var vQuality = 800 / divWidth ;
			domtoimage.toPng(element, {
					quality: vQuality
				})
				.then(function (dataUrl) {
					var doc = new jsPDF();
					var width = doc.internal.pageSize.width;
					var height = doc.internal.pageSize.height;
					height = ratio * width;
					doc.addImage(dataUrl, 'PNG', 1, 1, width, height);
					doc.save("FileName.pdf");
					var scaleBack = 'scale(1)';
					document.body.style.webkitTransform = scaleBack; // Chrome, Opera, Safari
					document.body.style.msTransform = scaleBack; // IE 9
					document.body.style.transform = scaleBack; // General	
				});
		

        },        
        getForms: function (filters) {
            //var scriptData = this.getWholeScript(scriptID || "BLUE100000");
            //get the Whole script and create a hierarchy of data 
            //Script Sections --> Section fields --> Firld Parameters
            var that = this;
            var promise = new Promise(function (resolve, reject) {
                var entity = "getForms()";
                var body = {
                    // "Version": 2
                };
                var paramName = "filters";
                var paramValue = filters;
                Utilities.getCFNodeJSData("https://broadspectrum-limited-brsiotpoc-neo-to-cf-poc-brs-iris-35bfe6a3.cfapps.ap10.hana.ondemand.com/combluleaderscriptingadminScripting_Admin/BRS_HANA", entity, paramName, paramValue, body, function (data) {
                    resolve(data);
                }.bind(that.oController), function (err) {
                    reject(err);
                }.bind(that.oController));
                            });
 
            return promise;

        },
        getWholeScript: function (scriptId) {
            var that = this;
            var promise = new Promise(function (resolve, reject) {
                var entity = "WholeScript";
                var body = {
                    // "Version": 2
                };
                var paramName = "scriptId";
                var paramValue = scriptId;
                Utilities.getCFNodeJSData("https://broadspectrum-limited-brsiotpoc-neo-to-cf-poc-brs-iris-35bfe6a3.cfapps.ap10.hana.ondemand.com/combluleaderscriptingadminScripting_Admin/BRS_HANA", entity, paramName, paramValue, body, function (data) {
                    resolve(data);
                }.bind(that.oController), function (err) {
                    reject(err);
                }.bind(that.oController));
            });
 
            return promise;
        },
    }

});