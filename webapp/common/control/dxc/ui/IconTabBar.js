/* eslint-disable no-console */
/*!
 * Lachlan Edwards
 * (c) Copyright 2020 DXC Technology or an DXC Technology affiliate company.
 */
sap.ui.define([
  "sap/ui/core/Control",
  "sap/m/Button",
  "sap/m/VBox",
  "sap/ui/dom/includeStylesheet",
  "sap/m/MessageToast",
  "sap/ui/core/Icon"
], function (Control, Button, VBox, includeStylesheet, MessageToast, Icon) {
	"use strict";
	return Control.extend("dxc.ui.IconTabBar", {
    properties: {
      style: null,
    },
		metadata: {
      events: {
        tabPress: {
          parameters: {
            tabKey: {
              type: "string"
            }
          }
        }
      },
      aggregations: {
				items: {
          type: "dxc.ui.IconTab",
          multiple: true,
          singularName: "item"
        }
      },
      defaultAggregation: "items",
      properties: {
        "style": {
          type: "string",
          defaultValue: "Default"
        },
      },
      library: "sap.m.IBar"
    },
		init: function () {
      includeStylesheet("./common/control/dxc/css/IconTabBar.css");
    },
		renderer: function (oRM, oControl) {
      let id = oControl.getId();
      oRM.write(`<div`);
      oRM.writeControlData(oControl);
      oRM.addClass(`dxcUiIconTabBarControl ${oControl._getStyle()}`);
      oRM.writeStyles();
      oRM.writeClasses();
      oRM.write(">");
      $.each(oControl.getItems(), (key, value) => {
        oControl._getEvent(value);
        oRM.renderControl(value);
      })
      oRM.write("</div>");
    },
    _getEvent: function(oControl) {
      oControl.attachPress((oEvent) => {
        let oParameters = oEvent.getParameters();
        let oSource = oEvent.getSource();
        let items = this.getItems();
        items.forEach(item => item.setActive(false));
        oSource.setActive(true);
        this.fireEvent("tabPress", {
          tabKey: oParameters.key
        });
      })
    },
    _getStyle: function() {
      let property = this.properties.style;
      switch (property) {
        case "Broadspectrum":
          return "dxcUiBroadspectrum";
        default:
          return "dxcUiDefault";
      }
    },
    onBeforeRendering: function() {
      this.properties.style = this.getProperty("style");
    },
    onAfterRendering: function () {
    }
	});
});
