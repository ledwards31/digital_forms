/* eslint-disable no-console */
/*!
 * Lachlan Edwards
 * (c) Copyright 2020 DXC Technology or an DXC Technology affiliate company.
 */
sap.ui.define([
  "sap/ui/core/Control",
  "sap/m/Button",
  "sap/m/VBox",
  "sap/ui/dom/includeStylesheet",
  "sap/m/MessageToast",
  "sap/ui/core/Icon"
], function (Control, Button, VBox, includeStylesheet, MessageToast, Icon) {
	"use strict";
	return Control.extend("dxc.ui.IconTab", {
    properties: {
      style: null
    },
		metadata: {
      events: {
				press: {
					parameters: {
						key: {
              type: "string"
            }
					}
				}
			},
      properties: {
        "icon": {
          type: "string",
          defaultValue: "sap-icon://folder-blank"
        },
        "label": {
          type: "string",
          defaultValue: "Tab"
        },
        "active": {
          type: "boolean",
          defaultValue: false
        },
        "style": {
          type: "string",
          defaultValue: "Default"
        },
        "key": {
          type: "string",
          defaultValue: "default"
        },
      }
    },
		init: function () {
      includeStylesheet("./common/control/dxc/css/IconTab.css");
    },
		renderer: function (oRM, oControl) {
      let icon = oControl.getProperty("icon");
      let label = oControl.getProperty("label");
      let active = oControl.getProperty("active");
      let id = oControl.getId();
      let key = oControl.getProperty("key");
      let oIcon = new Icon({
        src: icon
      });
      oRM.write(`<div`);
      oRM.writeControlData(oControl);
      oRM.addClass(`dxcUiIconTabControl ${oControl._getStyle()}`);
      if (active) oRM.addClass("active");
      oRM.writeStyles();
      oRM.writeClasses();
      oRM.write(">");
      oRM.write(`<div id='${id}_icon'`);
      oRM.writeControlData(oControl);
      oRM.addClass(`dxcUiIconTabIcon ${oControl._getStyle()}`);
      oRM.writeStyles();
      oRM.writeClasses();
      oRM.write(">");
      oRM.renderControl(oIcon);
      oRM.write("</div>");
      oRM.write(`<div id='${id}_label'`);
      oRM.writeControlData(oControl);
      oRM.addClass(`dxcUiIconTabLabel ${oControl._getStyle()}`);
      oRM.writeStyles();
      oRM.writeClasses();
      oRM.write(">");
      oRM.write(label);
      oRM.write("</div>");
      oRM.write("</div>");
      oControl.oIcon = oIcon;
    },
    _getIcon: function() {
      return this.oIcon;
    },
    _getStyle: function() {
      let property = this.properties.style;
      switch (property) {
        case "Broadspectrum":
          return "dxcUiBroadspectrum";
        default:
          return "dxcUiDefault";
      }
    },
    onclick: function() {
      this.fireEvent("press", {
				key: this.getProperty("key")
			});
    },
    setActive: function(bool) {
      this.setProperty("active", bool);
    },
    onBeforeRendering: function() {
      this.properties.style = this.getProperty("style");
    },
    onAfterRendering: function () {
    }
	});
});
