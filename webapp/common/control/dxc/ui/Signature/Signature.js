/*!
 *  ${copyright}
 */

sap.ui.define([
    "sap/ui/core/Control",
    "sap/m/Button",
    "sap/m/VBox",
    "sap/ui/dom/includeStylesheet",
    "sap/m/MessageToast",
    "sap/m/FlexItemData",
    "sap/m/FlexJustifyContent",
    "./SignatureRenderer"
], function (Control, Button, VBox, includeStylesheet, MessageToast, FlexItemData, FlexJustifyContent, SignatureRenderer) {
    var Signature = Control.extend("sap.ui.commons.Signature", {
        renderer: SignatureRenderer,
        library: "sap.ui.commons",
        metadata: {
            aggregations: {
                _button: {
                    type: "sap.m.VBox",
                    multiple: false
                }
            },
            events: {
                change: {
                    parameters: {
                        image: {
                            type: "string"
                        }
                    }
                }
            },
            properties: {
                "width": {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "270px"
                },
                "height": {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "90px"
                },
                "strokeWidth": {
                    type: "int",
                    defaultValue: 1
                },
                "strokeColor": {
                    type: "sap.ui.core.CSSColor",
                    defaultValue: "black"
                },
                "lineCap": {
                    type: "string",
                    defaultValue: "round"
                },
                "value": {
                    type: "string",
                    defaultValue: ""
                }
            }
        },
    });

    /**
     * Initializes the control
     * 
     * @override
     * @private
     */
    Signature.prototype.init = function() {
        includeStylesheet("./Signature.css");
        this.mProperties = {
            oCanvas: null,
            oContext: null,
            iX: 0,
            iY: 0,
            bLocked: false,
            inputcontrolmove: null,
            inputcontrolpress: null,
            locked: null,
            resize: null,
        };
        this.mProperties.inputcontrolpress = this.inputcontrolpress.bind(this);
        this.mProperties.inputcontrolpress = this.inputcontrolpress.bind(this);
        this.mProperties.islocked = this.islocked.bind(this);
        this.mProperties.resize = this._updateSize.bind(this);
    };
    /**
     * This utility updates the dimensions of the canvas element.
     * @private
     */
    Signature.prototype._updateSize = function() {
        let sId = this.getId();
        let oCanvas = document.getElementById(`${sId}_canvas`);
        let oImage = new Image;
        oImage.src = oCanvas.toDataURL("image/png");
        let oContainer = document.getElementById(`${sId}_container`);
        let sWidth = oContainer.clientWidth;
        let sHeight = oContainer.clientHeight;
        if (sWidth === "0" || sWidth === 0 || sWidth === "0px") {
            sWidth = "100%";
        }
        if (sHeight === "0" || sHeight === 0 || sHeight === "0px") {
            sHeight = "100%";
        }
        oCanvas.setAttribute("width", sWidth);
        oCanvas.setAttribute("height", sHeight);
        oImage.onload = () => {
            this.mProperties.oContext.drawImage(oImage, 0, 0);
        }
    }
    /**
     * This utility populates the x,y co-ordinations that represent the starting-point for a stroke.
     * This function is invoked by the mousedown/mouseeneter or touchdown/touchenter event.
     * @private
     */
    Signature.prototype.inputcontrolpress = function(oEvent) {
        let sId = this.getId();
        let oCanvas = document.getElementById(`${sId}_canvas`);
        let bTouch;
        if (oEvent.touches) {
            bTouch = oEvent.touches[0];
        }
        let clientX = (bTouch) ? bTouch.clientX : oEvent.clientX
        let clientY = (bTouch) ? bTouch.clientY : oEvent.clientY
        this.mProperties.iX = clientX - oCanvas.getBoundingClientRect().left;
        this.mProperties.iY = clientY - oCanvas.getBoundingClientRect().top;
    }
    /**
     * This utility renders a stroke between the x,y co-ordinates rapdily passed as the position of the input control moves.
     * This function is invoked by the mousemove or touchmove event.
     * @private
     */
    Signature.prototype.inputcontrolmove = function(oEvent) {
        if (!oEvent.touches && oEvent.buttons !== 1) return;
        this.mProperties.oContext.beginPath();
        this.mProperties.oContext.lineWidth = this.getProperty("strokeWidth");
        this.mProperties.oContext.lineCap = this.getProperty("lineCap");
        this.mProperties.oContext.strokeStyle = this.getProperty("strokeColor");
        this.mProperties.oContext.moveTo(this.mProperties.iX, this.mProperties.iY);
        this.inputcontrolpress(oEvent);
        this.mProperties.oContext.lineTo(this.mProperties.iX, this.mProperties.iY);
        this.mProperties.oContext.stroke();
        this.fireEvent("change", {
            image: this.onChange()
        });
    }
    /**
     * This utility displays a MessageToast to alert the client the Signature Pad is locked.
     * @private
     */
    Signature.prototype.islocked = function(oEvent) {
        MessageToast.show("Signature Input is locked.");
    }
    /**
     * This utility clears the canvas element.
     * @public
     */
    Signature.prototype.clear = function(oEvent) {
        if (!this.mProperties.islocked) {
            let sWidth = this.getProperty("width");
            let sHeight = this.getProperty("height");
            let iWidth = pWidth.substring(0, sWidth.length - 2);
            let iHeight = pHeight.substring(0, sHeight.length - 2);
            this.mProperties.ctx.clearRect(0, 0, iWidth, iHeight);
        }
    }
    /**
     * This utility re-instates the event-listeners that facilitate stroke rendering.
     * This function is invoked by "unlocking" the control.
     * @private
     */
    Signature.prototype._addEventListeners = function() {
        let oDomRef = this.getDomRef();
        /* Mouse */
        oDomRef.addEventListener("mousemove", this.mProperties.inputcontrolmove);
        oDomRef.addEventListener("mousedown", this.mProperties.inputcontrolpress);
        oDomRef.addEventListener("mouseenter", this.mProperties.inputcontrolpress);
        /* Touchscreen */
        oDomRef.addEventListener("touchmove", this.mProperties.inputcontrolmove);
        oDomRef.addEventListener("touchstart", this.mProperties.inputcontrolpress);
        oDomRef.addEventListener("touchenter", this.mProperties.inputcontrolpress);
    }
    /**
     * This utility removes the event-listeners that facilitate stroke rendering.
     * This function is invoked by "locking" the control.
     * @private
     */
    Signature.prototype._removeEventListeners = function() {
        let oDomRef = this.getDomRef();
        /* Mouse */
        oDomRef.removeEventListener("mousemove", this.mProperties.inputcontrolmove);
        oDomRef.removeEventListener("mousedown", this.mProperties.inputcontrolpress);
        oDomRef.removeEventListener("mouseenter", this.mProperties.inputcontrolpress);
        /* Touchscreen */
        oDomRef.removeEventListener("touchmove", this.mProperties.inputcontrolmove);
        oDomRef.removeEventListener("touchstart", this.mProperties.inputcontrolpress);
        oDomRef.removeEventListener("touchenter", this.mProperties.inputcontrolpress);
    }

    Signature.prototype.onBeforeRendering = function () {
        this.setAggregation("_button", new VBox({
            items: [
                new Button({
                    icon: "sap-icon://cancel",
                    type: "Unstyled",
                    layoutData: new FlexItemData({
                        growFactor: 1
                    }),
                    press: function () {
                        this.clear();
                    }.bind(this)
                }).addStyleClass(`sapUiCommonsSignatureBtn sapUiCommonsSignatureBtnBorderBottom sapUiCommonsSignature`),
                new Button({
                    icon: "sap-icon://unlocked",
                    type: "Unstyled",
                    layoutData: new FlexItemData({
                        growFactor: 1
                    }),
                    press: function (oEvent) {
                        let oSource = oEvent.getSource();
                        oSource.setIcon(this.mProperties.locked ? "sap-icon://unlocked" : "sap-icon://locked");
                        this.setLocked(!this.mProperties.locked);
                    }.bind(this)
                }).addStyleClass(`sapUiCommonsSignatureBtn sapUiCommonsSignature sapUiCommonsAnimationIdentifierPadlock`)
            ],
            justifyContent: FlexJustifyContent.Center,
            layoutData: new FlexItemData({
                growFactor: 1
            })
        }).addStyleClass(`sapUiCommonsSignatureBtnGroup sapUiCommonsSignature`));
    }

    Signature.prototype.onAfterRendering = function() {
        let sId = this.getId();
        let sValue = this.getProperty("value");
        let oCanvas = document.getElementById(`${sId}_cnvs`);
        this.mProperties.oContext = oCanvas.getContext("2d");
        this._updateSize();
        window.addEventListener("resize", this.mProperties.reside);
        this._addEventListeners();
        if (sValue.length > 0) {
            let oImage = new Image();
            oImage.src = sValue;
            setTimeout(() => {
                this.mProperties.oContext.drawImage(oImage, 0, 0);
            }, 0);
        }
    }
    /**
     * This utility accepts a Image type and sets the content displayed in the canvas.
     * @public
     */
    Signature.prototype.setImage = function(oImage) {
        this.mProperties.oContext.drawImage(oImage, 0, 0);
    }
    Signature.prototype.onChange = function() {
        let sDataUurl = this.getDataUrl();
        this.setValue(sDataUurl);
    }
    Signature.prototype.setValue = function(sDataUurl) {
        this.setProperty('value', sDataUurl, true);
    }
    Signature.prototype.getDataUrl = function() {
        let sId = this.getId();
        let oCanvas = document.getElementById(`${sId}_canvas`);
        return oCanvas.toDataURL("image/png");
    }
    Signature.prototype.getLocked = function() {
        return this.mProperties.bLocked;
    }
    Signature.prototype.setLocked = function(bLocked) {
        if (typeof bLocked === "boolean") {
            this.mProperties.bLocked = bLocked;
            bLocked ? this._removeEventListeners() : this._addEventListeners();
        };
    }
});