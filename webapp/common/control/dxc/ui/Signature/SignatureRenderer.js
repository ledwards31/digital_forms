sap.ui.define([], function () {
    var SignatureRenderer = {};
    SignatureRenderer.render = function(oRM, oControl) {
        let sWidth = oControl.getProperty("width");
        let sHeight = oControl.getProperty("height");
        let sId = oControl.getId();
        oRM.write(`<div style="width:${sWidth};height:${sHeight};"`);
        oRM.writeControlData(oControl);
        oRM.addClass(`sapUiCommonsSignatureControl ${oControl._getStyle()}`);
        oRM.writeStyles();
        oRM.writeClasses();
        oRM.write(">");
        oRM.write(`<div id='${sId}_container' class='sapUiCommonsSignaturePad'`);
        oRM.writeControlData(oControl);
        oRM.write(">");
        oRM.write(`<canvas id='${id}_canvas'`);
        oRM.writeControlData(oControl);
        oRM.writeStyles();
        oRM.write("></canvas>");
        oRM.write("</div>");
        oRM.write("<div");
        oRM.writeControlData(oControl);
        oRM.write(">");
        oRM.renderControl(oControl.getAggregation("_button"));
        oRM.write("</div>");
        oRM.write("</div>");
    }
    return SignatureRenderer;
}, true);