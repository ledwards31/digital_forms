/* eslint-disable no-console */
/*!
 * Lachlan Edwards
 * (c) Copyright 2020 DXC Technology or an DXC Technology affiliate company.
 */
sap.ui.define([
    "sap/ui/core/Control",
    "sap/m/Button",
    "sap/m/VBox",
    "sap/ui/dom/includeStylesheet",
    "sap/m/MessageToast"
], function (Control, Button, VBox, includeStylesheet, MessageToast) {
    "use strict";
    var Signature = Control.extend("dxc.ui.Signature", {
        metadata: {
            aggregations: {
                _button: {
                    type: "sap.m.VBox",
                    multiple: false
                }
            },
            events: {
                change: {
                    parameters: {
                        image: {
                            type: "string"
                        }
                    }
                }
            },
            properties: {
                "width": {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "270px"
                },
                "height": {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "90px"
                },
                "strokeWidth": {
                    type: "int",
                    defaultValue: 1
                },
                "strokeColor": {
                    type: "sap.ui.core.CSSColor",
                    defaultValue: "black"
                },
                "lineCap": {
                    type: "string",
                    defaultValue: "round"
                },
                "style": {
                    type: "string",
                    defaultValue: "Default"
                }
            }
        },
        init: function () {
            includeStylesheet("./common/control/dxc/css/Signature.css");
            this.properties = {
                canvas: null,
                ctx: null,
                flag: null,
                X: 0,
                Y: 0,
                dotFlag: false,
                locked: false,
                onPen: null,
                onPosition: null,
                onLocked: null,
                getSize: null,
                style: null,
            };
            this.properties.onPen = this._onPen.bind(this);
            this.properties.onPosition = this._onPosition.bind(this);
            this.properties.onLocked = this._onLocked.bind(this);
            this.properties.getSize = this._getSize.bind(this);
        },
        renderer: function (oRM, oControl) {
            let width = oControl.getProperty("width");
            let height = oControl.getProperty("height");
            let id = oControl.getId();
            oRM.write(`<div style="width:${width};height:${height};"`);
            oRM.writeControlData(oControl);
            oRM.addClass(`dxcUiSignatureControl ${oControl._getStyle()}`);
            oRM.writeStyles();
            oRM.writeClasses();
            oRM.write(">");
            oRM.write(`<div id='${id}_container' class='dxcUiSignaturePad'`);
            oRM.writeControlData(oControl);
            oRM.write(">");
            oRM.write(`<canvas id='${id}_cnvs'`);
            oRM.writeControlData(oControl);
            oRM.writeStyles();
            oRM.write("></canvas>");
            oRM.write("</div>");
            oRM.write("<div");
            oRM.writeControlData(oControl);
            oRM.write(">");
            oRM.renderControl(oControl.getAggregation("_button"));
            oRM.write("</div>");
            oRM.write("</div>");
        },
        _getSize: function () {
            let id = this.getId();
            let canvas = document.getElementById(`${id}_cnvs`);
            let image = new Image;
            image.src = canvas.toDataURL("image/png");
            let container = document.getElementById(`${id}_container`);
            let width = container.clientWidth;
            let height = container.clientHeight;
            if (width === "0" || width === 0 || width === "0px") {
                width = "100%";
            }
            if (height === "0" || height === 0 || height === "0px") {
                height = "100%";
            }
            canvas.setAttribute("width", width);
            canvas.setAttribute("height", height);
            image.onload = () => {
                this.properties.ctx.drawImage(image, 0, 0);
            }
        },
        _onPosition: function (oEvent) {
            let id = this.getId();
            let canvas = document.getElementById(`${id}_cnvs`);
            let touch;
            if (oEvent.touches) {
                touch = oEvent.touches[0];
            }
            let clientX = (touch) ? touch.clientX : oEvent.clientX
            let clientY = (touch) ? touch.clientY : oEvent.clientY
            this.properties.X = clientX - canvas.getBoundingClientRect().left;
            this.properties.Y = clientY - canvas.getBoundingClientRect().top;
        },
        _onPen: function (oEvent) {
            if (!oEvent.touches && oEvent.buttons !== 1) return;
            this.properties.ctx.beginPath();
            this.properties.ctx.lineWidth = this.getProperty("strokeWidth");
            this.properties.ctx.lineCap = this.getProperty("lineCap");
            this.properties.ctx.strokeStyle = this.getProperty("strokeColor");
            this.properties.ctx.moveTo(this.properties.X, this.properties.Y);
            this._onPosition(oEvent);
            this.properties.ctx.lineTo(this.properties.X, this.properties.Y);
            this.properties.ctx.stroke();
            this.fireEvent("change", {
                image: this.getDataURL()
            });
        },
        _onLocked: function (oEvent) {
            MessageToast.show("Signature Input is locked.");
        },
        _getStyle: function () {
            let property = this.properties.style;
            switch (property) {
                case "Broadspectrum":
                    return "dxcUiBroadspectrum";
                default:
                    return "dxcUiDefault";
            }
        },
        clear: function (oEvent) {
            //Will break if pWidth or pHeight don't resolve to a two character measurement (e.g. px or em);
            if (!this.properties.locked) {
                let pWidth = this.getProperty("width");
                let pHeight = this.getProperty("height");
                let nWidth = pWidth.substring(0, pWidth.length - 2);
                let nHeight = pHeight.substring(0, pHeight.length - 2);
                this.properties.ctx.clearRect(0, 0, nWidth, nHeight);
            }
        },
        _addEventListeners: function () {
            let DOMRef = this.getDomRef();
            /* Mouse */
            DOMRef.addEventListener("mousemove", this.properties.onPen);
            DOMRef.addEventListener("mousedown", this.properties.onPosition);
            DOMRef.addEventListener("mouseenter", this.properties.onPosition);
            /* Touchscreen */
            DOMRef.addEventListener("touchmove", this.properties.onPen);
            DOMRef.addEventListener("touchstart", this.properties.onPosition);
            DOMRef.addEventListener("touchenter", this.properties.onPosition);
            //DOMRef.removeEventListener("mousedown", this.properties.onLocked);
        },
        _removeEventListeners: function () {
            let DOMRef = this.getDomRef();
            /* Mouse */
            DOMRef.removeEventListener("mousemove", this.properties.onPen);
            DOMRef.removeEventListener("mousedown", this.properties.onPosition);
            DOMRef.removeEventListener("mouseenter", this.properties.onPosition);
            /* Touchscreen */
            DOMRef.removeEventListener("touchmove", this.properties.onPen);
            DOMRef.removeEventListener("touchstart", this.properties.onPosition);
            DOMRef.removeEventListener("touchenter", this.properties.onPosition);
            //DOMRef.addEventListener("mousedown", this.properties.onLocked);
        },
        onBeforeRendering: function () {
            this.properties.style = this.getProperty("style");
            this.setAggregation("_button", new VBox({
                items: [
                    new Button({
                        icon: "sap-icon://cancel",
                        type: "Unstyled",
                        layoutData: new sap.m.FlexItemData({
                            growFactor: 1
                        }),
                        press: function () {
                            this.clear();
                        }.bind(this)
                    }).addStyleClass(`dxcUiSignatureBtn dxcUiBtnBorderBottom ${this._getStyle()}`),
                    new Button({
                        icon: "sap-icon://unlocked",
                        type: "Unstyled",
                        layoutData: new sap.m.FlexItemData({
                            growFactor: 1
                        }),
                        press: function (oEvent) {
                            let oSource = oEvent.getSource();
                            oSource.setIcon(this.properties.locked ? "sap-icon://unlocked" : "sap-icon://locked");
                            this.setLocked(!this.properties.locked);
                        }.bind(this)
                    }).addStyleClass(`dxcUiSignatureBtn ${this._getStyle()} dxcUiAnimationIdentifierPadlock`)
                ],
                justifyContent: sap.m.FlexJustifyContent.Center,
                layoutData: new sap.m.FlexItemData({
                    growFactor: 1
                })
            }).addStyleClass(`dxcUiSignatureBtnGroup ${this._getStyle()}`));
        },
        onAfterRendering: function () {
            let id = this.getId();
            let canvas = document.getElementById(`${id}_cnvs`);
            this.properties.ctx = canvas.getContext("2d");
            this._getSize();
            window.addEventListener("resize", this.properties.getSize);
            this._addEventListeners();
            if (this.prefill) {
                let image = new Image();
                image.src = this.prefill;
                setTimeout(() => {
                    this.properties.ctx.drawImage(image, 0, 0);
                }, 0);
            }
        },
        getPrefill: function () {
            if (this.prefill) {
                let image = new Image();
                image.src = this.prefill;
                this.setImage(image);
            }
        },
        setImage: function (image) {
            this.properties.ctx.drawImage(image, 0, 0);
        },
        getDataURL: function () {
            let id = this.getId();
            let canvas = document.getElementById(`${id}_cnvs`);
            return canvas.toDataURL("image/png");
        },
        getLocked: function () {
            return this.properties.locked;
        },
        setLocked: function (bool) {
            if (typeof bool === "boolean") {
                this.properties.locked = bool;
                bool ? this._removeEventListeners() : this._addEventListeners();
            } else {
                console.error("Signature.setLocked method only accepts a parameter of type Boolean.");
            }
        },
        _prefill: function (image) {
            this.prefill = image;
        }
    });
    return Signature;
});
