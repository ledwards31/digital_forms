sap.ui.define([
  "sap/m/InputBase",
	"sap/ui/core/IconPool",
  "sap/m/InputBaseRenderer",
], function(InputBase, IconPool) {
  "use strict";

  return InputBase.extend("dxc.ui.InputWithIcon", {
    metadata: {
      events: {
        endButtonPress: {}
      },
      properties: {
        "icon": {
          type: "string",
          defaultValue: "sap-icon://value-help"
        }
      }
    },

    init: function() {
      InputBase.prototype.init.apply(this, arguments);
      var icon = this.addEndIcon({
        id: this.getId() + "-endBtn",
        src: this.getProperty("icon"),
        press: [this.onEndButtonPress, this],
      });
    },

    onBeforeRendering: function() {
      InputBase.prototype.onBeforeRendering.apply(this, arguments);
      var endIcons = this.getAggregation("_endIcon");
      var isEditable = this.getEditable();
      if (Array.isArray(endIcons)) {
      	//eslint-disable-next-line
        endIcons.map(icon => {
          icon.setProperty("src", this.getProperty("icon"), true)
          icon.setProperty("visible", isEditable, true)
        });
      }
    },

    onEndButtonPress: function() {
      if (this.getEnabled() && this.getEditable()) {
        this.fireEndButtonPress({});
      }
    },

    onsapshow: function(event) {
      if (!this.getEnabled() || !this.getEditable()) {
        return;
      }
      this.fireEndButtonPress({});
      event.preventDefault();
      event.stopPropagation();
    },

    renderer: "sap.m.InputBaseRenderer",
  });
});
