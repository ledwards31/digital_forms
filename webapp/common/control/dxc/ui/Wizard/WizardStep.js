sap.ui.define([
    "sap/m/WizardStep"
], function(WizardStep) {
    "use strict";
    return WizardStep.extend("dxc.ui.Wizard.WizardStep", {
        renderer: {},
        _complete: async function () {
            var oWizard = this._getWizardParent();
            this.setLast(this.bReviewStep || false);
            await this.fireComplete();
            setTimeout(() => {
                if (oWizard !== null) {
                    oWizard._handleNextButtonPress();
                }
                if (!this.bLast ) {
                    this._oNextButton.setVisible(false);
                }
            }, 0); //I'm sorry Thiago. 
        }
    });
});