sap.ui.define([
    "sap/m/Wizard"
], function(Wizard) {
    "use strict";

    return Wizard.extend("dxc.ui.Wizard.Wizard", {
        renderer: {},
        addStep: function (wizardStep) {
            this._incrementStepCount();
            return this.addAggregation("steps", wizardStep);
        }
    });
});