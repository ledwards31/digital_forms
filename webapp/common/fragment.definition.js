sap.ui.define([
    'sap/ui/commons/Button', 
    'sap/ui/core/Fragment'
], function(
    Button, 
    Fragment
) {
    "use strict";
    let module = {
        getMessage: (oController) => {
            return new Promise(resolve => {
                let control = new sap.m.MessagePopover({
                    items: {
                        path: "message>/",
                        template: new sap.m.MessagePopoverItem({
                            type: "{message>type}",
                            title: "{message>message}",
                            subtitle: "{message>additionalText}",
                            description: "{message>description}",
                        })
                    },
                    initiallyExpanded: true,
                    afterClose: ($event) => {
                        oController.onClearPress($event);
                    }
                }).addStyleClass("dxcUiMessagePopover");
                resolve(control);
            })
        },
        getMenu: (oController, tabKey) => {
            return new Promise(resolve => {
                let items = [];
                if (tabKey === "Browse") {
                    items.push(new sap.ui.unified.MenuItem({
                        text: "{i18n>CONTEXT_DOWNLOAD_TO_DEVICE}",
                        select: ($event) => {
                            oController.onContext_Download($event);
                        }
                    }));
                    items.push(new sap.ui.unified.MenuItem({
                        text: "{i18n>CONTEXT_DOWNLOAD_AS_PDF}",
                        select: ($event) => {
                            oController.onContext_PDF($event);
                        }
                    }));
                }
                if (tabKey === "Downloads") {
                    items.push(new sap.ui.unified.MenuItem({
                        text: "{i18n>CONTEXT_DELETE_FROM_DEVICE}",
                        select: ($event) => {
                            oController.onContext_Delete($event);
                        }
                    }));
                }
                if (tabKey === "Pending") {
                    items.push(new sap.ui.unified.MenuItem({
                        text: "{i18n>CONTEXT_DOWNLOAD_FILES}",
                        select: ($event) => {
                            oController.onContext_Files($event);
                        }
                    }));
                }
                if (tabKey === "Sent") {
                    items.push(new sap.ui.unified.MenuItem({
                        text: "{i18n>CONTEXT_DOWNLOAD_FILES}",
                        select: ($event) => {
                            oController.onContext_Files($event);
                        }
                    }));
                    items.push(new sap.ui.unified.MenuItem({
                        text: "{i18n>CONTEXT_DOWNLOAD_AS_PDF}",
                        select: ($event) => {
                            oController.onContext_PDF($event);
                        }
                    }));
                }
                let control = new sap.ui.unified.Menu({
                    items: items
                });
                resolve(control);
            })
        },
        getValueHelp: (oController) => {
            return new Promise(resolve => {
                let control = new sap.m.SelectDialog({
                    noDataText: "No data",
                    title: "Select",
                    search: ($event) => {
                        oController.handleInputHelpSearch($event)
                    },
                    confirm: ($event) => {
                        oController.handleInputHelpClose($event)
                    },
                    cancel: ($event) => {
                        oController.handleInputHelpClose($event)
                    },
                    items: { 
                        path: 'apiDataModel>/getContacts()',
                        template: new sap.m.StandardListItem({
                            title: "{apiDataModel>NAME_FIRST} {apiDataModel>NAME_LAST}",
                            description: "{apiDataModel>Email}",
                            type: "Active"
                        })
                    }
                }).addStyleClass("dxcUiDialog");
                resolve(control);
            })
        }
    }
    return module;
});