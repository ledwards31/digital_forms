sap.ui.define([
    "com/ven/iris/FormRenderer/common/IndexedDB",
    "com/ven/iris/FormRenderer/common/dependencies/uuid/uuidv4.min",
], function(IndexedDB, UUID) {
    /* eslint-disable no-unused-vars */
    class Order {
        constructor(controller, database) {
            this.controller = controller;
            database ? this.database = database : this.database = new IndexedDB();
            this.id = `${this.controller.urlq.order}-${this.controller.urlq.operation}`;
        }

        valid() {
            return this.controller.urlq.order && this.controller.urlq.operation;
        }

        fetch() {
            return new Promise((resolve, reject) => {
                if (this.valid()) {
                    this.database.Read("OrdersSAP", id).then(data => {
                        this.data = data;
                        resolve(true);
                    }).catch(error => {
                        reject(error);
                    })
                } else {
                    resolve(false);
                }
            })
        }

        attachment(name, description) {
            let attachment = {
                ObjectId: null,
                ObjectLine: null,
                AttachGuid: null,
                Description: null,
                FileName: name,
                MimeType: null,
                FileSize: null,
                CreatedById: null,
                CreatedByName: "You",
                CreatedAt: new Date().toLocaleDateString(),
                DocumentType: this.controller.urlq.type,
                DocumentNumber: null,
                DocumentDescription: description,
                ClassificationDate: null,
                ClassificationDescription: null,
                DocumentStatus: null,
                AuditOrder: null,
                DocumentVersion: null,
                DocumentPart: null,
            }
            this.data.OperationAttachments.push(attachment);
            return this.update();
        }
        update() {
            return new Promise((resolve, reject) => {
                this.database.Update("OrdersSAP", this.data).then(() => {
                    resolve();
                }).catch(error => {
                    reject(error);
                })
            })
        }
    }
    return Order;
});