sap.ui.define([
    "com/ven/iris/FormRenderer/common/Utilities",
    "com/ven/iris/FormRenderer/controller/BaseController",
    "sap/m/Title",
    "sap/m/Text",
    "sap/ui/richtexteditor/RichTextEditor",
    "sap/ui/richtexteditor/EditorType",
    "sap/ui/core/HTML",
    "sap/m/Label",
    "sap/m/Input",
    "sap/m/TextArea",
    "sap/m/DatePicker",
    "sap/m/DateRangeSelection",
    "sap/m/TimePicker",
    "sap/m/RadioButtonGroup",
    "sap/m/RadioButton",
    "sap/m/CheckBox",
    "sap/m/VBox",
    "sap/m/ComboBox",
    "sap/m/Select",
    "sap/ui/core/Item",
    "sap/m/MultiInput",
    "com/ven/iris/FormRenderer/common/control/dxc/ui/Signature",
    "com/ven/iris/FormRenderer/common/control/dxc/ui/InputWithIcon",
    "sap/m/MessageToast",
    "com/ven/iris/FormRenderer/common/control/vobu/ui/FileUploaderV4",
    "sap/m/Table",
    "sap/m/Column",
    "sap/m/ColumnListItem",
    "sap/m/OverflowToolbar",
    "sap/m/ToolbarSpacer",
    "sap/m/Button",
    "sap/m/Panel",
    "sap/ui/model/FilterOperator",
    "com/ven/iris/FormRenderer/common/helper/Dynamic",
    "com/ven/iris/FormRenderer/common/helper/DialogMate",
    "com/ven/iris/FormRenderer/common/dependencies/uuid/uuidv4.min",
    "com/ven/iris/FormRenderer/common/fragment.definition"
], function (
    Utilities,
    BaseController,
    Title,
    Text,
    RichTextEditor,
    EditorType,
    HTML,
    Label,
    Input,
    TextArea,
    DatePicker,
    DateRangeSelection,
    TimePicker,
    RadioButtonGroup,
    RadioButton,
    CheckBox,
    VBox,
    ComboBox,
    Select,
    Item,
    MultiInput,
    Signature,
    InputWithIcon,
    MessageToast,
    FileUploader,
    Table,
    Column,
    ColumnListItem,
    OverflowToolbar,
    ToolbarSpacer,
    Button,
    Panel,
    FilterOperator,
    Dynamic,
    DialogMate,
    UUID,
    FragmentDefinition
) {
    let _module = {
        input: Object.freeze({
            Text: "text",
            Input: "input",
            ComboBox: "dropdown",
            QR: "qr",
            CheckBox: "checkbox",
            Date: "date",
            Number: "number"
        }),
        //headingText, headingTextSize
        getHeading: function (data) {
            return new Title({
                level: Utilities.oc(data, "headingTextSize", "H2"),
                titleStyle: Utilities.oc(data, "headingTextSize", "H2"),
                text: Utilities.oc(data, "headingText", "")
            }).setLayoutData(new sap.ui.layout.GridData({
                span: "XL12 L12 M12 S12"
            }));
        },
        //bodyText, bodyTextSize
        getBody: function (data) {
            let control = [];
            // control.push(new Label());
            control.push(new Text({
                text: Utilities.oc(data, "bodyText", ""),
                fieldGroupIds: data.fieldGroupIds
            }).setLayoutData(new sap.ui.layout.GridData({
                span: "XL12 L12 M12 S12"
            })));
            return control;
        },
        //TODO
        getEditor: function (data) {
            let control = [];
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "required", false) == "true",
            }));
            control.push(new RichTextEditor({
                showGroupFont: true,
                showGroupInsert: true,
                showGroupInsert: true,
                showGroupUndo: true,
                required: Utilities.oc(data, "required", false) == "true",
                fieldGroupIds: data.fieldGroupIds
            }).addStyleClass("formRTE"));
            _module._getInstructions(data, control);
            return control;
        },
        //horLineColour
        getXLine: function (data) {
            let control = [];
            var toolbar = new sap.m.Toolbar({ height: "5px", width: "100%" }).addStyleClass("horLine");
            toolbar.onAfterRendering = function (o) {
                console.log(" toolbar.onAfterRendering  :", Utilities.oc(data, "horLineColour", "#000000"));
                toolbar.$().css("border-bottom", `5px solid ${Utilities.oc(data, "horLineColour", "#000000")}`);
            };
            toolbar.$().css("border-bottom", `5px solid ${Utilities.oc(data, "horLineColour", "#000000")}`);
            control.push(toolbar);
            return control;
        },
        //blankSize
        getSpacer: function (data) {
            let control = [];
            var toolbar = new sap.m.Toolbar({ height: `${Utilities.oc(data, "blankSize", "10")}px`, width: "100%" }).addStyleClass("horSpace");
            control.push(toolbar);
            return control;
        },
        _getServiceInput: function (data, path, keyCol, textCol, icon) {
            let control = [];
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "required", false) == "true"
            }).addStyleClass("formLabel"))
            // if (!icon) {
            let tempControl = new Input({
                placeholder: Utilities.oc(data, "placeholder", ""),
                maxLength: parseInt(Utilities.oc(data, "maxLength", 0), 10),
                required: Utilities.oc(data, "required", false) == "true",
                value: Utilities.oc(data, "value", ""),
                valueLiveUpdate: true,
                showSuggestion: true,
                enableSuggestionsHighlighting: true,
                showValueHelp: true,
                liveChange: (e) => this.handleInputHelpClear(e),
                valueHelpRequest: $.proxy(function (oEvt) {
                    this.onInputHelpPress(oEvt);
                }, this),
                suggestionItems: {
                    path: `script-read>/${path}`,
                    parameters: {
                        $$operationMode: "Server",
                    },
                    template: new sap.ui.core.ListItem({
                        additionalText: `{script-read>${keyCol}}`,
                        text: `{script-read>${textCol}}`
                    })
                },
                fieldGroupIds: data.fieldGroupIds,
                width: "100%"
            }).addStyleClass("formInput formLocation");
            if (icon) {
                tempControl.addStyleClass(icon);
            }
            control.push(tempControl);
            // } else {
            //     control.push(new InputWithIcon({
            //         placeholder: Utilities.oc(data, "placeholder", ""),
            //         maxLength: parseInt(Utilities.oc(data, "maxLength", 0), 10),
            //         required: Utilities.oc(data, "required", false) == "true",
            //         value: Utilities.oc(data, "value", ""),
            //         icon: icon,
            //         valueLiveUpdate: true,
            //         showSuggestion: true,
            //         enableSuggestionsHighlighting: true,
            //         showValueHelp: true,
            //         valueHelpRequest: $.proxy(function (oEvt) {
            //             this.onInputHelpPress(oEvt);
            //         }, this),
            //         suggestionItems: {
            //             path: `script-read>/${path}`,
            //             parameters: {
            //                 $$operationMode: "Server",
            //             },
            //             template: new sap.ui.core.ListItem({
            //                 additionalText: `{script-read>${keyCol}}`,
            //                 text: `{script-read>${textCol}}`
            //             })
            //         },
            //         fieldGroupIds: data.fieldGroupIds,
            //         width: "50%"
            //     }).addStyleClass("formInput formLocation"));
            // }
            _module._getInstructions(data, control);
            return control;
        },
        onInputHelpPress: function (oEvt, qr) {
            console.log("Starting onInputHelpPress in ControlDictionary", oEvt.getSource().getBindingInfo("suggestionItems"));
            this.currentlyFocusedInput = oEvt.getSource();
            var currentBindingInfo = this.currentlyFocusedInput.getBindingInfo("suggestionItems");
            var listItemMap = new Map();
            listItemMap.set("asset", { keyCol: "{script-read>EquipmentNumber}", textCol: "{script-read>EquipmentDescription}" });
            listItemMap.set("floc", { keyCol: "{script-read>FunctionalLocationID}", textCol: "{script-read>FunctionalLocationDescription}" });
            listItemMap.set("contact", { keyCol: "{script-read>Email} {script-read>CRMPARTNERBP}", textCol: "{script-read>NAME_FIRST} {script-read>NAME_LAST}" });
            var listMapItem;
            if (currentBindingInfo.path.toLowerCase().indexOf("asset") >= 0) {
                listMapItem = listItemMap.get("asset");
            } else if (currentBindingInfo.path.toLowerCase().indexOf("floc") >= 0) {
                listMapItem = listItemMap.get("floc");
            } else {
                listMapItem = listItemMap.get("contact");
            }
            var sInputValue = oEvt.getSource().getValue();
            var that = this.oController;
            var filters = this._getCFAfilters(sInputValue, currentBindingInfo.path.toLowerCase()); //[new sap.ui.model.Filter("SEARCH", FilterOperator.Contains, sInputValue)];
            filters.push(new sap.ui.model.Filter("BusinessEntity", sap.ui.model.FilterOperator.EQ, this.oController.getView().getModel("script").getData().Script.Entity));
            if (!this.oController._oValueHelpDialog) {
                FragmentDefinition.getValueHelp(this).then(function (control) {
                    var that = this.oController;
                    that._oValueHelpDialog = control;
                    that._oValueHelpDialog.attachConfirm($.proxy(function (oEvt) { this.handleInputHelpClose(oEvt); }, this));
                    that._oValueHelpDialog.attachCancel($.proxy(function (oEvt) { this.handleInputHelpClose(oEvt); }, this));
                    that._oValueHelpDialog.attachLiveChange($.proxy(function (oEvt) { this.handleInputHelpSearch(oEvt); }, this));
                    that._oValueHelpDialog.attachSearch($.proxy(function (oEvt) { this.handleInputHelpSearch(oEvt); }, this));
                    var oItemTemplate = new sap.m.StandardListItem({
                        adaptTitleSize: false,
                        title: listMapItem.textCol, //"{apiDataModel>" + + "}"
                        description: listMapItem.keyCol, //"{apiDataModel>" ++ "}"
                    });
                    that._oValueHelpDialog.bindAggregation('items', {
                        path: "script-read>" + currentBindingInfo.path,
                        parameters: {
                            $$operationMode: 'Server',
                            // $filter: 'Category eq \'Electronics\'',
                            // $select: 'Category,EmployeeId,ID,Name'
                        },
                        // sorters: [new sap.ui.model.Sorter("ChangesTime", true)],
                        template: oItemTemplate
                    });
                    that.getView().addDependent(that._oValueHelpDialog);
                    // Create a filter for the binding
                    that._oValueHelpDialog.getBinding("items")
                        .filter(filters);
                    // Open ValueHelpDialog filtered by the input's value
                    that._oValueHelpDialog.open(sInputValue);
                    that._oValueHelpDialog._oSubHeader.insertContentMiddle(new Button({
                        icon: "sap-icon://bar-code",
                        press: (oEvent) => {
                            _module.oController.getQRDialog(oEvent, that._oValueHelpDialog._oSearchField);
                        }
                    }), 1);
                }.bind(this));
            } else {
                var oItemTemplate = new sap.m.StandardListItem({
                    adaptTitleSize: false,
                    title: listMapItem.textCol, //"{apiDataModel>" + + "}"
                    description: listMapItem.keyCol, //"{apiDataModel>" ++ "}"
                });
                that._oValueHelpDialog.bindAggregation('items', {
                    path: "script-read>" + currentBindingInfo.path,
                    parameters: {
                        $$operationMode: 'Server',
                        // $filter: 'Category eq \'Electronics\'',
                        // $select: 'Category,EmployeeId,ID,Name'
                    },
                    // sorters: [new sap.ui.model.Sorter("ChangesTime", true)],
                    template: oItemTemplate
                });
                // Create a filter for the binding
                that._oValueHelpDialog.getBinding("items")
                    .filter(filters);
                // Open ValueHelpDialog filtered by the input's value
                that._oValueHelpDialog.open(sInputValue);
            }
        },
        handleInputHelpSearch: function (oEvent) {
            var sValue = oEvent.getParameter("value");
            var oFilter = new sap.ui.model.Filter("SEARCH", sap.ui.model.FilterOperator.Contains, sValue);
            var currentBindingInfo = this.currentlyFocusedInput.getBindingInfo("suggestionItems");
            var filters = this._getCFAfilters(sValue, currentBindingInfo.path.toLowerCase());
            filters.push(new sap.ui.model.Filter("BusinessEntity", sap.ui.model.FilterOperator.EQ, this.oController.getView().getModel("script").getData().Script.Entity));
            oEvent.getSource().getBinding("items").filter(filters);
        },
        handleInputHelpClear: function (oEvent) {
            let source = oEvent.getSource();
            let value = source.getValue();
            if (value.length <= 0) {
                this.oController.selectedCFAMap.set(source.getBindingInfo("suggestionItems").path.toLowerCase(), {});
            }
        },
        handleInputHelpClose: async function (oEvent) {
            var oSelectedItem = oEvent.getParameter("selectedItem");
            var that = this.oController;
            var selectedObject = await oSelectedItem.getBindingContext("script-read").requestObject(); //oSelectedItem.getBindingContext("apiDataModel").getObject();
            // oEvent.getSource().getBinding("items").filter([]);
            if (!that.selectedCFAMap) {
                that.selectedCFAMap = new Map();
            }
            that.selectedCFAMap.set(this.currentlyFocusedInput.getBindingInfo("suggestionItems").path.toLowerCase(), selectedObject);
            console.log("Selected in handleInputHelpClose", selectedObject);
            if (!oSelectedItem) {
                this.currentlyFocusedInput = null;
                return;
            } else {
                this.currentlyFocusedInput.setValue(oSelectedItem.getTitle());
                this.currentlyFocusedInput = null;
            }
            // this.byId("productInput").setValue(oSelectedItem.getTitle());
        },
        _getCFAfilters: function (sInputValue, currentEntity) {
            console.log("Starting _getCFAfilters");
            var that = this.oController;
            let filters = [new sap.ui.model.Filter("SEARCH", sap.ui.model.FilterOperator.Contains, sInputValue)];
            if (that.selectedCFAMap) {
                var configGroup = that.getView().getModel("script").getData().ConFloAssConfig.ConfigurationGroup;
                let selectedContact = [...that.selectedCFAMap].filter(([k, v]) => k.indexOf("contact") >= 0);
                let selectedAsset = [...that.selectedCFAMap].filter(([k, v]) => k.indexOf("asset") >= 0);
                let selectedFloc = [...that.selectedCFAMap].filter(([k, v]) => k.indexOf("floc") >= 0);
                if (selectedFloc) {
                    selectedFloc = selectedFloc[0];
                }
                if (selectedAsset) {
                    selectedAsset = selectedAsset[0];
                }
                if (selectedContact) {
                    selectedContact = selectedContact[0];
                }
                switch (configGroup) {
                    case "1":
                        if (currentEntity.indexOf("floc") >= 0) {
                            if (selectedContact && selectedContact.length > 1 && selectedContact[1].Partner) {
                                // filters.push(new sap.ui.model.Filter({
                                //     path: "CONTACTPERSON",
                                //     operator: sap.ui.model.FilterOperator.EQ,
                                //     value1: selectedContact[1].Partner
                                // }));
                                filters.push(new sap.ui.model.Filter("CONTACTPERSON", sap.ui.model.FilterOperator.EQ, selectedContact[1].Partner));
                            }
                            if (selectedAsset && selectedAsset.length > 1 && selectedAsset[1].EquipmentFunctionalLocationID_Ext) {
                                // filters.push(new sap.ui.model.Filter({
                                //     path: "FunctionalLocationLabel",
                                //     operator: sap.ui.model.FilterOperator.EQ,
                                //     value1: selectedAsset[1].EquipmentFunctionalLocationID_Ext
                                // }));
                                filters.push(new sap.ui.model.Filter("FunctionalLocationLabel", sap.ui.model.FilterOperator.EQ, selectedAsset[1].EquipmentFunctionalLocationID_Ext));
                            }
                            // filters.push(new sap.ui.model.Filter({
                            //     path: "ClassType",
                            //     operator: sap.ui.model.FilterOperator.EQ,
                            //     value1: '003'
                            // }));
                            filters.push(new sap.ui.model.Filter("ClassType", sap.ui.model.FilterOperator.EQ, '003F'));
                            // filters.push(new sap.ui.model.Filter({
                            //     path: "IS_DWEL",
                            //     operator: sap.ui.model.FilterOperator.EQ,
                            //     value1: 0
                            // }));
                            filters.push(new sap.ui.model.Filter("IS_DWEL", sap.ui.model.FilterOperator.EQ, 0));
                        } else if (currentEntity.indexOf("asset") >= 0) {
                            if (selectedFloc && selectedFloc.length > 1 && selectedFloc[1].FunctionalLocationLabel) {
                                // filters.push(new sap.ui.model.Filter({
                                //     path: "EquipmentFunctionalLocationID_Ext",
                                //     operator: sap.ui.model.FilterOperator.Contains,
                                //     value1: selectedFloc.FunctionalLocationLabel
                                // }));
                                filters.push(new sap.ui.model.Filter("EquipmentFunctionalLocationID_Ext", sap.ui.model.FilterOperator.EQ, selectedFloc[1].FunctionalLocationLabel));
                            }
                        } else if (currentEntity.indexOf("contact") >= 0) {
                            if (selectedFloc && selectedFloc.length > 1 && selectedFloc[1].CONTACTPERSON) {
                                // filters.push(new sap.ui.model.Filter({
                                //     path: "Partner",
                                //     operator: sap.ui.model.FilterOperator.Contains,
                                //     value1: selectedFloc.CONTACTPERSON
                                // }));
                                filters.push(new sap.ui.model.Filter("Partner", sap.ui.model.FilterOperator.EQ, selectedFloc[1].CONTACTPERSON));
                            }
                        }
                        break;
                    case "2":
                        if (currentEntity.indexOf("floc") >= 0) {
                            if (selectedAsset && selectedAsset.length > 1 && selectedAsset[1].EquipmentFunctionalLocationID_Ext) {
                                // filters.push(new sap.ui.model.Filter({
                                //     path: "FunctionalLocationLabel",
                                //     operator: sap.ui.model.FilterOperator.EQ,
                                //     value1: selectedAsset[1].EquipmentFunctionalLocationID_Ext
                                // }));
                                filters.push(new sap.ui.model.Filter("FunctionalLocationLabel", sap.ui.model.FilterOperator.EQ, selectedAsset[1].EquipmentFunctionalLocationID_Ext));
                            }
                        } else if (currentEntity.indexOf("asset") >= 0) {
                            if (selectedFloc && selectedFloc.length > 1 && selectedFloc[1].FunctionalLocationLabel) {
                                // filters.push(new sap.ui.model.Filter({
                                //     path: "EquipmentFunctionalLocationID_Ext",
                                //     operator: sap.ui.model.FilterOperator.Contains,
                                //     value1: selectedFloc.FunctionalLocationLabel
                                // }));
                                filters.push(new sap.ui.model.Filter("EquipmentFunctionalLocationID_Ext", sap.ui.model.FilterOperator.EQ, selectedFloc[1].FunctionalLocationLabel));
                            }
                        } else if (currentEntity.indexOf("contact") >= 0) {

                        }
                        break;
                    default:
                        break;
                }
            }
            return filters;
        },
        /* POOR EXECUTION */
        _getInstructions: function (data, control, unshift, fwidth) {
            if (data.instructions) {
                let label = control[0];
                let items = control.slice(1);
                items[unshift ? 'unshift' : 'push'](new Text({
                    text: Utilities.oc(data, "instructions", ""),
                }).addStyleClass("dxcUiInputInstruction sapUiTinyMarginTop"))
                while (control.length > 1) {
                    control.pop();
                }
                // control.push(new VBox({
                //     items: items
                // }));
                /** .setLayoutData(new sap.ui.layout.GridData({
                    span: "XL12 L12 M12 S12"
                })) **/
                var vbox = new VBox({
                    items: items
                });
                if (fwidth) {
                    vbox.setLayoutData(new sap.ui.layout.GridData({
                        span: "XL12 L12 M12 S12"
                    }));
                }
                control.push(vbox);
            }
        },

        //shortMandatory, shortCharLimit
        _getInput: function (data, InputType) {
            let control = [];
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "required", false) == "true"
            }).addStyleClass("formLabel"))
            var n = new Input({
                //placeholder: data?.placeholder ?? "",
                //maxLength: data?.maxLength ?? 0,
                //required: data?.required ?? false,
                placeholder: Utilities.oc(data, "placeholder", ""),
                maxLength: parseInt(Utilities.oc(data, "maxLength", 0), 10),
                required: Utilities.oc(data, "required", false) == "true",
                type: InputType,
                value: Utilities.oc(data, "value", ""),
                valueLiveUpdate: true,
                width: "100%",
                // liveChange: function (oEvent) {
                //     var value = oEvent.getSource().getValue();
                //     var bNotnumber = isNaN(value);
                //     var sNumber = "";
                //     if (bNotnumber == false) sNumber = value;
                //     else oEvent.getSource().setValue(sNumber);
                // },
                fieldGroupIds: data.fieldGroupIds
            }).addStyleClass("formInput");
            if (InputType === sap.m.InputType.Tel) {
                n.attachLiveChange(function (oEvent) {
                    var value = oEvent.getSource().getValue();
                    value = value.trim();
                    var bNotnumber = isNaN(value);
                    var sNumber = "";
                    if (bNotnumber == false) {
                        sNumber = value;
                    } else {
                        var reg = /([0-9]+)/g;
                        // sNumber = value.match(reg).join('');
                        if (value.match(reg)) {
                            sNumber = value.match(reg).join('');
                        } else {
                            sNumber = "";
                        }
                    }
                    oEvent.getSource().setValue(sNumber);
                });
            }
            control.push(n);
            // control.push(new Input({
            //     //placeholder: data?.placeholder ?? "",
            //     //maxLength: data?.maxLength ?? 0,
            //     //required: data?.required ?? false,
            //     placeholder: Utilities.oc(data, "placeholder", ""),
            //     maxLength: parseInt(Utilities.oc(data, "maxLength", 0), 10),
            //     required: Utilities.oc(data, "required", false) == "true",
            //     type: InputType,
            //     value: Utilities.oc(data, "value", ""),
            //     valueLiveUpdate: true,
            //     width: "100%",
            //     fieldGroupIds: data.fieldGroupIds
            // }).addStyleClass("formInput"))
            _module._getInstructions(data, control);
            return control;
        },
        getInput: function (data) {
            data.required = data.shortMandatory || data.mandatory;
            data.maxLength = data.shortCharLimit;
            return _module._getInput(data, sap.m.InputType.Text)
        },
        //longMandatory, longCharLimit
        getTextArea: function (data) {
            let control = [];
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "longMandatory", false) == "true"
            }).addStyleClass("formLabel"))
            control.push(new TextArea({
                placeholder: Utilities.oc(data, "placeholder", ""),
                maxLength: parseInt(Utilities.oc(data, "longCharLimit", 0)),
                value: Utilities.oc(data, "value", ""),
                fieldGroupIds: data.fieldGroupIds,
                width: "100%"
            }).addStyleClass("formTextArea"))
            _module._getInstructions(data, control);
            return control;
        },
        getEmail: function (data) {
            data.maxLength = data.emailCharLimit;
            data.required = data.emailMandatory || data.mandatory;
            return _module._getInput(data, sap.m.InputType.Email)
        },
        getNumber: function (data) {
            data.maxLength = data.numberCharLimit;
            data.required = data.numberMandatory || data.mandatory;
            return _module._getInput(data, sap.m.InputType.Tel); //sap.m.InputType.Number)
        },
        /* REQ. SUPPORT FOR DATE-RANGE */

        //dateLabel?, datePlaceholder, dateInstructions, dateCharLimit?, dateMandatory
        getDatePicker: function (data) {
            let control = [];
            // data.instructions = data.dateInstructions;
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "dateMandatory", false) == "true"
            }).addStyleClass("formLabel"))
            let template = {
                valueFormat: Utilities.oc(data, "format", "dd/MM/yyyy"), //data?.format ?? "dd/MM/yyyy",
                placeholder: Utilities.oc(data, "placeholder", ""), //data?.placeholder ?? "",datePlaceholder
                required: Utilities.oc(data, "dateMandatory", false) == "true", //data?.required ?? false
                fieldGroupIds: data.fieldGroupIds,
                width: "100%",
                change: (e) => this.oController.getModel("form").setProperty(`${data.path}/ParameterValue`, e.getParameter("value"))
            }
            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                return [day, month, year].join('/');
            }
            if (data.style === "range") {
                let setting = {
                    displayFormat: Utilities.oc(data, "format", "dd/MM/yyyy"), //data?.format ?? "dd/MM/yyyy",
                    dateValue: Utilities.oc(data, "value.dateValue", new Date().toLocaleDateString('en-AU')),
                    secondDateValue: Utilities.oc(data, "value.secondDateValue", ""),
                }
                Object.assign(setting, template);
                control.push(new DateRangeSelection(setting).addStyleClass("formDateRangeSelection"))
            } else {
                let setting = {
                    displayFormat: Utilities.oc(data, "style", "dd/MM/yyyy"), //data?.style ?? "dd/MM/yyyy",
                    value: Utilities.oc(data, "value", ""),
                }
                Object.assign(setting, template);
                let datePicker = new DatePicker(setting).addStyleClass("formDatePicker");
                datePicker.setDateValue(new Date());
                var x = data.path;
                x = x + "/ParameterValue";
                this.oController.getModel("form").setProperty(x, formatDate(new Date()));
                control.push(datePicker);
            }
            _module._getInstructions(data, control);
            return control;
        },
        getTimeFormat(format) {
            switch (format) {
                case "12":
                    return {
                        support2400: false,
                        displayFormat: "hh:mm a",
                        valueFormat: "hh:mm a"
                    }
                case "24":
                    return {
                        support2400: true,
                        displayFormat: "HH:mm",
                        valueFormat: "HH:mm"
                    }
                default:
                    return {
                        support2400: false,
                        displayFormat: "hh:mm a",
                        valueFormat: "hh:mm a"
                    }
            }
        },
        /* REQ. SUPPORT FOR TIME-RANGE */
        //timeLabel?, timePlaceholder, timeInstructions, timeCharLimit?, timeMandatory
        getTimePicker: function (data) {
            let control = [];
            // data.instructions = data.timeInstructions;
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "timeMandatory", false) == "true"
            }))
            let format = _module.getTimeFormat(data.format);
            let template = {
                displayFormat: format.displayFormat,
                support2400: format.support2400,
                valueFormat: format.valueFormat,
                placeholder: Utilities.oc(data, "placeholder", ""), //data?.placeholder ?? "",
                required: Utilities.oc(data, "timeMandatory", false) == "true", //data?.required ?? false,
                fieldGroupIds: data.fieldGroupIds,
                maskMode: "Off",
                change: (e) => {
                    console.log(e, e.getParameters(), e.getParameter("newValue"), this.oController.getModel("form"), `${data.path}/ParameterValue`);
                    this.oController.getModel("form").setProperty(`${data.path}/ParameterValue`, e.getParameter("newValue"))
                }
            }
            function formatTime(date, support2400) {
                let hour = date.getHours();
                let minute = date.getMinutes();
                let ampm = "";
                if (!support2400) {
                    ampm = hour >= 12 ? " PM" : " AM";
                    hour = hour % 12;
                    hour = hour ? hour : 12;
                }
                minute = minute < 10 ? `0${minute}` : minute;
                return `${hour}:${minute}${ampm}`;
            }
            if (data.style === "range") {
                control.push(new HBox({
                    items: [
                        new TimePicker(Object.assign({
                            value: Utilities.oc(data, "value.timeValue", ""),
                            width: "100%",
                        }, template)),
                        new TimePicker(Object.assign({
                            value: Utilities.oc(data, "value.secondTimeValue", ""),
                            width: "100%",
                        }, template))
                    ]
                }));
            } else {
                let timePicker = new TimePicker(Object.assign({
                    value: Utilities.oc(data, "value", ""),
                    width: "100%",
                }, template)).setValue(formatTime(new Date(), format.support2400));
                var x = data.path;
                x = x + "/ParameterValue";
                this.oController.getModel("form").setProperty(x, formatTime(new Date(), format.support2400));
                control.push(timePicker);
            }
            _module._getInstructions(data, control);
            return control;
        },
        getContent: function (data) {
            let control = [];
            // control.push(new Label());
            control.push(new HTML({
                content: Utilities.oc(data, "userContent", ""),
                fieldGroupIds: data.fieldGroupIds
            }).setLayoutData(new sap.ui.layout.GridData({
                span: "XL12 L12 M12 S12"
            })));
            return control;
        },
        /* REQ: CONFIRM PARAMETER NAME */
        //radioMandatory
        getRadio: function (data) {
            let control = [];
            let path = `${data.path}/ParameterValue`;
            let model = this.oController.getModel("form");
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "radioMandatory", false) == "true"
            }));
            let template = {
                fieldGroupIds: data.fieldGroupIds,
                select: (oEvent) => {
                    this.oController.onRadio(oEvent, path)
                }
            };
            if (data.options.length > 1) {
                template.buttons = [];
                let dfault;
                let selectedIndex = -1;
                for (var i = 0; i < data.options.length; i++) {
                    let option = data.options[i];
                    let selected = data.path ? this.oController.getSelected(path, option.content) : false;
                    if (selected || (selectedIndex === -1 && data.defaultValue === option.content)) {
                        selectedIndex = i;
                        dfault = option.content;
                    }
                    template.buttons.push(new RadioButton({
                        text: option.content,
                    }))
                }
                if (!dfault && selectedIndex === -1) {
                    template.selectedIndex = 0;
                    template.buttons[0].setSelected(true);
                    model.setProperty(path, data.options[0].content);
                } else {
                    template.selectedIndex = selectedIndex;
                    template.buttons[selectedIndex].setSelected(true);
                    model.setProperty(path, dfault);
                }
            } else if (data.dataUrl && data.targetColumn) {
                template.buttons = {
                    path: `script-read>${data.dataUrl}`,
                    template: new RadioButton({
                        text: `{script-read>${data.targetColumn}}`,
                        selected: false //Utilities.oc(data, "value", false),

                    })
                }
            } else {
                template.buttons = {
                    path: `script-read>/getValidValues(parameterId='${data.pId}',entity='${data.entity}')`,
                    template: new RadioButton({
                        text: `{script-read>ValidValueDescription}`,
                        selected: false //Utilities.oc(data, "value", false)
                    }).addCustomData(new sap.ui.core.CustomData({
                        key: "value",
                        value: `{script-read>ValidValueShortDesc}`
                    }))
                }
            }
            console.log(template.buttons);
            control.push(new RadioButtonGroup(template));
            _module._getInstructions(data, control, true);
            return control;
        },
        /* REQ: CONFIRM PARAMETER NAME */
        //checkboxLabel?, checkboxInstructions, checkboxMandatory
        getCheckbox: function (data) {
            let control = [];
            let path = Utilities.oc(data, "value", "");
            let _path = `${data.path}/ParameterValue`;
            let model = this.oController.getModel("form");
            // data.instructions = data.checkboxInstructions;
            let template = {
                fieldGroupIds: data.fieldGroupIds
            };
            if (data.options.length >= 1) {
                template.items = [];
                for (var i = 0; i < data.options.length; i++) {
                    let option = data.options[i];
                    data.path = "/" + data.stepId + "/parameters/" + i;
                    _path = data.path + "/ParameterValue";
                    if (data.defaultValue && data.defaultValue === option.content) {
                        model.setProperty(_path, true);
                        //let arr = data.defaultValue.split(",");
                        //if (arr.indexOf(option.content) > -1) model.setProperty(_path, true);
                    }
                    template.items.push(new CheckBox({
                        text: option.content,
                        selected: "{form>" + data.path + "/ParameterValue}",
                        customData: [
                            new sap.ui.core.CustomData({
                                key: "path",
                                value: _path
                            }),
                            new sap.ui.core.CustomData({
                                key: "stepId",
                                value: data.stepId
                            }),
                            new sap.ui.core.CustomData({
                                key: "required",
                                value: Utilities.oc(data, "checkboxMandatory", false) == "true"
                            })
                        ]
                    }))
                }
            } else if (data.dataUrl && data.targetColumn) {
                template.items = {
                    path: `script-read>${data.dataUrl}`,
                    template: new CheckBox({
                        text: `{script-read>${data.targetColumn}}`,
                        selected: false, //Utilities.oc(data, "value", false) /*ATTN*/
                        select: ($event) => {
                            this.oController.onSelect($event, path, data.stepId)
                        },
                        customData: [
                            new sap.ui.core.CustomData({
                                key: "path",
                                value: _path
                            }),
                            new sap.ui.core.CustomData({
                                key: "stepId",
                                value: data.stepId
                            }),
                            new sap.ui.core.CustomData({
                                key: "required",
                                value: Utilities.oc(data, "checkboxMandatory", false) == "true"
                            })
                        ]
                    })
                }
            } else {
                // aData.push(new sap.ui.core.CustomData({
                //     key: "value",
                //     value: `{script-read>ValidValueShortDesc}`
                // }));
                template.items = {
                    path: `script-read>/getValidValues(parameterId='${data.pId}',entity='${data.entity}')`,
                    template: new CheckBox({
                        text: `{script-read>ValidValueDescription}`,
                        selected: false, //Utilities.oc(data, "value", false) /*ATTN*/
                        select: ($event) => {
                            this.oController.onSelect($event, path, data.stepId)
                        },
                        customData: [
                            new sap.ui.core.CustomData({
                                key: "path",
                                value: _path
                            }),
                            new sap.ui.core.CustomData({
                                key: "stepId",
                                value: data.stepId
                            }),
                            new sap.ui.core.CustomData({
                                key: "required",
                                value: Utilities.oc(data, "checkboxMandatory", false) == "true"
                            })
                        ]
                    })
                }
            }
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "checkboxMandatory", false) == "true"
            }));
            control.push(new VBox(template));
            _module._getInstructions(data, control, true);
            return control;
        },
        //dropdownLabel?, dropdownInstructions, dataServiceUrl, dropdownTarget, dropdownMandatory
        getComboBox: async function (data) {
            let control = [];
            let bind = data.value ? data.value.path : "";
            // data.instructions = data.dropdownInstructions;
            let template = {
                placeholder: Utilities.oc(data, "placeholder", ""),
                required: Utilities.oc(data, "dropdownMandatory", false) == "true",
                change: (oEvent) => {
                    let source = oEvent.getSource();
                    let selectedItem = oEvent.getParameter("selectedItem");
                    source.unbindProperty("selectedKey", false);
                    let re = /(\w*)>([A-Za-z0-9\/\-\_]*)/g;
                    let results = re.exec(bind);
                    let model = results[1];
                    let path = results[2];
                    if (model && path) {
                        let form = this.oController.getModel(model);
                        let value = selectedItem.getKey() ? selectedItem.getKey() : selectedItem.getText();
                        form.setProperty(path, value);
                        source.setSelectedKey(value);
                    }
                },
                selectedKey: Utilities.oc(data, "value", ""),
                fieldGroupIds: data.fieldGroupIds,
                showSecondaryValues: true,
                forceSelection: false,
                width: "100%",
            };
            if (data.options.length > 0) {
                template.items = [];
                for (var i = 0; i < data.options.length; i++) {
                    let option = data.options[i];
                    template.items.push(new Item({
                        key: option.content,
                        text: option.content
                    }));
                    if (data.defaultValue && option.content === data.defaultValue) template.selectedKey = option.content;
                }
            } else if (data.dataServiceUrl && data.dropdownTarget) {
                let model = await this.oController.lazyOData(data.dataServiceUrl).catch(error => console.error(error));
                template.items = {
                    path: `${model}>/`,
                    template: new Item({
                        text: `{${model}>${data.dropdownTarget}}`,
                        key: `{${model}>${data.dropdownTarget}}`
                    })
                }
                if (data.dsDefaultValue) template.selectedKey = data.dsDefaultValue;
            } else {
                template.items = {
                    path: `script-read>/getValidValues(parameterId='${data.pId}',entity='${data.entity}')`,
                    template: new sap.ui.core.ListItem({
                        text: `{script-read>ValidValueDescription}`,
                        key: `{script-read>ValidValueShortDesc}`,
                        additionalText: `{script-read>ValidValueShortDesc}`
                    }).addCustomData(new sap.ui.core.CustomData({
                        key: "value",
                        value: `{script-read>ValidValueShortDesc}`
                    }))
                }
                if (data.dsDefaultValue) template.selectedKey = data.dsDefaultValue;
            }
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "dropdownMandatory", false) == "true"
            }));
            control.push(new Select(template));
            _module._getInstructions(data, control);
            return control;
        },
        //signatureMandatory
        getSignature: function (data) {
            let control = [];
            let prefill = _module.oController.getModel("form").getProperty(`${data.path}/Notes`);
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "signatureMandatory", false) == "true"
            }))
            let _control = new Signature({
                style: "Broadspectrum",
                fieldGroupIds: data.fieldGroupIds,
                change: ($event) => {
                    this.oController.onSign($event, data.stepId)
                }
            });
            control.push(_control);
            if (prefill) _control._prefill(prefill);
            _module._getInstructions(data, control);
            return control;
        },
        //multiLabel?, multiInstructions, multiLimit?, multiMandatory, multiFileType
        getUpload: function (data) {
            let control = [];
            data.instructions = data.multiInstructions;
            let cid = `U${uuidv4()}`;
            let uploadUrl = `${this.oController.getOwnerComponent().getModel("script-write").sServiceUrl}ScriptAttachments`;
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "multiMandatory", false) == "true"
            }))
            control.push(new FileUploader({
                id: this.oController.createId(cid),
                multiple: Utilities.oc(data, "multiple", false), //REPLACE W. LIMIT
                useMultipart: false,
                fileType: Utilities.oc(data, "multiFileType", []),
                placeholder: Utilities.oc(data, "placeholder", ""),
                value: Utilities.oc(data, "value", ""),
                fieldGroupIds: data.fieldGroupIds,
                uploadUrl: uploadUrl,
            }));
            this.oController.newFile(cid, {
                controlId: cid,
                description: Utilities.oc(data, "question", "")
            });
            _module._getInstructions(data, control);
            return control;
        },
        //qrMandatory
        getQR: function (data) {
            let control = [];
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "qrMandatory", false) == "true"
            }))
            control.push(new InputWithIcon({
                placeholder: Utilities.oc(data, "placeholder", ""),
                required: Utilities.oc(data, "qrMandatory", false) == "true",
                icon: "sap-icon://bar-code",
                value: Utilities.oc(data, "value", ""),
                endButtonPress: (oEvent) => {
                    _module.oController.getQRDialog(oEvent);
                },
                fieldGroupIds: data.fieldGroupIds,
                width: "100%"
            }).addStyleClass("formInput formQr"))
            _module._getInstructions(data, control);
            return control;
        },
        //multiLimit?, multiMandatory, multiDataServiceURL, multiTargetColumn
        getMultiInput: function (data) {
            let control = [];
            let template = {
                value: Utilities.oc(data, "value", ""),
                required: Utilities.oc(data, "multiMandatory", "") == "true",
                placeholder: Utilities.oc(data, "placeholder", ""),
                fieldGroupIds: data.fieldGroupIds
            };
            if (data.options) {
                template.suggestionItems = [];
                for (var i = 0; i < data.options.length; i++) {
                    let option = data.options[i];
                    template.suggestionItems.push(new Item({
                        text: option
                    }))
                }
            } else if (data.multiDataServiceURL && data.multiTargetColumn) {
                template.suggestionItems = {
                    path: `script-read>${data.multiDataServiceURL}`,
                    template: new CheckBox({
                        text: `{script-read>${data.multiTargetColumn}}`
                    })
                }
            } else {
                return;
            }
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "required", false) == "true"
            }));
            control.push(new MultiInput(template));
            _module._getInstructions(data, control);
            return control;
        },
        getLocation: function (data) {
            data.required = data.locationMandatory;
            let path = "getFlocs()";
            let keyCol = "FunctionalLocationID" //To-do: Alternative Labelling of Functional Locations.
            let textCol = "FunctionalLocationDescription"
            return _module._getServiceInput(data, path, keyCol, textCol, "map");
        },
        getAsset: function (data) {
            data.required = data.assetMandatory;
            let path = "getAssets()";
            let keyCol = "EquipmentNumber"
            let textCol = "EquipmentDescription"
            return _module._getServiceInput(data, path, keyCol, textCol, "wrench");
        },
        getContact: function (data) {
            data.required = data.contactMandatory;
            let path = "getContacts()";
            let keyCol = "CRMPARTNERBP"
            let textCol = "NAME_FIRST"
            return _module._getServiceInput(data, path, keyCol, textCol, "person-placeholder");
        },
        popValidValues: function (pId, entity, template) {
            return {
                path: `script-read>/getValidValues(parameterId='${pId}',entity='${entity}')`,
                template: template
            }
        },
        getSelectDialog: function (data) {
            let control = [];
            control.push(new Label({
                text: Utilities.oc(data, "question", ""),
                required: Utilities.oc(data, "selectMandatory", false) == "true"
            }))
            if (data.selectMulti) {
                let dm = new DialogMate(null, this.oController, data, true);
                dm.onInit();
                let template = {
                    expandable: true,
                    expanded: true,
                    width: "auto",
                    backgroundDesign: "Solid",
                    headerToolbar: new OverflowToolbar({
                        content: [
                            new sap.m.Title({
                                text: this.oController.i18n("SELCTS")
                            }),
                            new ToolbarSpacer(),
                            new sap.m.Button({
                                icon: "sap-icon://value-help",
                                press: () => dm.onOpenDialog()
                            })
                        ]
                    }),
                    content: []
                }
                template.content = new sap.m.List({
                    mode: "Delete",
                    delete: (e) => dm.onPressDelete(e),
                    items: {
                        path: `form>/${data.stepId}/parameters`,
                        formatter: (d) => dm.formatter(d),
                        template: new sap.m.StandardListItem({
                            title: {
                                parts: [
                                    "form>displayValue",
                                    "form>ParameterValue"
                                ],
                                formatter: (displayValue, ParameterValue) => {
                                    let response = displayValue;
                                    if (ParameterValue) response += ` (${ParameterValue})`;
                                    return response;
                                }
                            },
                            visible: "{= ${form>Counter} >= 0 }"
                        })
                    }
                });
                dm.setList(template.content);
                control.push(new Panel(template)
                    .addStyleClass("dxcUiPanel")
                );
            } else {
                let template;
                let bind = data.value ? data.value.path : "";
                let dm = new DialogMate(null, this.oController, data, false);
                dm.onInit();
                if (data.selectTargetColumnID) {
                    template = new sap.ui.core.ListItem({
                        key: `{script-read>${data.selectTargetColumnID}}`,
                        text: `{script-read>${data.selectTargetColumnTitle}}`,
                        additionalText: `{script-read>${data.selectTargetColumnDescription}}`
                    })
                } else if (!data.dataServiceUrl) {
                    template = new sap.ui.core.ListItem({
                        text: `{script-read>ValidValueDescription}`,
                        key: `{script-read>ValidValueShortDesc}`,
                    })
                } else {
                    template = new sap.ui.core.ListItem({
                        key: `{script-read>${data.targetColumn}}`,
                        text: `{script-read>${data.targetColumn}}`
                    })
                }
                let oInput = new sap.m.Input({
                    showValueHelp: true,
                    valueHelpRequest: () => dm.onOpenDialog(),
                    liveChange: (oEvent) => {
                        let source = oEvent.getSource();
                        let key = source.getSelectedKey();
                        let value = source.getValue();
                        source.unbindValue();
                        let re = /(\w*)>([A-Za-z0-9\/\-\_]*)/g;
                        console.log(bind);
                        let results = re.exec(bind);
                        let model = results[1];
                        let path = results[2];
                        if (model && path) {
                            let form = this.oController.getModel(model);
                            let input_value = key ? key : value;
                            form.setProperty(path, input_value);
                            source.setValue(input_value);
                        }
                    },
                    value: Utilities.oc(data, "value", ""),
                    required: Utilities.oc(data, "selectMandatory", "") == "true",
                    placeholder: Utilities.oc(data, "placeholder", ""),
                    fieldGroupIds: data.fieldGroupIds,
                    showSuggestion: true,
                    textFormatMode: "KeyValue",
                    suggest: (x) => {
                        console.log(x);
                        var sTerm = x.getParameter("suggestValue");
                        var aFilters = [];
                        if (sTerm) {
                            aFilters.push(new sap.ui.model.Filter(data.selectTargetColumnTitle, sap.ui.model.FilterOperator.StartsWith, sTerm));
                            aFilters.push(new sap.ui.model.Filter(data.selectTargetColumnDescription, sap.ui.model.FilterOperator.StartsWith, sTerm));
                            aFilters.push(new sap.ui.model.Filter(data.selectTargetColumnID, sap.ui.model.FilterOperator.StartsWith, sTerm));
                        }
                        x.getSource().getBinding("suggestionItems").sOperationMode = sap.ui.model.odata.OperationMode.Server;
                        var orFilters = new sap.ui.model.Filter({
                            filters: aFilters,
                            and: false
                        });
                        x.getSource().getBinding("suggestionItems").filter(orFilters);
                    },
                    suggestionItemSelected: (oEvent) => {
                        let source = oEvent.getSource();
                        let key = source.getSelectedKey();
                        let value = source.getValue();
                        source.unbindValue();
                        let re = /(\w*)>([A-Za-z0-9\/\-\_]*)/g;
                        let results = re.exec(bind);
                        let model = results[1];
                        let path = results[2];
                        if (model && path) {
                            let form = this.oController.getModel(model);
                            let input_value = key ? key : value;
                            form.setProperty(path, input_value);
                            source.setSelectedKey(input_value);
                        }
                    },
                    suggestionItems: {
                        path: `script-read>${data.dataServiceUrl || `/getValidValues(parameterId='${data.pId}',entity='${data.entity}')`}`,
                        template: template
                    }
                });
                dm.setSource(oInput);
                control.push(oInput);
            }
            _module._getInstructions(data, control);
            return control;
        },
        //tableMandatory?, tableType, tableTitle question
        getTable: async function (data) {
            function visible(disp) {
                let device = sap.ui.Device.system;
                let visible;
                if (disp === "tablet") {
                    visible = device.tablet || device.combi;
                } else if (disp === "mobile") {
                    visible = device.phone;
                } else {
                    visible = true;
                }
                return visible;
            }
            let dynamic;
            let control = [];
            // data.instruction = data.tableInstructions;
            let template = {
                expandable: true,
                expanded: true,
                width: "100%",
                backgroundDesign: "Solid",
                headerToolbar: new OverflowToolbar({
                    content: [
                        new sap.m.Title({
                            text: Utilities.oc(data, "tableTitle", "")
                        }),
                        new ToolbarSpacer()
                    ]
                }),
                content: []
            }
            let table = {
                columns: [],
                items: [],
                fieldGroupIds: data.fieldGroupIds,
                id: `ID_${data.stepId}`
            }
            if (data.tableType === "dynamic") {
                dynamic = new Dynamic(null, this.oController, data);
                dynamic.onInit();
                template.headerToolbar.insertContent(new sap.m.Button({
                    icon: "sap-icon://add",
                    type: "Transparent",
                    press: () => {
                        dynamic.onPressNewRow()
                    }
                }), 2)
            }

            function calcCWR(value, columns) {
                let aggregation = 0;
                value = parseFloat(value);
                for (let column of columns) {
                    let origin = 0;
                    if (column && column.ratio) {
                        origin = column.ratio;
                    }
                    let float = parseFloat(origin);
                    if (!isNaN(float)) aggregation += float;
                }
                if (!isNaN(aggregation) && !isNaN(value)) {
                    let decimal = value / aggregation;
                    let percent = decimal * 100;
                    return `${percent}%`;
                } else {
                    return "auto";
                }
            }
            _module._getInstructions(data, control, true, true);
            if (data.columns && data.rows) {
                for (var i = 0; i < data.columns.length; i++) {
                    let column = data.columns[i];
                    if (column) {
                        var visible_setting = Utilities.oc(column, "display_setting", "all");
                        var bVisible = visible(visible_setting);
                        let cwr = Utilities.oc(column, "ratio", "auto");
                        let width = calcCWR(cwr, data.columns);
                        if (column && bVisible) {
                            table.columns.push(new Column({
                                width: width,
                                header: new Text({
                                    text: Utilities.oc(column, "title", "")
                                })
                            }))
                        }
                    }
                }
                for (var i = 0; i < data.rows.length; i++) {
                    let row = data.rows[i];
                    let _template = {
                        cells: []
                    };
                    if (row && row.cells) {
                        for (var n = 0; n < data.columns.length; n++) {
                            let column = data.columns[n];
                            if (column) {
                                var visible_setting = Utilities.oc(column, "display_setting", "all");
                                var bVisible = visible(visible_setting);
                                if (!bVisible) continue;
                                let cell = row.cells[n] ? row.cells[n] : { content: null }
                                let ufid = `E${uuidv4()}`
                                let pId = column.pid;
                                let entity = data.entity;
                                cell.content = await this.oController.popBinding(data.stepId, [n, i], cell.content);
                                if (column.type === _module.input.Text) {
                                    let text_setting = Utilities.oc(column, "text_setting", "wrapped");
                                    let wrapped = !(text_setting === "truncated");
                                    _template.cells.push(new Text({
                                        wrapping: wrapped,
                                        text: `{form>${Utilities.oc(cell, "content", "")}}`,
                                    }));
                                } else if (column.type === _module.input.Input) {
                                    _template.cells.push(new Input({
                                        value: `{form>${Utilities.oc(cell, "content", "")}}`,
                                    }));
                                } else if (column.type === _module.input.Date) {
                                    let tempDate = new DatePicker({
                                        value: `{form>${Utilities.oc(cell, "content", "")}}`,
                                        valueFormat: "yyyy-MM-dd",
                                    });
                                    tempDate.setDateValue(new Date());
                                    _template.cells.push(tempDate);
                                } else if (column.type === _module.input.Number) {
                                    _template.cells.push(new Input({
                                        value: `{form>${Utilities.oc(cell, "content", "")}}`,
                                        type: sap.m.InputType.Number,//sap.m.InputType.Tel,
                                    }));
                                } else if (column.type === _module.input.QR) {
                                    _template.cells.push(new InputWithIcon({
                                        icon: "sap-icon://bar-code",
                                        value: `{form>${Utilities.oc(cell, "content", "")}}`,
                                        endButtonPress: (oEvent) => {
                                            _module.oController.getQRDialog(oEvent);
                                        },
                                    }));
                                } else if (column.type === _module.input.ComboBox) {
                                    let path = Utilities.oc(cell, "content", "");
                                    let config = {
                                        placeholder: "",
                                        change: (oEvent) => {
                                            let source = oEvent.getSource();
                                            let selectedItem = oEvent.getParameter("selectedItem");
                                            source.unbindProperty("selectedKey", false);
                                            if (path) {
                                                let form = this.oController.getModel("form");
                                                let value = selectedItem.getKey() ? selectedItem.getKey() : selectedItem.getText();
                                                form.setProperty(path, value);
                                                source.setSelectedKey(value);
                                            }
                                        },
                                        selectedKey: `{form>${Utilities.oc(cell, "content", "")}}`,
                                        showSecondaryValues: true,
                                        forceSelection: false,
                                        items: []
                                    }
                                    if (data.fields.length < 1) {
                                        let controlate = new sap.ui.core.ListItem({
                                            text: `{script-read>ValidValueDescription}`,
                                            key: `{script-read>ValidValueShortDesc}`,
                                            additionalText: `{script-read>ValidValueShortDesc}`
                                        }).addCustomData(new sap.ui.core.CustomData({
                                            key: "value",
                                            value: `{script-read>ValidValueShortDesc}`
                                        }));
                                        config.items = this.popValidValues(pId, entity, controlate);
                                    }
                                    if (data.fields && data.fields[n] && data.fields[n].length > 0) {
                                        data.fields[n].forEach(field => {
                                            config.items.push(new sap.ui.core.Item({
                                                text: Utilities.oc(field, "content", ""),
                                                key: Utilities.oc(field, "content", "")
                                            }))
                                        });
                                    }
                                    _template.cells.push(new Select(config));
                                } else if (column.type === _module.input.CheckBox) {
                                    let config = {
                                        items: []
                                    }
                                    let path = Utilities.oc(cell, "content", "");
                                    if (data.fields.length < 1) {
                                        let controlate = new CheckBox({
                                            text: `{script-read>ValidValueDescription}`,
                                        }).addCustomData(new sap.ui.core.CustomData({
                                            key: "value",
                                            value: `{script-read>ValidValueShortDesc}`
                                        }));
                                        config.items = this.popValidValues(pId, controlate);
                                    }
                                    if (data.fields[n]) {
                                        data.fields[n].forEach(field => {
                                            let text = Utilities.oc(field, "content", "");
                                            config.items.push(new CheckBox({
                                                fieldGroupIds: data.fieldGroupIds.concat([ufid]),
                                                text: text,
                                                selected: this.oController.getSelected(path, text),
                                                select: ($event) => {
                                                    this.oController.onSelect($event, path, ufid)
                                                }
                                            }))
                                        });
                                    }
                                    _template.cells.push(new VBox(config));
                                }
                            }
                        }
                        table.items.push(new ColumnListItem(_template).setType(sap.m.ListType.Active));
                    }
                }
                let oTable = new Table(table);
                if (dynamic) {
                    dynamic.setTable(oTable);
                    dynamic.getDynamics();
                }
                template.content.push(oTable);
                control.push(new Panel(template)
                    .addStyleClass("dxcUiPanel")
                    .setLayoutData(new sap.ui.layout.GridData({
                        span: "XL12 L12 M12 S12"
                    }))
                );
            }
            // _module._getInstructions(data, control, true);
            return control;
        },
        getControl: function (template, type) {
            if (type === "input") {
                let control = new Input();
                template.cells.push(control);
            } else if (type === "text") {
                let control = new Text({
                    text: "Placeholder"
                });
                template.cells.push(control);
            }
        }
    }
    let module = {
        /**
         * This function is to set the controller in the module class
         * @param {*} oController 
         */
        setOController: function (oController) {
            _module.oController = oController;
        },
        /**
         * Directory/Map of the Functions.
         * CONTROLS MUST BE SET ON THE controls MAP TO BE EXPOSED TO THE CONTROLLER LOGIC.
         */
        controls: new Map([
            ["20", _module.getHeading],
            ["21", _module.getBody],
            ["99", _module.getEditor],
            ["34", _module.getXLine],
            ["35", _module.getSpacer],
            ["22", _module.getInput],
            ["23", _module.getTextArea],
            ["24", _module.getEmail],
            ["25", _module.getNumber],
            ["26", _module.getContent],
            ["27", _module.getRadio],
            ["28", _module.getCheckbox],
            ["29", _module.getComboBox],
            ["30", _module.getTable],
            ["31", _module.getDatePicker],
            ["32", _module.getTimePicker],
            ["33", _module.getSignature],
            ["36", _module.getUpload],
            ["37", _module.getQR],
            ["38", _module.getMultiInput],
            ["39", _module.getLocation],
            ["40", _module.getAsset],
            ["41", _module.getContact],
            ["42", _module.getSelectDialog]
        ]),
    }
    return module;

});