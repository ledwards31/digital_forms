sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/Dialog",], function (Controller, Dialog) {
    return {

        intializeUtilities: function (oController) {
            this.oController = oController;
        },

        getXSJSData: function (sURL, body, successCallback, errCallback) {
            if (window.location.href.indexOf('workspaces') >= 0) {
                sURL = "https://brstoolscf-workspaces-ws-xf5tm-app2.ap10.applicationstudio.cloud.sap/BRS_HANA";
                // sURL = "https://webidetesting5140463-c910ee3ff.dispatcher.ap1.hana.ondemand.com/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
            }
            if (body && body.Entity) {
                sURL = sURL + "/get" + body.Entity + "()";
            }
            $.ajax({
                header: {
                    "access-control-allow-origin": "*"
                },
                url: sURL,
                type: "GET",
                crossDomain: true,
                dataType: 'json',
                data: body,
                //dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    successCallback(result.value);
                },
                error: function (err) {
                    errCallback(err);
                }
            });
        },

        getCFNodeJSData: function (sURL, entity, paramName, paramValue, filters, successCallback, errCallback) {
            /*if (window.location.href.indexOf('workspaces') >= 0) {
                sURL = "https://brstoolscf-workspaces-ws-xf5tm-app2.ap10.applicationstudio.cloud.sap/BRS_HANA";
                // sURL = "https://webidetesting5140463-c910ee3ff.dispatcher.ap1.hana.ondemand.com/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
            }*/

            if (!sURL) {
                sURL = this.oController.serviceURL
            }
            if (entity && !paramName) {
                sURL = sURL + "/get" + entity + "()";
            } else if (paramName && paramValue) {
                sURL = sURL + "/get" + entity + "(" + paramName + "='" + paramValue + "')";
            }
            if (filters) {
                var keys = Object.keys(filters);
                if (keys.length > 0) {
                    sURL = sURL + "?$filter=";
                    keys.forEach(function (key, index) {
                        var filter = filters[key];
                        if (index === 0) {
                            sURL = sURL + key + " eq " + filter + "";
                        } else {
                            sURL = sURL + "&" + key + " eq " + filter + "";
                        }

                    })
                }
            }
            $.ajax({
                header: {
                    "access-control-allow-origin": "*"
                },
                url: sURL,
                type: "GET",
                crossDomain: true,
                dataType: 'json',
                // data: body,
                //dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    successCallback(result.value);
                },
                error: function (err) {
                    errCallback(err);
                }
            });
        },
        /**
        * This is the GENERIC POST function to call any XSJS service passing on a data and getting back response
        * Input Parameters:
        *	- sURL -> String URL for the XSJS service to be called
        *  - body -> A JSON object, to be passed on as data to the XSJS
        *  - successCallback -> The function to be called when success
        *  - errCallback -> The function to be called when error
        *  - i -> An index that is passed back to the success function (to keep track of the return)
        * Return:
        *	- Generic Resultset
        **/
        postXSJSData: function (sURL, body, successCallback, errCallback, i) {
            console.log("Utilities postXSJSData Started", body, i);
            if (window.location.href.indexOf('webapp') >= 0) {
                sURL = window.location.protocol + "//" + window.location.hostname + "/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
                // sURL = "https://webidetesting5140463-c910ee3ff.dispatcher.ap1.hana.ondemand.com/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
            }
            $.ajax({
                header: {
                    "access-control-allow-origin": "*"
                },
                url: sURL,
                type: "POST",
                crossDomain: true,
                dataType: 'json',
                data: JSON.stringify(body),
                contentType: "application/json",
                indexValue: i,
                success: function (result) {
                    console.log("Utilities postXSJSData Ended", result, this.indexValue);
                    successCallback(result, this.indexValue);
                },
                error: function (err) {
                    errCallback(err);
                }
            });
        },

        postCFNodeJSData: function (sURL, entity, body, successCallback, errCallback, i) {
            console.log("Starting postCFNodeJSData", sURL, entity, body)
            if (window.location.href.indexOf('workspaces') >= 0) {
                sURL = "https://brstoolscf-workspaces-ws-xf5tm-app2.ap10.applicationstudio.cloud.sap/BRS_HANA";
                // sURL = "https://webidetesting5140463-c910ee3ff.dispatcher.ap1.hana.ondemand.com/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
            }
            if (!sURL) {
                sURL = this.oController.serviceURL
            }
            if (entity) {
                sURL = sURL + "/" + entity; //+ "()";
            }
            // if (filters) {
            //     sURL = sURL + "?$filter="
            //     var keys = Object.keys(filters);
            //     keys.forEach(function (key, index) {
            //         console.log(key, index);
            //         var filter = filters[key];
            //         if (index === 0) {
            //             sURL = sURL + key + " eq " + filter + "";
            //         } else {
            //             sURL = sURL + "&" + key + " eq " + filter + "";
            //         }

            //     })
            // }
            $.ajax({
                header: {
                    "access-control-allow-origin": "*"
                },
                url: sURL,
                type: "POST",
                crossDomain: true,
                dataType: 'json',
                data: JSON.stringify(body),
                indexValue: i,
                contentType: "application/json",
                success: function (result) {
                    successCallback(result, this.indexValue);
                },
                error: function (err) {
                    errCallback(err);
                }
            });
        },

        deleteXSJSData: function (sURL, body, successCallback, errCallback, i) {
            console.log("Utilities deleteXSJSData Started", body, i);
            if (window.location.href.indexOf('webapp') >= 0) {
                sURL = window.location.protocol + "//" + window.location.hostname + "/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
                // sURL = "https://webidetesting5140463-c910ee3ff.dispatcher.ap1.hana.ondemand.com/BRS_HANA/BRS/IRIS/Scripting/xs/Scripting.xsjs";
            }
            $.ajax({
                header: {
                    "access-control-allow-origin": "*"
                },
                url: sURL,
                type: "DELETE",
                crossDomain: true,
                dataType: 'json',
                data: JSON.stringify(body),
                contentType: "application/json",
                indexValue: i,
                success: function (result) {
                    console.log("Utilities postXSJSData Ended", result, this.indexValue);
                    successCallback(result, this.indexValue);
                },
                error: function (err) {
                    errCallback(err);
                }
            });
        },
        _messageHandling: function (text, title, icon, buttons) {
            var that = this.oController;
            if (!buttons) {
                if (!that.diag) {
                    that.diag = new sap.m.Dialog({
                        title: title ? title : that.oBundle.getProperty("warning"),
                        icon: icon ? icon : "sap-icon://message-warning",
                        content: [
                            new sap.m.Text({
                                text: text || ""
                            })
                        ],
                        buttons: [
                            new sap.m.Button({
                                text: that.oBundle.getProperty("ok"),
                                // type: "Emphasized",
                                press: function () {
                                    that.diag.close();
                                }
                            })
                        ]
                    }).addStyleClass("sapUiContentPadding sapUiSizeCompact viewDialog");
                } else {
                    that.diag.setTitle(title);
                    that.diag.getContent()[0].setText(text);
                }
                that.diag.open();
            } else {
                if (that.diag) {

                }
                that.diag = new sap.m.Dialog({
                    title: title ? title : that.oBundle.getProperty("warning"),
                    icon: icon ? icon : "sap-icon://message-warning",
                    content: [
                        new sap.m.Text({
                            text: text || ""
                        })
                    ],
                    buttons: buttons
                }).addStyleClass("sapUiContentPadding sapUiSizeCompact viewDialog").open();
            }
        },
        /**
         * TEMP Replacement for Optional Changing in JavaScript.
         * @param {Object} data
         * @param {string} path
         * @param {Any} fb
         */
        oc(data, path, fb) {
            for (var i = 0, path=path.split('.'); i < path.length; i++){
                let property = data[path[i]];
                if (property) {
                    data = property;
                } else {
                    return fb;
                }
            };
            return data;
        }
    };
},
    true);