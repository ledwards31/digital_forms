if ('function' === typeof importScripts) {
    importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.1.0/workbox-sw.js');
    if (workbox) {
        self.skipWaiting();
        workbox.core.clientsClaim();
        workbox.routing.registerRoute(new RegExp('.+/resources/.+'), new workbox.strategies.StaleWhileRevalidate());
        workbox.routing.registerRoute(
            new RegExp('.+\/index\.html'),
            new workbox.strategies.NetworkFirst()
        );
        workbox.routing.registerRoute(
            /.+\/manifest\.json(\?.*)?/g,
            new workbox.strategies.NetworkFirst()
        );
        let manifest = self.__WB_MANIFEST;
        workbox.precaching.precacheAndRoute(manifest);
    }
}