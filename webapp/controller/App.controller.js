sap.ui.define([
	"com/ven/iris/FormRenderer/controller/BaseController",
], function (Controller) {
	"use strict";
	return Controller.extend("com.ven.iris.FormRenderer.controller.App", {
		_onInit: function () {
			var oRouter = this.getRouter();
			oRouter.attachBypassed(function (oEvent) {
				var sHash = oEvent.getParameter("hash");
			});
			oRouter.attachRouteMatched(function (oEvent){
				var sRouteName = oEvent.getParameter("name");
			});
			this.verifyVer().catch(error => console.error(error));
		},
		verifyVer() {
			return new Promise((resolve, reject) => {
				let update = (version) => {
					console.log(`Digital Forms v${version}`)
					localStorage.setItem('DIGIFORM_VERSION', version);
				}
				let oModel = this.getModel("script-read");
				let sPath = `/latestVersion(...)`;
				let oBindingContext = oModel.createBindingContext("/");
				let oOperation = oModel.bindContext(sPath, oBindingContext);
				oOperation.execute().then(() => {
					let response = oOperation.getBoundContext().getObject();
					let version = localStorage.getItem('DIGIFORM_VERSION');
					if (version && version != "null") {
						if (response.version !== version && 'serviceWorker' in navigator) {
							caches.keys().then((names) => {
								for (let name of names) caches.delete(name);
								console.log("Digital Forms detected a new cache version from the server. Reloading...");
								update(response.version);
								location.reload();
							}).catch(error => reject(error));
						}
					} else {
						update(response.version);
					}
					resolve();
				}).catch((error) => {
					reject(error);
				});
			})
		},
		//Navigation Buttons
		onTabPress(oEvent, _Override) {
			let oParameters = oEvent.getParameters();
			this.navToTab(oParameters, _Override);
		},
		navToTab: function(oParameters, _Override) {
			let key;
			if (_Override) {
				key = _Override;
			} else {
				key = oParameters.tabKey;
			}
			if (key === "Home") {
				window.location = `${window.location.origin}/comventiamobApps/index.html`
			} else if (key === "WorkOrders") {
				window.location = `${window.location.origin}/comventiamobApps/index.html#/orders`
			} else {
				this.getOwnerComponent().getRouter().navTo(key);
			}
			let control = this.byId("idIconTabBar");
			control.getItems().forEach(item => {
				item.setActive(item.getKey() === key);
			})
		}      
	});
});