sap.ui.define([
    "com/ven/iris/FormRenderer/controller/BaseController",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel",
    "com/ven/iris/FormRenderer/common/ActionClass",
    "com/ven/iris/FormRenderer/common/ControlDictionary",
    "com/ven/iris/FormRenderer/common/Utilities",
    "sap/ui/core/Fragment",
    "sap/ui/core/Popup",
    "sap/m/CustomListItem",
    "sap/ui/core/Icon",
    "sap/m/HBox",
    "sap/m/Text",
    "sap/m/Button",
    "sap/ui/Device",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "com/ven/iris/FormRenderer/common/fragment.definition",
    "com/ven/iris/FormRenderer/common/dependencies/zip/jszip",
    "sap/m/Title",
    "sap/m/VBox"
], function (
    Controller,
    MessageBox,
    MessageToast,
    JSONModel,
    ActionClass,
    ControlDictionary,
    Utilities,
    Fragment,
    Popup,
    CustomListItem,
    Icon,
    HBox,
    Text,
    Button,
    Device,
    Filter,
    FilterOperator,
    FragmentDefinition,
    JSZIP,
    Title,
    VBox
) {
    "use strict";
    return Controller.extend("com.ven.iris.FormRenderer.controller.list", {
        _onInit: function () {
            this.onMatched();
            this.byId("idList").removeSelections();
            this.sOrder = "desc";
            var oRouter = this.getRouter();
            //oRouter.getRoute("list").attachMatched(this._onRouteMatched, this);
        },
        onMatched: function() {
            let oTabSelect = this.byId("idTabSelect");
            let selectedKey = oTabSelect.getSelectedKey();
            this.tabKey = selectedKey ? selectedKey : "Browse";
            this.setTitle(this.tabKey);
        },
        onAfterRendering: function () {
            this.onSync();
            window.addEventListener("online", this.onSync.bind(this));
            window.addEventListener("offline", this._onNetworkOFF.bind(this));
        },
        onItemPress: function (oEvent) {
            let oParameters = oEvent.getParameters();
            let list = oEvent.getSource();
            let listItem = oParameters.listItem;
            let oBindingInfo = list.getBindingInfo("items");
            let model = oBindingInfo.model;
            let oBindingContext = listItem.getBindingContext(model);
            let value = oBindingContext.getObject();
            if (this.tabKey === "Pending") {
                this.formId = value.ScriptID, this.runId = value.RunID;
                this.onContext_PDF();
            } else {
                let type = this.getRoute(model, value);
                this.getRouter().navTo("script", {
                    type: type,
                    id: type === this.nRoutes.run ? value.RunID : value.ScriptID
                });
            }
        },
        onContextPress: function (oEvent) {
            var oButton = oEvent.getSource();
            this.formId = oButton.data("form"), this.runId = oButton.data("run");
            FragmentDefinition.getMenu(this, this.tabKey).then(oMenu => {
                this._Menu = oMenu;
                this._oOpener = oButton;
                this.getView().addDependent(this._Menu);
                this._Menu.open(this._bKeyboard, oButton, Popup.Dock.BeginTop, Popup.Dock.BeginBottom, oButton);
            })
        },
        onSearch: function (oEvent) {
            let oParameters = oEvent.getParameters();
            let list = this.byId("idList");
            let value = oParameters.newValue ? oParameters.newValue : oParameters.query;
            if (!value) value = "";
            let filt = [
                new Filter("Description", FilterOperator.Contains, value),
                new Filter("Tags", FilterOperator.Contains, value)
            ];
            list.getBinding("items").filter(new Filter({
                filters: filt,
                and: false
            }));
        },
        _getSyncIcon: function () {
            let iconTab = this.byId("idSync");
            return iconTab;
        },
        _onNetworkOFF: function () {
            this.newMessage({
                message: this.i18n("OFFLINE_WARN_MESSAGE"),
                description: this.i18n("OFFLINE_WARN_DESCRIPTION"),
                type: "Warning",
            });
        },
        onPressSort: function () {
            let list = this.byId("idList");
            if (!this.sOrder) this.sOrder = "desc";
            if (this.sOrder === "asc") {
                this.sOrder = "desc";
                MessageToast.show(this.i18n("LIST_ORDER_DESC"));
            } else if (this.sOrder === "desc") {
                this.sOrder = "asc";
                MessageToast.show(this.i18n("LIST_ORDER_ASC"));
            }
            list.getBinding("items").sort(this.sOrder && new sap.ui.model.Sorter("createdAt", this.sOrder === "desc"));
        },
        popRun: function (data, runId) {
            return new Promise(resolve => {
                data.formResults.scriptResultsHeader.RunID = runId;
                data.formResults.scriptResults.forEach(e => e.RunID = runId);
                data.formResults.scriptResultsMultiParam.forEach(e => e.RunID = runId);
                data.formResults.scriptResultsStepsHierarchy.forEach(e => e.RunID = runId);
                resolve();
            })
        },
        tryPost: function (data) {
            return new Promise((resolve, reject) => {
                this.getRunID().then(runId => {
                    let trid = data.formResults.scriptResultsHeader.RunID;
                    return Promise.all([
                        runId,
                        this.database.ReadSome("Files", "runId", trid),
                        this.popRun(data, runId)
                    ]);
                }).then((values) => {
                    let runId = values[0];
                    let files = values[1];
                    return Promise.all([
                        this.postScript(data),
                        this.tryUpload(files, runId)
                    ]);
                }).then(() => {
                    return this.database.Delete("Pending", data.id);
                }).then(() => {
                    resolve();
                }).catch(error => {
                    reject(error);
                })
            })
        },
        tryUpload: function (files, runId) {
            function put(file) {
                return new Promise((resolve, reject) => {
                    file.payload.IRIS_RUNID = runId;
                    fetch(`${file.url}`, {
                        method: "POST",
                        body: JSON.stringify(file.payload),
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(response => {
                        if (!response.ok) {
                            reject(new Error(this.i18n("UL_FAIL_DESCRIPTION")))
                        } else {
                            return response.json();
                        }
                    }).then(response => {
                        return new Promise((resolve, reject) => {
                            var xhr = new XMLHttpRequest();
                            xhr.open('PUT', `${file.url}(${response.ID})/FileContent`, true);
                            xhr.setRequestHeader("Content-Type", file.payload.MimeType);
                            xhr.onreadystatechange = function () {
                                if (this.status === 204) {
                                    resolve(file.id);
                                } else {
                                    reject(new Error(xhr.statusText));
                                }
                            }
                            xhr.send(file.file);
                        });
                    }).then((id) => {
                        resolve(id);
                    }).catch(error => {
                        console.error(error);
                        reject(error);
                    });
                })
            }
            return new Promise((resolve, reject) => {
                let jobs = [];
                files.forEach(file => jobs.push(put(file)));
                Promise.all(jobs).then((ids) => {
                    let txs = [];
                    ids.forEach(id => {
                        txs.push(this.database.Delete("Files", id));
                    });
                    return Promise.all(txs);
                }).then(() => {
                    resolve();
                }).catch(error => {
                    reject(error)
                });
            });
        },
        onSync: function () {
            let icon = this._getSyncIcon();
            icon.addStyleClass("dxcUiSpin");
            this.database.ReadAll("Pending").then(data => {
                let jobs = [];
                data.forEach(row => {
                    jobs.push(this.tryPost(row));
                })
                return Promise.all(jobs);
            }).then(() => {
                return this.fireRequest();
            }).then(() => {
                this.onSyncDone();
            }).catch(error => {
                console.error(error);
                this.newMessage({
                    message: this.i18n("SYNC_ERROR"),
                    description: error.message,
                    type: "Error",
                });
            })
        },
        onSyncDone: function () {
            let icon = this._getSyncIcon();
            if (this.tabKey === "Pending") this.getPendingBinding();
            setTimeout(() => {
                icon.removeStyleClass("dxcUiSpin");
            }, 2000);
        },
        onSearchOpen: function (oEvent) {
            let oSource = oEvent.getSource();
            let oHeader = oSource.getParent();
            let aContent = oHeader.getContentRight();
            aContent[0].removeStyleClass("dxcUi0Width");
            aContent[1].addStyleClass("dxcUi0Width");
        },
        onSearchClose: function (oEvent) {
            let oSource = oEvent.getSource();
            let oHeader = oSource.getParent();
            let aContent = oHeader.getContentRight();
            let list = this.byId("idList");
            aContent[1].removeStyleClass("dxcUi0Width");
            aContent[0].addStyleClass("dxcUi0Width");
            list.getBinding("items").filter([]);
        },
        onContext_PDF: function (oEvent) {
            let prefill = ["Sent", "Pending"].includes(this.tabKey);
            let id = prefill ? this.runId : this.formId;
            this.title = this.formId;
            if (this._oOpener && this._oOpener.getParent() && this._oOpener.getParent().getParent() && this._oOpener.getParent().getParent() && this._oOpener.getParent().getParent().getBindingContext("results-read").getObject()) {
                let item = this._oOpener.getParent().getParent().getBindingContext("results-read").getObject();
                if (item && item.Title) {
                    this.title = item.Title;
                    this.title = this.title.replace(" - ", "_");
                    this.title = this.title.replace(/ /g, "");
                }
            }
            let oModel = this.getModel("results-read");
            if (navigator.onLine) {
                if (sap.ui.Device.os.ios || (sap.ui.Device.system.tablet && sap.ui.Device.os.macintosh)) {
                    let sPath = prefill ? `/genRaw(runId='${id}')` : `/getRaw(formId='${id}')`;
                    let sOrigin = window.location.origin;
                    let sServiceUrl = oModel.sServiceUrl;
                    window.open(`${sOrigin}${sServiceUrl}${sPath}`, "_blank");
                } else {
                    let sPath = prefill ? "/genExport(...)" : "/getExport(...)";
                    let oBindingContext = oModel.createBindingContext("/");
                    let oOperation = oModel.bindContext(sPath, oBindingContext);
                    oOperation.setParameter(prefill ? "runId" : "formId", id);
                    oOperation.execute().then(() => {
                        let response = oOperation.getBoundContext().getObject();
                        let pdf = response.value;
                        let link = document.createElement('a');
                        link.href = pdf;
                        link.download = this.title + ".pdf";
                        link.click();
                    }).catch((error) => {
                        this.fatal(error);
                    });
                }
            } else {
                let type;
                if (this.tabKey === "Browse") {
                    type = this.nRoutes.new;
                } else if (this.tabKey === "Sent") {
                    type = this.nRoutes.run;
                } else if (this.tabKey === "Downloads") {
                    type = this.nRoutes.local;
                } else if (this.tabKey === "Pending") {
                    type = this.nRoutes.prefill;
                }
                this.getRouter().navTo("pdf", {
                    type: type,
                    id: id
                });
            }
        },
        getFile: function (uuid) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("script-write");
                let sServiceUrl = oModel.sServiceUrl;
                let origin = window.location.origin;
                let base = `${origin}${sServiceUrl}`;
                fetch(`${base}/ScriptAttachments/${uuid}/FileContent`).then(response => {
                    return response.blob();
                }).then(blob => {
                    resolve(blob);
                }).catch((error) => {
                    reject(error);
                });
            })
        },
        getCompress: function (data, files) {
            return new Promise(resolve => {
                if (data.length > 0) {
                    let instance = new JSZip();
                    for (var i = 0; i < data.length; i++) {
                        var options = {
                            base64: true
                        }
                        let name = files ? files[i].FileName : data[i].payload.FileName;
                        let file = data[i] === typeof Blob ? data[i] : data[i].file;
                        instance.file(name, file, options);
                    }
                    instance.generateAsync({
                        type: "blob"
                    }).then((content) => {
                        let url = window.URL.createObjectURL(content)
                        let link = document.createElement('a');
                        link.href = url;
                        link.download = this.runId + ".zip";
                        link.click();
                        resolve();
                    });
                } else {
                    MessageToast.show(this.i18n("NO_FILES"));
                }
            })
        },
        onContext_Files: function (oEvent) {
            let id = this.runId;
            if (this.tabKey === "Sent") {
                let oModel = this.getModel("results-read");
                let sPath = "/getFiles(...)";
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                let files;
                oOperation.setParameter("runId", id);
                oOperation.execute().then(() => {
                    let response = oOperation.getBoundContext().getObject();
                    files = response.value;
                    let jobs = [];
                    for (var file of files) {
                        jobs.push(this.getFile(file.ID));
                    }
                    return Promise.all(jobs)

                }).then((data) => {
                    return this.getCompress(data, files);
                }).catch((error) => {
                    this.fatal(error);
                });
            } else if (this.tabKey === "Pending") {
                this.database.ReadSome("Files", "runId", id).then(data => {
                    return this.getCompress(data);
                });
            }
        },
        onTabSelect: function (oEvent) {
            var oParameters = oEvent.getParameters();
            this.tabKey = oParameters.key;
            this.setTitle(this.tabKey);
            if (this.tabKey === "Browse") {
                this.getBrowseBinding();
            } else if (this.tabKey === "Downloads") {
                this.getDownloadBinding();
            } else if (this.tabKey === "Pending") {
                this.getPendingBinding();
            } else if (this.tabKey === "Sent") {
                this.getSentBinding();
            }
        },
        safeBinding: function (oEvent) {
            let oParameters = oEvent.getParameters();
            let list = this.byId("idList");
            if (oParameters.error) {
                this.fatal(new Error(oParameters.error.message)).then(() => {
                    list.setNoDataText(oParameters.error.message);
                    list.rerender();
                });
            } else {
                list.setNoDataText(this.i18n("NO_DATA"));
            }
        },
        getBrowseBinding: function () {
            let list = this.byId("idList");
            list.bindAggregation("items", {
                path: "results-read>/getForms()",
                parameters: {
                    $$operationMode: "Server"
                },
                sorter: new sap.ui.model.Sorter("createdAt", this.sOrder === "desc"),
                templateShareable: true,
                template: this.getTemplate("results-read"),
                events: {
                    dataReceived: this.safeBinding.bind(this)
                }
            });
        },
        getSentBinding: function () {
            let list = this.byId("idList");
            list.bindAggregation("items", {
                path: "results-read>/getExisting()",
                templateShareable: true,
                parameters: {
                    $$operationMode: "Server"
                },
                sorter: new sap.ui.model.Sorter("createdAt", this.sOrder === "desc"),
                template: this.getTemplate("results-read", true),
                events: {
                    dataReceived: this.safeBinding.bind(this)
                }
            });
        },
        getDownloadBinding: function () {
            let list = this.byId("idList");
            list.setBusy(true);
            list.setNoDataText(this.i18n("NO_DATA"));
            this.getDownloads().then(() => {
                list.bindAggregation("items", {
                    path: "downloads>/forms",
                    templateShareable: true,
                    template: this.getTemplate("downloads"),
                    events: {
                        dataReceived: this.safeBinding.bind(this)
                    }
                });
            }).catch(error => {
                console.error(error);
                this.newMessage({
                    message: this.i18n("DL_ERROR"),
                    description: error.message,
                    type: "Error",
                });
            }).finally(() => {
                list.setBusy(false);
            })
        },
        getPendingBinding: function () {
            let list = this.byId("idList");
            list.setBusy(true);
            list.setNoDataText(this.i18n("NO_DATA"));
            this.getPending().then(() => {
                list.bindAggregation("items", {
                    path: "pending>/forms",
                    templateShareable: true,
                    template: this.getTemplate("pending"),
                    events: {
                        dataReceived: this.safeBinding.bind(this)
                    }
                });
            }).catch(error => {
                console.error(error);
                this.newMessage({
                    message: this.i18n("SYNC_ERROR"),
                    description: error.message,
                    type: "Error",
                });
            }).finally(() => {
                list.setBusy(false);
            })
        },
        goBack: function () {
            this.getRouter().navTo("home");
        },
        getTemplate: function (model, isSent) {
            let tempVbox = {};
            if (isSent) {
                tempVbox = new VBox({
                    items: [
                        new Title({
                            // text: `{${model}>ScriptID}: {${model}>Description}`
                            text: `{${model}>Title}`
                        }),
                        new Text({
                            text: `{${model}>Tags}`
                        })
                    ]
                }).addStyleClass("dxcUiSmallMarginBegin");
            } else {
                tempVbox = new VBox({
                    items: [
                        new Title({
                            text: `{${model}>ScriptID}: {${model}>Description}`
                            // text: `{${model}>Title}`
                        }),
                        new Text({
                            text: `{${model}>Tags}`
                        })
                    ]
                }).addStyleClass("dxcUiSmallMarginBegin");
            }
            return new CustomListItem({
                content: [
                    new HBox({
                        alignItems: "Center",
                        justifyContent: "SpaceBetween",
                        items: [
                            new HBox({
                                alignItems: "Center",
                                justifyContent: "SpaceBetween",
                                items: [
                                    new Icon({
                                        size: "23px",
                                        src: "sap-icon://document"
                                    }),
                                    tempVbox
                                ]
                            }),
                            new Button({
                                icon: "sap-icon://overflow",
                                type: "Transparent",
                                press: this.onContextPress.bind(this)
                            })
                                .addStyleClass("dxcUiBtn")
                                .addCustomData(new sap.ui.core.CustomData({
                                    key: "form",
                                    value: `{${model}>ScriptID}`
                                }))
                                .addCustomData(new sap.ui.core.CustomData({
                                    key: "run",
                                    value: `{${model}>RunID}`
                                }))
                        ]
                    }).addStyleClass("dxcUiMediumMarginTopBottom dxcUiLargeMarginBeginEnd"),
                ]
            })
        },
        getFactory: function (sId, oContext) {
            return this.getTemplate("results-read").clone(sId);
        },
        getDownloads: function () {
            return new Promise((resolve, reject) => {
                this.database.ReadAll("Downloads").then(data => {
                    console.log(data);
                    let model = data.map(item => item.Script);
                    console.log(model);
                    let oModel = new JSONModel({
                        forms: model
                    });
                    this.setModel(oModel, "downloads");
                    resolve();
                }).catch(error => {
                    reject(error);
                })
            })
        },
        getPending: function () {
            return new Promise((resolve, reject) => {
                this.database.ReadAll("Pending").then(data => {
                    let oModel = new JSONModel({
                        forms: data
                    });
                    this.setModel(oModel, "pending");
                    resolve();
                }).catch(error => {
                    reject(error);
                })
            })
        },
        onContext_Delete: function (oEvent) {
            let listItem = this._oOpener.getParent().getParent();
            listItem.setBusy(true);
            let oBindingContext = listItem.getBindingContext("downloads");
            let value = oBindingContext.getObject();
            this.database.Delete("Downloads", value.ScriptID).then(() => {
                MessageToast.show(this.i18n("DEL_SUCCESS"));
            }).catch(error => {
                console.error(error);
                this.newMessage({
                    message: this.i18n("DEL_ERROR"),
                    description: error.message,
                    type: "Error",
                });
            }).finally(() => {
                listItem.setBusy(false);
                this.getDownloadBinding();
            })
        },
        onContext_Download: function (oEvent) {
            if (!this.tabKey === "Browse") {
                MessageToast.show(this.i18n("ERR_NA"))
            } else {
                let listItem = this._oOpener.getParent().getParent();
                listItem.setBusy(true);
                let oBindingContext = listItem.getBindingContext("results-read");
                let value = oBindingContext.getValue();
                this.getScript(value.ScriptID).then(response => {
                    response.id = value.ScriptID;
                    return this.database.Create("Downloads", response);
                }).then(() => {
                    MessageToast.show(this.i18n("DL_SUCCESS"));
                }).catch(error => {
                    console.error(error);
                    this.newMessage({
                        message: this.i18n("DL_ERROR"),
                        description: error.message,
                        type: "Error",
                    });
                }).finally(() => {
                    listItem.setBusy(false);
                })
            }
        }
    });
});