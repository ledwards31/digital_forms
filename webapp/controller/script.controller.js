/* global draw2d:true */
/* global Connection:true */
/* global InterceptorPolicy:true */
/* global TooltipFigure:true */
sap.ui.define([
    "com/ven/iris/FormRenderer/controller/BaseController",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel",
    "com/ven/iris/FormRenderer/common/ActionClass",
    "com/ven/iris/FormRenderer/common/ControlDictionary",
    "com/ven/iris/FormRenderer/common/Utilities",
    "sap/ui/layout/form/SimpleForm",
    "com/ven/iris/FormRenderer/common/control/dxc/ui/Wizard/Wizard",
    "com/ven/iris/FormRenderer/common/control/dxc/ui/Wizard/WizardStep",
    "sap/ui/model/odata/v4/ODataModel",
    "sap/base/util/UriParameters",
    "com/ven/iris/FormRenderer/common/dependencies/uuid/uuidv4.min",
    "com/ven/iris/FormRenderer/common/Order"
], function (Controller, MessageBox, MessageToast, JSONModel, ActionClass, ControlDictionary, Utilities, SimpleForm, Wizard, WizardStep, ODataModel, UriParameters, UUID, Order) {
    "use strict";
    return Controller.extend("com.ven.iris.FormRenderer.controller.script", {
        oArgs: null,
        oOps: new Map(),
        urlq: [],
        /**
        * Initialiser
        */
        _onInit: function (oEvent) {
            window.oController = this;
            this.setBusy(true);
            this._onRouteMatched(oEvent);
            ControlDictionary.setOController(this);
            this.getServices();
            let instance = UriParameters.fromURL(window.location.href);
            let urlq = instance.keys();
            for (let item of urlq) {
                let all = instance.getAll(item)
                this.urlq[item] = all.length > 1 ? all : all[0];
            }

            // var that = this;
            // var keys = Object.keys(that.getModel("form").getData());
            // urlq.forEach(itemPassedKey => {
            //     keys.forEach(stepId => {
            //         var step = that.getModel("form").getData()[stepId];
            //         step.parameters.forEach(param => {
            //             if (param.ParameterDesc === itemPassedKey) {
            //                 console.log("Found");
            //                 param.ParameterValue = this.urlq[itemPassedKey];
            //             }
            //         })
            //     });
            // });
            // that.getModel("form").refresh(true);

            this.order = new Order(this, this.database);
        },
        _onExit: function (oEvent) {
            let oWizard = this.byId("ContentWizard");
            if (oWizard) {
                oWizard.destroySteps();
                oWizard.destroy();
            }
        },
        getServices: function () {
            this.serviceURL = this.getOwnerComponent().getManifestEntry("/sap.app/dataSources/api/uri");
            this.apiDataModel = new ODataModel({
                serviceUrl: this.serviceURL,
                synchronizationMode: "None",
                operationMode: sap.ui.model.odata.OperationMode.Server,
                groupId: "$direct"
            });
            this.getView().setModel(this.apiDataModel, "apiDataModel");
        },
        _onRouteMatched: async function (oEvent) {
            this.oArgs = oEvent.getParameter("arguments");
            this.temp.route = [];
            this.onReady();
        },
        onReady: function () {
            if (this.oArgs.type === this.nRoutes.new) {
                if (navigator.onLine) {
                    this.getScript(this.oArgs.id).then(response => {
                        if (!response.Script) this.getRouter().getTargets().display("notFound");
                        let oView = this.getView();
                        let form = new JSONModel({});
                        let oModel = new JSONModel(response);
                        if (response.Script || response.ConFloAssConfig) this.gEntity = response.Script.Entity ? response.Script.Entity : response.ConFloAssConfig.Entity;
                        oView.setModel(oModel, "script");
                        oView.setModel(form, "form");
                        this.popWizard();
                    });
                } else {
                    this.getRouter().navTo("offline");
                }
            } else if (this.oArgs.type === this.nRoutes.run) {
                /* getRun IS IDENTICAL TO getScript */
                if (navigator.onLine) {
                    this.getRun(this.oArgs.id).then(response => {
                        if (!response.Script) this.getRouter().getTargets().display("notFound");
                        let oView = this.getView();
                        let form = new JSONModel({});
                        let oModel = new JSONModel(response);
                        if (response.Script || response.ConFloAssConfig) this.gEntity = response.Script.Entity ? response.Script.Entity : response.ConFloAssConfig.Entity;
                        oView.setModel(oModel, "script");
                        oView.setModel(form, "form");
                        this.popWizard();
                    });
                } else {
                    this.getRouter().navTo("offline");
                }
            } else if (this.oArgs.type === this.nRoutes.local) {
                this.getDownload(this.oArgs.id).then(response => {
                    let oView = this.getView();
                    let form = new JSONModel({});
                    let oModel = new JSONModel(response);
                    if (response.Script || response.ConFloAssConfig) this.gEntity = response.Script.Entity ? response.Script.Entity : response.ConFloAssConfig.Entity;
                    oView.setModel(oModel, "script");
                    oView.setModel(form, "form");
                    this.popWizard();
                }).catch(error => {
                    this.fatal(error);
                });
            } else if (this.oArgs.type === this.nRoutes.prefill) {
                this.getPending(this.oArgs.id).then(response => {
                    let oView = this.getView();
                    let form = new JSONModel({});
                    let oModel = new JSONModel(response.formResults);
                    if (response.Script || response.ConFloAssConfig) this.gEntity = response.Script.Entity ? response.Script.Entity : response.ConFloAssConfig.Entity;
                    oView.setModel(oModel, "script");
                    oView.setModel(form, "form");
                    this.popWizard();
                }).catch(error => {
                    this.fatal(error);
                });
            }
        },
        popData: function (data, models) {
            return new Promise(resolve => {
                let form = models[0],
                    script = models[1];
                for (var i = 0; i < script.ScriptSteps.length; i++) {
                    let step = script.ScriptSteps[i];
                    let field = form[step.StepID];
                    let question;
                    if (field) question = script.ScriptStepFields.find(field => field.StepID === form.StepID && field.StepFieldName === "question");
                    data.formResults.scriptResults.push({
                        ScriptID: script.Script.ScriptID,
                        StepID: step.StepID,
                        RunID: data.formResults.scriptResultsHeader.RunID,
                        LoopParam1: "",
                        LoopParam2: "",
                        User: "",
                        DateTime: new Date().toISOString(),
                        StepType: step.StepType,
                        Description: step.DescriptionShort,
                        DescriptionShort: step.DescriptionShort,
                        DescriptionLong: step.DescriptionLong,
                        SectionID: step.SectionID,
                        SectionOrder: step.SectionOrder,
                        Comments: step.Comments,
                        Question: question ? question.StepFieldValue : "",
                        Version: step.Version,
                        Entity: step.Entity,
                        Status: "",
                        StatusID: 0
                    })
                }
                resolve();
            })
        },
        popRoute: function (data, models) {
            return new Promise(resolve => {
                let form = models[0],
                    script = models[1],
                    uuid = uuidv4();
                for (var i = 0; i < this.temp.route.length; i++) {
                    let section = this.temp.route[i];
                    data.formResults.scriptResultsMultiParam.push({
                        ScriptID: script.Script.ScriptID,
                        StepID: section,
                        RunID: data.formResults.scriptResultsHeader.RunID,
                        ParameterType: "RouteParam",
                        ParameterID: uuid,
                        ParameterDesc: "",
                        ParameterValue: section,
                        Notes: "",
                        Version: 2,
                        Entity: "",
                        TargetParameter: "",
                        ParameterValueDesc: "",
                        Counter: i
                    })
                }
                resolve(uuid);
            });
        },
        popParams: function (data, models) {
            return new Promise(resolve => {
                let form = models[0],
                    script = models[1];
                for (var i = 0; i < script.ScriptSteps.length; i++) {
                    let step = script.ScriptSteps[i];
                    let field = form[step.StepID];
                    if (field) {
                        for (let parameter of field.parameters) {
                            if (parameter.ParameterID) {
                                data.formResults.scriptResultsMultiParam.push({
                                    ScriptID: script.Script.ScriptID,
                                    StepID: step.StepID,
                                    RunID: data.formResults.scriptResultsHeader.RunID,
                                    ParameterType: parameter.ParameterType,
                                    ParameterID: parameter.ParameterID,
                                    ParameterDesc: parameter.ParameterDesc,
                                    ParameterValue: parameter.ParameterValue,
                                    Notes: parameter.Notes,
                                    Version: parameter.Version,
                                    Entity: parameter.Entity,
                                    TargetParameter: parameter.TargetParameter,
                                    //TargetOrder: parameter.TargetOrder,
                                    ParameterValueDesc: "",
                                    Counter: (parameter.Counter) ? parameter.Counter : -1
                                })
                            }
                        }
                    }
                }
                resolve();
            })
        },
        getSave: function (runId) {
            return new Promise((resolve, reject) => {
                let form = this.getModel("form").getData();
                let script = this.getModel("script").getData();
                let models = [form, script];
                let data = {
                    metadata: script,
                    formResults: {
                        scriptResultsHeader: {
                            RunID: runId,
                            ScriptID: script.Script.ScriptID,
                            Entity: script.Script.Entity,
                            Origin: "",
                            Status: "",
                            ScriptAction: "",
                            StatusID: 0,
                            IsSubscript: "",
                            AssignedToGroup: ""
                        },
                        scriptResults: [],
                        scriptResultsMultiParam: [],
                        scriptResultsStepsHierarchy: script.StepsHierarchy.map(element => {
                            element.RunID = runId;
                            return element;
                        }),
                        scriptResultsStepField: script.ScriptStepFields.map(element => {
                            element.RunID = runId;
                            return element;
                        }),
                    }
                }
                this.popData(data, models).then(() => {
                    Promise.all([
                        this.popParams(data, models),
                        this.popRoute(data, models)
                    ])
                }).then(() => {
                    resolve(data)
                }).catch(error => {
                    reject(error);
                })
            })
        },
        popWOM: function (scriptId) {
            this.order.fetch(e => {
                let script = this.getModel("script").getData();
                if (e) {
                    this.order.attachment(scriptId, script.Description);
                }
            }).catch(error => {
                this.newMessage({
                    message: this.i18n("ERR_WOM_UPDATE"),
                    description: error,
                    type: "Error",
                });
            });
        },
        onSave: function (oEvent, b401) {
            let state = {
                b401: false
            };
            let runId;
            this.getRunID(state).then(runId => {
                return this.getSave(runId);
            }).then(async data => {
                this.setBusy(true);
                runId = data.formResults.scriptResultsHeader.RunID;
                if (window.navigator.onLine && !state.b401) {
                    return Promise.all([
                        this.postScript(data, this),
                        this.putFile(runId)
                    ])
                } else {
                    let scriptId = data.formResults.scriptResultsHeader.ScriptID;
                    let timestamp = new Date();
                    let files = await this.getRaw(runId).catch(error => this.fatal(this.i18n("FATAL"), error));
                    data.id = runId;
                    data.RunID = runId;
                    data.ScriptID = scriptId;
                    data.Description = `${scriptId} (${timestamp.toLocaleDateString()})`;
                    let jobs = [this.database.Create("Pending", data)];
                    files.forEach(file => {
                        jobs.push(this.database.Create("Files", file))
                    });
                    let actions = await Promise.all(this.pActions.map(f => f()));
                    this.queue.forEach(method => method(runId, scriptId));
                    this.popWOM(scriptId);
                    return Promise.all(jobs);
                }
            }).then(() => {
                if (state.b401) {
                    MessageToast.show(this.i18n("401_DELAY_OK"));
                    setTimeout(() => {
                        window.location.assign(`${window.location.origin}/index.html#/list`);
                        location.reload();
                    }, 3000);
                } else {
                    let sMsg = this.i18n(window.navigator.onLine ? "DATA_STORE_OK" : "DATA_POST_OK");
                    MessageToast.show(sMsg);
                    if (this.urlq.src === "wom") {
                        let operationNo = this.urlq.operation || this.urlq["OP Number"];
                        window.location.href = `${window.location.origin}/comventiamobApps/index.html#/order/${this.urlq.Workorder}/${operationNo}`;
                        // window.location.href = `${window.location.origin}/comventiamobApps/index.html#/order/${this.urlq.Workorder}/${this.urlq.operation}`
                    } else {
                        this.getRouter().navTo("list");
                    }
                }
                this.oOps = new Map();
            }).catch(error => {
                if (error.message && error.message.includes("401")) {
                    this.onSave(null, true);
                } else {
                    this.fatal(error);
                }
            }).finally(() => {
                this.setBusy(false);
            });

        },
        getTitle: function (oModel) {
            let data = oModel.getData();
            let title = data.Script.Description;
            this.setTitle(title);
        },
        goBack: function () {
            if (this.urlq.src === "wom") {
                let operationNo = this.urlq.operation || this.urlq["OP Number"];
                window.location.href = `${window.location.origin}/comventiamobApps/index.html#/order/${this.urlq.Workorder}/${operationNo}`;
            } else {
                MessageBox.show(
                    `${this.i18n("ONNAV_DATLOSS")}`, {
                    id: this.createId("change"),
                    icon: MessageBox.Icon.WARNING,
                    styleClass: "dxcUiDialog",
                    title: this.i18n("ONNAV_DATLOSS_TITLE"),
                    actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                    emphasizedAction: MessageBox.Action.NO,
                    onClose: (oAction) => {
                        if (oAction === MessageBox.Action.YES) {
                            this.getRouter().navTo("list");
                        }
                    }
                })
            }
        },
        /**
        * Initialise the Wizard.
        */
        popWizard: function () {
            var that = this;
            let prefill = this.oArgs.type === this.nRoutes.run || this.oArgs.type === this.nRoutes.prefill;
            let oModel = this.getView().getModel("script");
            this.getTitle(oModel);
            this.getSteps(oModel).then((aStep) => {
                let oContainer = this.byId("idContainer");
                let oWizard = this.byId("ContentWizard");
                if (oWizard) {
                    try {
                        oWizard.removeAllSteps();
                        oWizard.destroy();
                        oWizard = null;
                    } catch (error) {
                        console.error(error);
                    }
                }
                if (!oWizard) {
                    oContainer.addItem(new Wizard({
                        steps: aStep,
                        id: this.createId("ContentWizard"),
                        finishButtonText: this.i18n(!prefill ? "SUBMIT_FORM" : "GO_BACK"),
                        enableBranching: !prefill,
                        complete: (e) => !prefill ? this.onSave(e) : this.goBack()
                    }).addStyleClass("sapUiResponsivePadding--header sapUiResponsivePadding--content"));
                } else {
                    try {
                        oWizard.removeAllSteps();
                        oWizard.destroySteps();
                    } catch (e) {
                        console.error(e);
                    }
                    aStep.forEach(step => {
                        oWizard.addStep(step);
                    });
                }

                return this.getOps(oModel);
            }).then(() => {
                var that = this;
                var urlq = Object.keys(that.urlq);
                var keys = Object.keys(that.getModel("form").getData());
                urlq.forEach(itemPassedKey => {
                    keys.forEach(stepId => {
                        var step = that.getModel("form").getData()[stepId];
                        step.parameters.forEach(param => {
                            if (param.ParameterDesc === itemPassedKey) {
                                console.log("Found");
                                param.ParameterValue = this.urlq[itemPassedKey];
                            }
                        })
                    });
                });
                that.getModel("form").refresh(true);
                this.setBusy(false);
            }).catch(error => {
                that.fatal(error);
            });
        },
        /**
        * Instantiates a WizardStep.
        * @param {Object} data 
        * @returns {sap.m.WizardStep} WizardStep
        */
        getWizardStep: function (oModel, data, p) {
            return new Promise(async (resolve) => {
                let fields = await this.getFields(oModel, data.sectionId).catch(error => this.fatal(error));
                data.validated = !fields.getContent().some(e => {
                    if (e.getRequired && !["sap.m.RadioButtonGroup", "sap.m.RadioButton"].includes(e.getMetadata()._sClassName)) {
                        return e.getRequired();
                    }
                });
                let parameter = {
                    id: data.id,
                    title: Utilities.oc(data, "title", ""),
                    validated: data.validated,
                    content: fields,
                    subsequentSteps: data.subsequentSteps,
                    complete: data.complete,
                    activate: data.activate
                }
                let res = new WizardStep(parameter);
                resolve(res);
            })
        },
        /**
        * Get the Settings Object to be used to instantiate a Control.
        * @param {sap.ui.model.json.JSONModel} oModel 
        * @param {string} StepFieldName 
        * @returns {Object} settings
        */
        getSettings: function (oModel, step) {
            return new Promise(async (resolve) => {
                let data = oModel.getData();
                let stepId = step.StepID;
                let sectionId = step.SectionID;
                let fields = data.ScriptStepFields.filter(field => field.StepID === step.StepID);
                let parameters = data.ScriptStepParameters.filter(e => e.StepID === stepId && e.ParameterType === "FieldParameter");
                let filterParameters = data.ScriptStepParameters.filter(e => e.StepID === stepId && e.ParameterType.indexOf("Filter") >= 0);
                let res = {
                    fieldGroupIds: [stepId, sectionId]
                };
                if (filterParameters) {
                    res.filterParameters = filterParameters;
                }
                for (let field of fields) {
                    res[field.StepFieldName] = field.StepFieldValue;
                }
                if (step.StepType === this.controls.Table) {
                    await this.popTable(res, fields, stepId, parameters).catch(error => this.fatal(this.i18n("FATAL"), error));
                } else if (step.StepType === this.controls.ComboBox) {
                    await this.popCombo(res, fields, this.input.ComboBox, stepId, parameters).catch(error => this.fatal(this.i18n("FATAL"), error));
                } else if (step.StepType === this.controls.Radio) {
                    await this.popCombo(res, fields, this.input.Radio, stepId, parameters).catch(error => this.fatal(this.i18n("FATAL"), error));
                } else if (step.StepType === this.controls.CheckBox) {
                    await this.popCombo(res, fields, this.input.CheckBox, stepId, parameters).catch(error => this.fatal(this.i18n("FATAL"), error));
                }
                resolve(res);
            });
        },
        popCombo: async function (res, fields, control, stepId, parameters) {
            return new Promise(async (resolve, reject) => {
                res.options = [];
                let re = new RegExp(`(?:${control})(([A-Z][A-Za-z]*)([A-Z][A-Za-z]*))(?:_)([0-9]*)(?:_?)([0-9]*)`);
                for (var i = 0; i < fields.length; i++) {
                    let field = fields[i];
                    let match = re.exec(field.StepFieldName);
                    if (match && match.length > 0) {
                        let name = match[1],
                            target = match[2].toLowerCase(),
                            parameter = match[3].toLowerCase(),
                            index = match[4];
                        if (!name || !target || !parameter || !index) continue;
                        let template = {};
                        template[parameter] = field.StepFieldValue;
                        if (parameters && parameters.length > 0) {
                            res.pId = parameters[0].ParameterValue;
                        }
                        if (target === "text") {
                            if (parameter === "content") {
                                if (control === "checkbox") {
                                    let path = await this.popBinding(stepId, i, field.StepFieldValue).catch(error => this.fatal(this.i18n("FATAL"), error));
                                    template.value = `{form>${path}}`;
                                }
                                res.options[index] = Object.assign(template, res.options[index]);
                            }
                        }
                    }
                }
                resolve(res);
            })
        },
        getCells: function (res, index) {
            if (!res.rows[index]) {
                res.rows[index] = {
                    cells: []
                };
            } else if (!res.rows[index].cells) {
                res.rows[index].cells = [];
            }
            return res;
        },
        popTable: async function (res, fields, stepId, parameters) {
            return new Promise((resolve, reject) => {
                res.columns = [], res.rows = [], res.fields = [];
                let re = /(?:table)(([A-Z][A-Za-z]*)([A-Z][A-Za-z]*))(?:_)([0-9]*)(?:_?)([0-9]*)/;
                for (var i = 0; i < fields.length; i++) {
                    let field = fields[i];
                    let match = re.exec(field.StepFieldName);
                    if (match && match.length > 0) {
                        let name = match[1],
                            target = match[2].toLowerCase(),
                            parameter = match[3].toLowerCase(),
                            index = match[4];
                        if (!name || !target || !parameter || !index) continue;
                        let template = {};
                        template[parameter] = field.StepFieldValue;
                        if (target === "column") {
                            template.pid = parameters.find(e => RegExp(`(?:_${index})$`, 'g').test(e));
                            res.columns[index] = Object.assign(template, res.columns[index]);
                        } else if (target === "input") {
                            res.columns[index] = Object.assign(template, res.columns[index]);
                        } else if (target === "text") {
                            let column = index;
                            if (parameter === "content") {
                                let row = match[5];
                                this.getCells(res, row);
                                res.rows[row].cells[column] = Object.assign(template, res.rows[row].cells[column]);
                            } else if (parameter === "setting") {
                                if (!res.fields[column]) res.fields[column] = [];
                                delete template[parameter];
                                template[`${target}_${parameter}`] = field.StepFieldValue;
                                res.columns[column] = Object.assign(template, res.columns[column]);
                            } else {
                                let row = index;
                                res.rows[row] = Object.assign(template, res.rows[row]);
                            }
                        } else if (target === "dropdowntext" || target === "checkboxtext") {
                            let column = index;
                            if (parameter === "content") {
                                let order = match[5];
                                if (!res.fields[column]) res.fields[column] = [];
                                res.fields[column][order] = Object.assign(template, res.fields[column][order]);
                            }
                        } else if (target === "display") {
                            let column = index;
                            if (parameter === "setting") {
                                if (!res.fields[column]) res.fields[column] = [];
                                delete template[parameter];
                                template[`${target}_${parameter}`] = field.StepFieldValue;
                                res.columns[column] = Object.assign(template, res.columns[column]);
                            }
                        }
                    }
                }
                resolve(res);
            })
        },
        setProperties: function (oModel, stepId) {
            return new Promise(resolve => {
                let data = oModel.getData();
                let form = this.getModel("form");
                let template = {
                    parameters: []
                };
                let parameters = data.ScriptStepParameters.filter(parameter => parameter.StepID === stepId);
                for (var i = 0; i < parameters.length; i++) {
                    let parameter = parameters[i];
                    template.parameters.push({
                        ParameterValue: parameter.ParameterValue,
                        ParameterID: parameter.ParameterID,
                        ParameterType: parameter.ParameterType,
                        TargetParameter: parameter.TargetParameter,
                        Notes: parameter.Notes,
                        TargetOrder: parameter.TargetOrder,
                        Entity: parameter.Entity,
                        Version: parameter.Version,
                        Counter: parameter.Counter,
                        ParameterDesc: parameter.ParameterDesc
                    });
                }
                // form.setProperty(`/${stepId}`, template);
                //Fix for multiple checkboxes
                if (data.ScriptSteps.filter(x => x.StepID === stepId)[0].StepType !== "28") {
                    form.setProperty(`/${stepId}`, template);
                }
                resolve(template);
            })
        },
        _v: function (source, sectionId) {
            let oWizardStep = this.byId(`ID_${sectionId}`);
            let fields = sap.ui.getCore().byFieldGroupId(sectionId);
            let valid = !fields.some(e => {
                if (["sap.m.CheckBox"].includes(e.getMetadata()._sClassName)) {
                    let model = this.getModel("form");
                    let stepId = e.data("stepId");
                    let required = e.data("required");
                    let property = model.getProperty(`/${stepId}/parameters`);
                    return required && !property.some(e => e.ParameterValue);
                } else if (["sap.m.Select"].includes(e.getMetadata()._sClassName)) {
                    return e.getRequired() && !e.getSelectedKey();
                } else if (e.getRequired && e.getValue) {
                    return e.getRequired() && !e.getValue();
                }
            });
            let checkboxes = fields.filter(e => ["sap.m.CheckBox"].includes(e.getMetadata()._sClassName));
            checkboxes.forEach(cb => this.setValueState(cb, "None"));
            if (source) this.setValueState(source, "None");
            oWizardStep.setValidated(valid);
            oWizardStep._getWizardParent().setShowNextButton(valid);
        },
        _iv: function (source, sectionId) {
            let oWizardStep = this.byId(`ID_${sectionId}`);
            this.setValueState(source, "Error", this.i18n("REQUIRED"));
            let response = false;
            if (["sap.m.CheckBox"].includes(source.getMetadata()._sClassName)) {
                let model = this.getModel("form");
                let stepId = source.data("stepId");
                let property = model.getProperty(`/${stepId}/parameters`);
                response = property.some(e => e.ParameterValue);
            }
            oWizardStep._getWizardParent().setShowNextButton(response);
            oWizardStep.setValidated(response);
        },
        getValidation: async function (controls, sectionId) {
            if (!Array.isArray(controls)) controls = [controls];
            let traverse = (control) => {
                return new Promise(resolve => {
                    let children = [];
                    let dig = (_control) => {
                        if (_control.getMetadata && ["sap.m.VBox", "sap.m.HBox"].includes(_control.getMetadata()._sClassName)) {
                            let items = _control.getItems();
                            for (let item of items) {
                                dig(item);
                            }
                        } else {
                            children.push(_control);
                        }
                    }
                    dig(control);
                    resolve(children);
                })
            }

            for (let control of controls) {
                let children = await traverse(control);
                children.forEach(child => {
                    if (child.attachChange) {
                        child.attachChange(oEvent => {
                            let parameters = oEvent.getParameters();
                            let source = oEvent.getSource();
                            let value = parameters.value || parameters.selectedItem;
                            !value ? this._iv(source, sectionId) : this._v(source, sectionId);
                        })
                    } else if (child.attachSelect && ["sap.m.CheckBox"].includes(child.getMetadata()._sClassName)) {
                        child.attachSelect(oEvent => {
                            let parameters = oEvent.getParameters();
                            let source = oEvent.getSource();
                            // !parameters.selected ? iv(source, sectionId) : v(source, sectionId);
                            let fields = sap.ui.getCore().byFieldGroupId(sectionId);
                            let checkboxes = fields.filter(e => ["sap.m.CheckBox"].includes(e.getMetadata()._sClassName));
                            let valid = checkboxes.filter(cb => cb.getSelected()).length > 0;
                            !valid ? this._iv(source, sectionId) : this._v(source, sectionId);
                        })
                    } else if (child.attachSelect && !["sap.m.RadioButtonGroup", "sap.m.RadioButton"].includes(child.getMetadata()._sClassName)) {
                        child.attachSelect(oEvent => {
                            let parameters = oEvent.getParameters();
                            let source = oEvent.getSource();
                            !parameters.value ? this._iv(source, sectionId) : this._v(source, sectionId);
                        })
                    }
                })
            }
        },
        getValidValues(id) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("script-read");
                let sPath = `/getValidValues(...)`;
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                oOperation.setParameter("parameterId", id);
                oOperation.execute().then(() => {
                    let response = oOperation.getBoundContext().getObject();
                    resolve(response.value);
                }).catch((error) => {
                    reject(error);
                });
            })
        },
        /**
        * Get an instance of a Control required to service the field.
        * @param {sap.ui.model.json.JSONModel} oModel 
        * @param {Object} field 
        */
        getControl: function (oModel, step) {
            return new Promise((resolve, reject) => {
                let data = oModel.getData();
                let stepId = step.StepID;
                this.getSettings(oModel, step).then(async settings => {
                    let map = ControlDictionary.controls;
                    if (map.has(step.StepType)) {
                        let method = map.get(step.StepType);
                        let sectionId = step.SectionID;
                        let template = await this.setProperties(oModel, stepId).catch(error => this.fatal(this.i18n("FATAL"), error));
                        let index = template.parameters.findIndex(e => e.ParameterType === "FieldParameter");
                        if (index >= 0) {
                            let path = `/${stepId}/parameters/${index}`;
                            settings.stepId = stepId;
                            if (template.parameters && template.parameters.length > 0) settings.pId = template.parameters[index].ParameterID;
                            settings.path = index >= 0 ? path : null;
                            settings.value = {
                                path: `form>/${stepId}/parameters/${index}/ParameterValue`,
                            }
                            settings.entity = this.gEntity;
                            let control = await method(settings);
                            let mand = Object.keys(settings).find(e => e.toLowerCase().includes("mandatory") && settings[e] == "true");
                            if (settings.required || mand) this.getValidation(control, sectionId);
                            resolve(control);
                        } else {
                            reject(`${this.i18n("ERROR_FIELDPARAM01")} stepId: ${stepId}, sectionId: ${step.SectionID}`);
                        }
                    } else {
                        resolve()
                    }
                }).catch(error => {
                    reject(error);
                })
            })
        },

        /**
        * Get an Array of the Fields (Many-to-One Relationship) related to a Step.
        * @param {sap.ui.model.json.JSONModel} oModel 
        * @param {string} StepID 
        * @returns {Array} fields
        */
        getFields: function (oModel, sectionId) {
            return new Promise((resolve, reject) => {
                //Method to getFields from ScriptStepFields.
                let data = oModel.getData();
                let steps = this.getOrder(oModel, sectionId);
                let jobs = []
                for (let step of steps) {
                    jobs.push(this.getControl(oModel, step));
                }
                Promise.all(jobs).then(res => {
                    res = res.filter(e => e).flat();
                    resolve(new SimpleForm({
                        content: res,
                        layout: "ResponsiveGridLayout",
                        labelSpanXL: 4,
                        labelSpanL: 3,
                        labelSpanM: 4,
                        labelSpanS: 12,
                        adjustLabelSpan: false,
                        emptySpanXL: 0,
                        emptySpanL: 4,
                        emptySpanM: 0,
                        emptySpanS: 0,
                        columnsXL: 1,
                        columnsL: 1,
                        columnsM: 1,
                        singleContainerFullSize: false
                    }));
                }).catch(error => {
                    reject(error);
                });
            })
        },
        getOrder: function (oModel, sectionId) {
            let data = oModel.getData();
            let list = data.ScriptSteps.filter(e => e.SectionID === sectionId);
            return list.sort((a, b) => a.SectionOrder - b.SectionOrder);

        },
        getChildRef: function (oModel, id) {
            let data = oModel.getData();
            let next = data.StepsHierarchy.filter(e => e.ParentStepID === id);
            let values = [];
            next.map(e => {
                let child = data.ScriptSteps.find(x => x.StepID === e.ChildStepID);
                if (child) values.push(child);
            })
            return values;
        },
        getChild: function (oModel, id) {
            let data = oModel.getData();
            let next = data.StepsHierarchy.filter(e => e.ParentStepID === id);
            let values = [];
            next.map(e => {
                let child = data.StepsHierarchy.find(x => x.ParentStepID === e.ChildStepID);
                if (child) values.push(child);
            })
            return values;
        },
        getSubsequent: function (oModel, sectionId, sections) {
            return new Promise((resolve, reject) => {
                let data = oModel.getData();
                let children = data.StepsHierarchy.filter(e => e.ParentStepID === sectionId);
                let values = [];
                for (let child of children) {
                    let step = data.ScriptSteps.find(e => e.StepID === child.ChildStepID);
                    let index = 0;
                    while (step.StepType != "19" && index <= 5) {
                        index++
                        let next = data.StepsHierarchy.find(e => e.ParentStepID === step.StepID);
                        if (next)
                            step = data.ScriptSteps.find(e => e.StepID === next.ChildStepID);
                    }
                    let exists = sections.find(e => e.StepID === step.StepID);
                    let id = this.createId(`ID_${step.StepID}`);
                    if (exists && !values.includes(id)) {
                        values.push(id);
                    }
                }
                resolve(values);
            })
        },
        getCondition: function (method, parameter, value) {
            if (method === "not") {
                return parameter != value
            } else if (method === "contains") {
                return value.includes(parameter);
            }
        },
        getMatches: function (method, parameters, value) {
            return new Promise(resolve => {
                parameters = parameters.split(",");
                let response = false;
                for (let parameter of parameters) {
                    response = this.getCondition(method, parameter, value);
                    if (response) break;
                }
                resolve(response);
            })
        },
        validate: function (parameter, field) {
            let paramTemp = parameter;
            return new Promise(async (resolve, reject) => {
                let stepId;
                if (Array.isArray(field)) {
                    stepId = field;
                } else {
                    stepId = field.StepID;
                }
                this.getParameter(stepId, "FieldParameter").then(async res => {
                    let paramValue = res.ParameterValue;
                    if (typeof paramValue === 'boolean') {
                        paramValue = res.TargetParameter;
                    }
                    if (parameter.ParameterType === "Condition") {
                        resolve(paramValue === parameter.ParameterValue);
                    } else if (paramTemp.ParameterType === "Formula") {
                        let funct = /([A-Za-z]*)\(([\w\d,]*)\)/g
                        //let param = /([A-Za-z0-9@#$%&']+)(?:,?)/g
                        let matches = funct.exec(paramTemp.ParameterValue);
                        let method = matches[1];
                        parameter = matches[2];
                        let is = await this.getMatches(method, parameter, paramValue).catch(error => this.fatal(this.i18n("FATAL"), error));
                        resolve(is);
                    } else {
                        resolve(false);
                    }
                }).catch(error => {
                    reject(error);
                });
            })
            // return new Promise(async resolve => {
            //     let stepId = field.StepID;
            //     this.getParameter(stepId, "FieldParameter").then(async res => {
            //         let paramValue = res.ParameterValue;
            //         if (typeof paramValue === 'boolean') {
            //             paramValue = res.TargetParameter;
            //         }
            //         if (parameter.ParameterType === "Condition") {
            //             resolve(paramValue === parameter.ParameterValue);
            //         } else if (parameter.ParameterType === "Formula") {
            //             let funct = /([A-Za-z]*)\(([\w\d,]*)\)/g
            //             //let param = /([A-Za-z0-9@#$%&']+)(?:,?)/g
            //             let matches = funct.exec(parameter.ParameterValue);
            //             let method = matches[1],
            //                 parameter = matches[2];
            //             let is = await this.getMatches(method, parameter).catch(error => this.fatal(this.i18n("FATAL"), error));
            //             resolve(is);
            //         } else {
            //             resolve(false);
            //         }
            //     }).catch(error => {
            //         reject(error);
            //     });
            // })
        },
        discardProgress: function (oWizard, oStep) {
            oWizard.discardProgress(oStep);
            oWizard.setCurrentStep(oStep);
            oWizard.goToStep(oStep);
            oStep._oNextButton.setVisible(true);
        },
        getParameter: function (stepIds, parameter) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("form");
                let data = oModel.getData();
                let value;// = data[stepId].parameters.find(e => e.ParameterType === parameter);
                if (Array.isArray(stepIds)) {
                    for (var i = 0; i < stepIds.length; i++) {
                        let stepId = stepIds[i].StepID;
                        if (!data[stepId]) reject(`${this.i18n("ERROR_NOPARAM")} stepId: ${stepId}`);
                        var valueArr = data[stepId].parameters.filter(e => e.ParameterType === parameter);
                        if (valueArr.length === 1) {
                            value = valueArr[0];
                        } else {
                            value = data[stepId].parameters.find(e => e.ParameterType === parameter && e.ParameterValue);
                        }
                        if (value.ParameterValue) {
                            resolve(value);
                            break;
                        }
                    }
                } else {
                    let stepId = stepIds;
                    if (!data[stepId]) reject(`${this.i18n("ERROR_NOPARAM")} stepId: ${stepId}`);
                    var valueArr = data[stepId].parameters.filter(e => e.ParameterType === parameter);
                    if (valueArr.length === 1) {
                        value = valueArr[0];
                    } else {
                        value = data[stepId].parameters.find(e => e.ParameterType === parameter && e.ParameterValue);
                    }
                    resolve(value);
                }

            });
        },
        getParameterIndex: function (stepId, parameter) {
            let oModel = this.getModel("form");
            let data = oModel.getData();
            let value = data[stepId].parameters.findIndex(e => e.ParameterType === parameter);
            return value;
        },
        onChange: async function (oEvent, stepId) {
            let oModel = this.getView().getModel("script");
            let data = oModel.getData();
            oEvent.getSource().setValueState("None");
            if (!this.byId("change")) {
                MessageBox.show(
                    `${this.i18n("ONCHANGE_WARN_DESC")}`, {
                    id: this.createId("change"),
                    icon: MessageBox.Icon.WARNING,
                    title: this.i18n("ONCHANGE_WARN_TITLE"),
                    actions: [MessageBox.Action.YES, MessageBox.Action.ABORT],
                    emphasizedAction: MessageBox.Action.ABORT,
                    onClose: (oAction) => {
                        if (oAction === MessageBox.Action.ABORT) {
                            let oModel = this.getModel("form");
                            let fieldParameter = this.getParameterIndex(stepId, "FieldParameter");
                            let historic = oModel.getProperty(`/${stepId}/${fieldParameter}/HistoricValue`);
                            oModel.setProperty(`/${stepId}/parameters/${fieldParameter}/ParameterValue`, historic);
                        } else {
                            let sectionId = data.ScriptSteps.find(e => e.StepID === stepId).SectionID;
                            let oWizard = this.byId("ContentWizard");
                            let oStep = this.byId(`ID_${sectionId}`);
                            let routeIndex = this.temp.route.findIndex(e => e === sectionId);
                            this.temp.route = this.temp.route.slice(0, routeIndex + 1);
                            this.discardProgress(oWizard, oStep);
                        }
                    }
                })
            }
        },
        onFocus: function (oEvent, stepId) {
            let source = oEvent.srcControl;
            let oModel = this.getModel("form");
            let fieldParameter = this.getParameterIndex(stepId, "FieldParameter");
            let value = oModel.getProperty(`/${stepId}/parameters/${fieldParameter}/ParameterValue`);
            oModel.setProperty(`/${stepId}/parameters/${fieldParameter}/HistoricValue`, value);
        },
        onComplete: function (oEvent) {
            return new Promise(async (resolve, reject) => {
                let oModel = this.getView().getModel("script");
                let data = oModel.getData();
                let source = oEvent.getSource();
                let subsequentSteps = source.getSubsequentSteps();
                let match = (s) => s.match(/ID_([A-Za-z0-9\-\_]*)/)[1];
                let stepId = match(source.getId());
                let controls = [];
                this.goOps(stepId);
                let nextStep = null;
                let directions = data.StepsHierarchy.filter(e => e.ParentStepID === (stepId));
                let directionChildSections = new Map();
                directions.forEach(direction => {
                    var section = data.StepsHierarchy.find(e => e.ParentStepID === direction.ChildStepID);
                    directionChildSections.set(section.ChildStepID, section);
                });
                if (subsequentSteps.length > 1) {
                    for (let subsequentStep of subsequentSteps) {
                        let decisionId = directionChildSections.get(match(subsequentStep));
                        if (!decisionId) {
                            decisionId = data.StepsHierarchy.find(e => e.ChildStepID === match(subsequentStep) && e.ParentStepTypeID === "9").ParentStepID;
                        } else {
                            decisionId = decisionId.ParentStepID;
                        }
                        let parameter = data.ScriptStepParameters.find(e => e.StepID === decisionId);
                        let field = data.ScriptStepParameters.find(e => e.ParameterID === parameter.ParameterID && e.ParameterType === "FieldParameter");
                        let fields = data.ScriptStepParameters.filter(e => e.ParameterID === parameter.ParameterID && e.ParameterType === "FieldParameter");
                        let valid;
                        if (fields && fields.length > 1) {
                            valid = await this.validate(parameter, fields).catch(error => this.fatal(error));
                        } else {
                            valid = await this.validate(parameter, field).catch(error => this.fatal(error));
                        }
                        let stepId = field.StepID;
                        let aControl = sap.ui.getCore().byFieldGroupId(stepId);
                        aControl.forEach(control => {
                            if (!valid) controls.push(control);
                            if (control.attachChange) {
                                control.addEventDelegate({
                                    onfocusin: (oEvent) => this.onFocus(oEvent, stepId)
                                });
                                control.attachChange(oEvent => this.onChange(oEvent, stepId));
                            } else if (control.attachSelect) {
                                control.addEventDelegate({
                                    onfocusin: (oEvent) => this.onFocus(oEvent, stepId)
                                });
                                control.attachSelect(oEvent => this.onChange(oEvent, stepId));
                            }
                        });
                        if (valid) {
                            nextStep = subsequentStep;
                            break;
                        }
                    }
                    if (nextStep) {
                        this.temp.route.push(match(nextStep));
                        resolve(source.setNextStep(nextStep))
                    } else {
                        this.setValueState(controls, "Error", this.i18n("NO_MATCH_DESC"));
                        reject(this.i18n("NO_MATCH_DESC"));
                    }
                } else {
                    nextStep = subsequentSteps[0];
                    this.temp.route.push(match(nextStep));
                    source._getWizardParent().setShowNextButton(true);
                    resolve(source.setNextStep(nextStep));
                }
            })
        },
        onActivate: function (oEvent, sectionId) {
            this._v(null, sectionId);
        },
        goOps: function (stepId) {
            return new Promise(resolve => {
                let oModel = this.getModel("form");
                let form = oModel.getData();
                let data = this.getModel("script").getData();
                if (this.oOps.has(stepId)) {
                    let confs = this.oOps.get(stepId);
                    let jobs = [];
                    for (let conf of confs) {
                        let many = [];
                        let method = this.fOps.get(conf.method);
                        for (let p of conf.request) {
                            let body = {
                                StepID: p.StepID,
                                Version: p.Version,
                                Entity: p.Entity,
                                ParameterID: p.ParameterID,
                                ParameterType: p.ParameterType,
                                ParameterValue: null,
                                TargetParameter: p.TargetParameter
                            }
                            if (p.ParameterValue) {
                                body.ParameterValue = p.ParameterValue;
                            } else {
                                let ref = data.ScriptStepParameters.find(e => e.ParameterID === p.ParameterID && e.StepID !== conf.stepId);
                                let step = form[ref.StepID];
                                if (step) {
                                    let value = step.parameters.find(e => e.ParameterID === ref.ParameterID).ParameterValue;
                                    body.ParameterValue = value;
                                }
                            }
                            many.push(body);
                        }
                        if (conf.type === "legacy") {
                            for (let p of conf.response) {
                                let body = {
                                    StepID: p.StepID,
                                    Version: p.Version,
                                    Entity: p.Entity,
                                    ParameterID: p.ParameterID,
                                    ParameterType: p.ParameterType,
                                    ParameterValue: p.ParameterValue,
                                    TargetParameter: p.TargetParameter
                                }
                                many.push(body);
                            }
                        }
                        if (conf.method === "5") {
                            this.pActions.push(method.bind(this, conf.url, many));
                        } else {
                            jobs.push(method.call(this, conf.url, many));
                        }
                    }
                    Promise.allSettled(jobs).then(values => {
                        for (var i = 0; i < values.length; i++) {
                            let index = values[i];
                            let status = index.status;
                            if (status === "fulfilled") {
                                let conf = confs[i];
                                if (conf.type === "modern") {
                                    let value = Array.isArray(index.value) ? index.value[0] : index.value;
                                    for (let p of conf.response) {
                                        let ref = data.ScriptStepParameters.find(e => e.ParameterID === p.ParameterID && e.StepID !== conf.stepId);
                                        let field = form[ref.StepID].parameters.findIndex(e => e.ParameterID === ref.ParameterID);
                                        oModel.setProperty(`/${ref.StepID}/parameters/${field}/ParameterValue`, value[p.TargetParameter ? p.TargetParameter : p.ParameterDesc]);
                                    }
                                } else if (conf.type === "legacy") {
                                    for (var value of index.value) {
                                        let ref = data.ScriptStepParameters.find(e => e.ParameterID === value.ParameterID && e.StepID !== conf.stepId);
                                        let field = form[ref.StepID].parameters.findIndex(e => e.ParameterID === ref.ParameterID);
                                        oModel.setProperty(`/${ref.StepID}/parameters/${field}/ParameterValue`, value.ParameterValue);
                                        //this.setParameterValue(value.ParameterID, value.ParameterValue);
                                    }
                                }
                            }
                        }
                        resolve();
                    });
                } else {
                    resolve();
                }
            })
        },
        getOps: function (oModel) {
            return new Promise(async (resolve, reject) => {
                let data = oModel.getData();
                let ops = data.ScriptSteps.filter(e => e.StepType === "5" || e.StepType === "10");
                let type = {
                    req: ["Filter", "Pass"],
                    res: ["RetCode"]
                }
                for (let op of ops) {
                    let hierarchy = data.StepsHierarchy.find(e => e.ChildStepID === op.StepID);
                    if (hierarchy) {
                        let parentStepId = hierarchy.ParentStepID;
                        let params = data.ScriptStepParameters.filter(e => e.StepID === op.StepID);
                        let field = data.ScriptStepFields.find(e => e.StepID === op.StepID && e.StepFieldName === "ODataService");
                        if (field) {
                            let conf = {
                                url: field.StepFieldValue,
                                type: ["GW", "HADM"].some(e => field.StepFieldValue.includes(e)) ? "legacy" : "modern",
                                stepId: op.StepID,
                                method: op.StepType,
                                request: [],
                                response: []
                            };
                            for (let param of params) {
                                param.TargetParameter = param.TargetParameter ? param.TargetParameter : param.ParameterDesc.replace(/\s/g, "");
                                if (type.req.includes(param.ParameterType)) {
                                    conf.request.push(param);
                                } else if (type.res.includes(param.ParameterType)) {
                                    conf.response.push(param);
                                }
                            }
                            if (this.oOps.has(parentStepId)) {
                                let confs = this.oOps.get(parentStepId);
                                confs.push(conf);
                                this.oOps.set(parentStepId, confs);
                            } else {
                                this.oOps.set(parentStepId, [conf]);
                            }
                        }
                    } else {
                        this.newMessage({
                            message: this.i18n("Step with no links, contact SCP support with StepID:" + op.StepID),
                            description: "Step with no links, contact SCP support with StepID:" + op.StepID,
                            type: "Error",
                        });
                    }
                }
                resolve();
            });
        },
        getSections: function (data) {
            return new Promise(async (resolve, reject) => {
                let start = data.ScriptSteps.find(e => e.StepType === "3");
                let start_hierarchy = data.StepsHierarchy.filter(e => e.ParentStepID === start.StepID);
                let sections = [];
                let prefill = this.oArgs.type === this.nRoutes.prefill || this.oArgs.type === this.nRoutes.run;
                let dig = (i, init) => {
                    let hierarchy = data.StepsHierarchy.filter(e => e.ParentStepID === i.StepID);
                    for (let h of hierarchy) {
                        let next = data.ScriptSteps.find(e => e.StepID === h.ChildStepID);
                        if (next) {
                            if (prefill) {
                                let route = data.ScriptStepParameters.filter(e => e.ParameterType === "RouteParam" && e.ParameterValue === next.StepID);
                                if (next.StepType === "19" && route.length < 1) continue;
                            }
                            let duplicate = sections.find(e => e.StepID === next.StepID);
                            if (next.StepType === "19" && !duplicate) {
                                if (init) this.temp.route.push(next.StepID);
                                sections.push(next);
                            }
                            dig(next);
                        }
                    }
                }
                if (start && start_hierarchy.length > 0) {
                    dig(start, true);
                    resolve(sections);
                } else {
                    reject(this.i18n("ERROR_HIERARCHY01"))
                }
            })
        },
        /**
        * Initialise the sap.m.WizardStep\s. 
        * @param {sap.ui.mode.json.JSONModel} oModel 
        * @param {sap.m.Wizard} target 
        */
        getSteps: function (oModel) {
            return new Promise(async (resolve, reject) => {
                let aStep = [];
                let data = oModel.getData();
                let sections = await this.getSections(data).catch(error => this.fatal(error));
                let prefill = this.oArgs.type === this.nRoutes.prefill || this.oArgs.type === this.nRoutes.run;
                for (var i = 0; i < sections.length; i++) {
                    let section = sections[i];
                    let sectionId = section.StepID;
                    let valid = data.StepsHierarchy.find(e => e.ParentStepID === sectionId);
                    if (valid) {
                        let id = `ID_${sectionId}`;
                        if (this.byId(id)) {
                            try {
                                this.byId(id).destroy();
                            } catch (error) {
                                console.error(error);
                            }
                        }
                        var parameter = {
                            id: this.createId(id),
                            sectionId: sectionId,
                            validated: true,
                            title: section.DescriptionShort,
                            complete: async ($event) => {
                                await this.onComplete($event).catch(error => {
                                    this.newMessage({
                                        message: this.i18n("NO_MATCH"),
                                        description: error,
                                        type: "Error",
                                    });
                                });
                            },
                            activate: (oEvent) => this.onActivate(oEvent, sectionId)
                        }
                        if (prefill) {
                            let nextStep = sections[i + 1];
                            if (nextStep) parameter.nextStep = this.createId(`ID_${nextStep.StepID}`);
                        } else {
                            parameter.subsequentSteps = await this.getSubsequent(oModel, sectionId, sections).catch(error => {
                                this.fatal(this.i18n("FATAL"), error)
                            });
                        }
                        var oWizardStep = await this.getWizardStep(oModel, parameter).catch(error => this.fatal(this.i18n("FATAL"), error));
                        aStep.push(oWizardStep);
                    }
                }
                resolve(aStep);
            });
        },
    });
});