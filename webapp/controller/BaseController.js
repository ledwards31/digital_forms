sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "sap/m/MessageBox",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/message/Message",
    "sap/ui/core/MessageType",
    "sap/ui/core/ValueState",
    "sap/ui/core/Fragment",
    "com/ven/iris/FormRenderer/common/IndexedDB",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/ButtonType",
    "sap/ui/core/HTML",
    "sap/m/MessageToast",
    "com/ven/iris/FormRenderer/common/dependencies/qr/quagga.min",
    "com/ven/iris/FormRenderer/common/dependencies/qr/zxing",
    "com/ven/iris/FormRenderer/common/dependencies/uuid/uuidv4.min",
    "com/ven/iris/FormRenderer/common/fragment.definition",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (
    Controller,
    History,
    UIComponent,
    MessageBox,
    JSONModel,
    Message,
    MessageType,
    ValueState,
    Fragment,
    IndexedDB,
    Dialog,
    Button,
    ButtonType,
    HTML,
    MessageToast,
    _1D,
    _2D,
    UUID,
    FragmentDefinition,
    Filter,
    FilterOperator
) {
    "use strict";
    return Controller.extend("com.ven.iris.FormRenderer.controller.BaseController", {
        files: new Map(),
        pActions: [],
        queue: [],
        temp: {
            formId: null,
            runId: null,
            route: []
        },
        nModels: Object.freeze({
            server: "results-read",
            downloads: "downloads",
            pending: "pending"
        }),
        nRoutes: Object.freeze({
            new: "new",
            run: "run",
            local: "local",
            prefill: "prefill"
        }),
        nOps: Object.freeze({
            bizRule: "10",
            action: "5",
        }),
        controls: Object.freeze({
            Heading: "20",
            Body: "21",
            Editor: "99",
            XLine: "34",
            Spacer: "35",
            Input: "22",
            TextAre: "23",
            Email: "24",
            Number: "25",
            Content: "26",
            Radio: "27",
            CheckBox: "28",
            ComboBox: "29",
            Table: "30",
            DatePicker: "31",
            TimePicker: "32",
            Signature: "33",
            Upload: "36",
            QR: "37",
            MultiInput: "38",
            Location: "39",
            Asset: "40",
            Contact: "41",
        }),
        input: Object.freeze({
            Radio: "radio",
            ComboBox: "dropdown",
            CheckBox: "checkbox",
            Input: "input"
        }),
        docuri: "/sap/opu/odata/sap/ZKMDOCUMENTS_SRV/Documents",
        onInit: function () {
            let oView = this.getView();
            var oRouter = this.getOwnerComponent().getRouter();
            oRouter.attachRouteMatched(this.onRouteMatched, this);
            this.setup(oView);
            let methods = {
                popBizRule: this.popBizRule,
                popAction: this.popAction.bind(this)
            }
            this.fOps = new Map([
                ["10", methods.popBizRule],
                ["5", methods.popAction]
            ]);
        },
        onExit: function () {
            if (this._onExit) this._onExit(oEvent);
        },
        setup: function (oView) {
            let oMessageManager = sap.ui.getCore().getMessageManager();
            let page = new JSONModel({
                title: this.i18n("IRIS_FORM")
            });
            oView.setModel(oMessageManager.getMessageModel(), "message");
            oMessageManager.registerObject(oView, true);
            oView.setBusyIndicatorDelay(0);
            this.database = new IndexedDB();
            this.setModel(page, "page");
        },
        onRouteMatched: function (oEvent) {
            if (this._onInit) this._onInit(oEvent);
        },
        getModel: function (model) {
            if (model) {
                return this.getView().getModel(model);
            } else {
                return this.getView().getModel();
            }
        },
        setTitle: function (title) {
            let oModel = this.getModel("page");
            oModel.setProperty("/title", title);
        },
        setModel: function (value, model) {
            return this.getView().setModel(value, model);
        },
        setDefaultModel(model) {
            return this.getView().setModel(model);
        },
        getBindingContext: function (oControl) {
            return oControl.getBindingContext();
        },
        deleteModel(model) {
            this.setModel(null, model);
        },
        setBusy: function (bool) {
            this.getView().setBusy(bool);
        },
        setBusyById: function (id, bool) {
            this.byId(id).setBusy(bool);
        },
        geti18n: function (query) {
            return this.getResourceBundle().getText(query);
        },
        i18n: function (query) {
            return this.getResourceBundle().getText(query);
        },
        fatal: function (error, stacktrace) {
            return new Promise(resolve => {
                if (!this.byId("fatal")) {
                    MessageBox.show(
                        `${error.message ? error.message : error}`, {
                        id: this.createId("fatal"),
                        icon: MessageBox.Icon.ERROR,
                        title: error.name ? error.name : this.geti18n("ERR"),
                        actions: [MessageBox.Action.OK],
                        emphasizedAction: MessageBox.Action.OK,
                        onClose: (oAction) => {
                            resolve(oAction);
                        }
                    }
                    )
                }
                //eslint-disable-next-line
                console.error(error);
                if (stacktrace) console.error(stacktrace);
            })
        },
        setValueState: function (control, state, text) {
            let array = Array.isArray(control);
            function set(oControl, state, text) {
                if (oControl.setValueState && oControl.setValueStateText) {
                    oControl.setValueState(state);
                    oControl.setValueStateText(text);
                }
            }
            if (array) {
                control.map(e => set(e, state, text));
            } else {
                set(control, state, text);
            }
        },
        onRadio: function (oEvent, path) {
            let oModel = this.getModel("form");
            let oSource = oEvent.getSource();
            let selectedButton = oSource.getSelectedButton();
            let data = selectedButton.data("value");
            let text = selectedButton.getText();
            let value = data ? data : text;
            oModel.setProperty(path, value);
        },
        onSign: function (oEvent, stepId) {
            let oModel = this.getModel("form");
            let value = oEvent.getParameters().image;
            let fieldParameter = this.getParameterIndex(stepId, "FieldParameter");
            if (fieldParameter > -1) {
                oModel.setProperty(`/${stepId}/parameters/${fieldParameter}/Notes`, value);
            } else {
                oModel.setProperty(`/${stepId}/parameters/${fieldParameter}`, {
                    ParameterType: "FieldParameter",
                    ParameterID: stepId,
                    ParameterValue: "",
                    TargetOrder: null,
                    TargetParameter: null,
                    Version: 2,
                    Entity: "",
                    Notes: value
                });
            }
        },
        question: function (data) {
            if (!this.byId("question")) {
                MessageBox.show(
                    `${data.message}`, {
                    id: this.createId("question"),
                    icon: MessageBox.Icon.QUESTION,
                    title: data.title,
                    actions: data.message,
                    emphasizedAction: data.emphasizedAction,
                }
                )
            }
            //eslint-disable-next-line
            console.error(error)
        },
        lazyOData: function (url) {
            return new Promise((resolve, reject) => {
                fetch(url).then(response => {
                    return response.json();
                }).then(data => {
                    let name = uuidv4();
                    let model = new JSONModel(data.value);
                    this.setModel(model, name);
                    resolve(name);
                }).catch((error) => {
                    reject(error);
                });
            })
        },
        newFile: function (id, config) {
            this.files.set(id, config);
        },
        delFiles: function () {
            this.files.clear();
        },
        putDMS: function (id, WON) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("results-write");
                let sPath = `/DMSUpload(...)`;
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                oOperation.setParameter("id", id);
                oOperation.setParameter("WON", WON);
                oOperation.execute().then(() => {
                    let response = oOperation.getBoundContext().getObject();
                    resolve(response.value);
                }).catch((error) => {
                    this.newMessage({
                        message: this.i18n("OPA_ERR"),
                        description: error,
                        type: "Error",
                    });
                    reject(error);
                });
            })
        },
        putFile: function (runId) {
            return new Promise((resolve, reject) => {
                for (const [k, v] of this.files.entries()) {
                    let source = this.byId(v.controlId);
                    if (source && source.getValue()) {
                        let file = source.oFileUpload.files[0];
                        let payload = {
                            DocumentVersion: "1",
                            DocumentType: "T99",
                            DocumentNumber: "1",
                            Description: v.description,
                            FileName: file.name,
                            MimeType: file.type,
                            IRIS_RUNID: runId
                        }
                        fetch(`${source.getUploadUrl()}`, {
                            method: "POST",
                            body: JSON.stringify(payload),
                            headers: {
                                "Content-Type": "application/json"
                            }
                        }).then(response => {
                            if (!response.ok) {
                                reject(new Error(this.i18n("UL_FAIL_DESCRIPTION")))
                            } else {
                                return response.json();
                            }
                        }).then(response => {
                            this.UL_URL = source.getUploadUrl();
                            source.setUploadUrl(`${this.UL_URL}(${response.ID})/FileContent`);
                            source.setSendXHR(true);
                            source.upload();
                        }).catch(error => {
                            console.error(error);
                            reject(error);
                        }).finally(() => {
                            source.setUploadUrl(this.UL_URL);
                        });
                    }
                }
                resolve();
            })
            //let files = oEvent.getParameter('files');
            //this.files.set(stepId, files);
        },
        getRaw: function (runId) {
            return new Promise((resolve, reject) => {
                let files = [];
                for (const [k, v] of this.files.entries()) {
                    let source = this.byId(v.controlId);
                    if (source.getValue()) {
                        let file = source.oFileUpload.files[0];
                        let doc = {
                            id: uuidv4(),
                            runId: runId,
                            file: file,
                            payload: {
                                DocumentVersion: "1",
                                DocumentType: "T99",
                                DocumentNumber: "1",
                                Description: v.description,
                                FileName: file.name,
                                MimeType: file.type,
                                IRIS_RUNID: runId
                            },
                            url: source.getUploadUrl()
                        }
                        files.push(doc);
                    }
                }
                resolve(files);
            })
        },
        onMessagePopoverPress: function (oEvent) {
            let oSource = oEvent.getSource();
            if (!this._Messages) {
                FragmentDefinition.getMessage(this).then(function (oMessageManager) {
                    this._Messages = oMessageManager;
                    this.getView().addDependent(this._Messages);
                    this._Messages.openBy(oSource);
                }.bind(this));
            } else {
                return this._Messages.openBy(oSource);
            }
        },
        onClearPress: function () {
            sap.ui.getCore().getMessageManager().removeAllMessages();
        },
        newMessage: function (data) {
            var oMessage = new Message(data);
            sap.ui.getCore().getMessageManager().addMessages(oMessage);
        },
        getQRCode: function (oEvent) {
            console.log(oEvent);
        },
        getQuaggaConfig: function () {
            let target = document.getElementById("idQuagga");
            return {
                inputStream: {
                    type: "LiveStream",
                    constraints: {
                        width: {
                            min: 640
                        },
                        height: {
                            min: 480
                        },
                        facingMode: "environment",
                        aspectRatio: {
                            min: 1,
                            max: 2
                        }
                    },
                    target: target
                },
                locator: {
                    patchSize: "medium",
                    halfSample: true
                },
                numOfWorkers: 2,
                frequency: 10,
                decoder: {
                    readers: ["code_128_reader", "code_39_reader", "code_39_vin_reader"],
                    multiple: false,
                    debug: {
                        drawBoundingBox: true,
                        showFrequency: true,
                        drawScanline: true,
                        showPattern: true
                    }
                },
                locate: true
            };
        },
        onProcessed: function (e) {
            var context = Quagga.canvas.ctx.overlay, canvas = Quagga.canvas.dom.overlay;
            if (e) {
                if (e.boxes) {
                    context.clearRect(0, 0, parseInt(canvas.getAttribute("width")), parseInt(canvas.getAttribute("height")));
                    e.boxes.filter((box) => {
                        return box !== e.box;
                    }).forEach((box) => {
                        Quagga.ImageDebug.drawPath(
                            box,
                            {
                                x: 0,
                                y: 1
                            },
                            context,
                            {
                                color: "#F2F2F2",
                                lineWidth: 2
                            });
                    });
                }
                if (e.box) {
                    Quagga.ImageDebug.drawPath(
                        e.box,
                        {
                            x: 0,
                            y: 1
                        },
                        context,
                        {
                            color: "#00A651",
                            lineWidth: 2
                        }
                    );
                }
                if (e.codeResult && e.codeResult.code) {
                    Quagga.ImageDebug.drawPath(e.line,
                        {
                            x: 'x',
                            y: 'y'
                        },
                        context,
                        {
                            color: '#00A651',
                            lineWidth: 2
                        }
                    );
                }
            }
        },
        popAttMeta: function (url, runId) {
            let parameters = [];
            if (url === this.docuri && runId) {
                let params = ["order", "operation", "equipment"];
                parameters.push({
                    TargetParameter: "ID",
                    ParameterValue: runId
                });
                for (let param of params) {
                    if (this.urlq[param]) {
                        parameters.push({
                            TargetParameter: param,
                            ParameterValue: this.urlq[param]
                        })
                    }
                }
            }
            return parameters;
        },
        popParameters(oParameters) {
            let res = [];
            if (oParameters) {
                let keys = Object.keys(oParameters);
                for (let name of keys) {
                    res.push({
                        k: name,
                        v: oParameters[name]
                    });
                }
            }
            return res;
        },
        fireRequest() {
            return new Promise((resolve, reject) => {
                this.database.ReadAll("__Request").then(data => {
                    let promises = [];
                    data.forEach(e => {
                        let oModel = this.getModel(e.sModel);
                        let oBindingContext = oModel.createBindingContext("/");
                        let oOperation = oModel.bindContext(e.sPath, oBindingContext);
                        if (e.oParameters) {
                            if (e.oParameters.id) delete e.oParameters.id;
                            this.popParameters(e.oParameters).forEach(e => oOperation.setParameter(e.k, e.v));
                        }
                        let promise = oOperation.execute();
                        promises.push(promise);
                    });
                    Promise.allSettled(promises).then(response => {
                        for (var i = 0; i < response.length; i++) {
                            let promise = response[i];
                            if (promise.status === "rejected") {
                                this.newMessage({
                                    message: this.i18n("ACTION_ERROR"),
                                    description: promise.reason,
                                    type: "Error",
                                });
                                console.error(promise);
                            }
                            this.database.Delete("__Request", data[i].id);
                        }
                        resolve(response)
                    });
                }).catch(error => {
                    reject(error);
                });
            })
        },
        popReuqest: function (sModel, sPath, oParameters, oFilter, sMsg) {
            return new Promise((resolve, reject) => {
                this.database.Create("__Request", {
                    id: uuidv4(),
                    sModel: sModel,
                    sPath: sPath,
                    oParameters: oParameters,
                    oFilter: oFilter,
                    sDescriptor: this.i18n("REQ_DESCRIPTOR"),
                    sDate: new Date().toLocaleString('en-AU')
                }).then(() => {
                    this.newMessage({
                        message: this.i18n("REQ_SCH"),
                        description: sMsg || this.i18n("REQ_DESC"),
                        type: "Success",
                    });
                    resolve();
                }).catch((error) => {
                    this.newMessage({
                        message: this.i18n("REQ_ERR"),
                        description: error,
                        type: "Error",
                    });
                    reject(error);
                });
            })
        },
        popAction: function (url, many) {
            return new Promise((resolve, reject) => {
                let sPath = `/executeGatewayService(...)`;
                let sModel = "results-write";
                let runId = this.temp.runId;
                let scriptId = this.temp.formId;
                let tempArr = this.popAttMeta(url, this.temp.runId);
                many = many.concat(tempArr);
                let queue = (sModel, sPath) => {
                    this.queue.push((runId, scriptId) => {
                        // many.concat(this.popAttMeta(url, runId));
                        let oParameters = {
                            odataUrl: url,
                            params: many
                        }
                        let sMsg = `Digital Forms queued a delayed request by ${scriptId}. Request is a member of the response ${runId}.`
                        this.popReuqest(sModel, sPath, oParameters, [], sMsg);
                    });
                }
                if (navigator.onLine) {
                    // many.concat(this.popAttMeta(url, runId));
                    let oModel = this.getModel(sModel);
                    let oBindingContext = oModel.createBindingContext("/");
                    let oOperation = oModel.bindContext(sPath, oBindingContext);
                    oOperation.setParameter("odataUrl", url);
                    oOperation.setParameter("params", many);
                    oOperation.execute().then(() => {
                        let response = oOperation.getBoundContext().getObject();
                        let jsonResponse = JSON.parse(response.value);
                        console.log("jsonResponse.responseText:" + jsonResponse.responseText);
                        this.newMessage({
                            message: this.i18n("OPA_OK"),
                            description: jsonResponse.responseText,
                            type: "Success",
                        });
                        resolve(response.value);
                    }).catch((error) => {
                        if (error.message && error.message.includes("401")) {
                            queue(sModel, sPath);
                            resolve();
                        } else {
                            this.newMessage({
                                message: this.i18n("OPA_ERR"),
                                description: error,
                                type: "Error",
                            });
                            reject(error);
                        }
                    });
                } else {
                    queue(sModel, sPath);
                    resolve();
                }
            })
        },
        /*  REFACTOR REQUIRED:
            Code block is way too large. Componentize... */
        popBizRule: function (url, many) {
            return new Promise((resolve, reject) => {
                let oModel, sModel, sPath, oParameters = {};
                if (["GW","HADM"].some(e => url.includes(e))) {
                    sModel = "script-read";
                    sPath = url.includes("HADM") ? "readFromHADM(...)" : "readFromGateway(...)"
                    oModel = this.getModel(sModel);
                } else {
                    let oExpression = /([A-Za-z0-9_\-\/]*)(\/[A-Za-z0-9_\-]*\(\))/g;
                    let aMatches = oExpression.exec(url);
                    if (!aMatches || aMatches.length < 2) return reject(`Digital Forms was unable to resolve the service ${url}`);
                    sModel = aMatches[1];
                    sPath = aMatches[2];
                    oModel = new sap.ui.model.odata.v4.ODataModel({
                        serviceUrl: `${sModel}/`,
                        synchronizationMode: "None",
                        operationMode: sap.ui.model.odata.OperationMode.Server,
                        groupId: "$direct"
                    });
                    oParameters = {
                        $filter: "",
                    };
                    let length = many.length;
                    for (var i = 0; i < length; i++) {
                        let item = many[i];
                        oParameters.$filter += `${item.TargetParameter} eq '${item.ParameterValue}'`;
                        if (length > 1 && i !== length - 1) oParameters.$filter += " and ";
                    }
                }
                if (!sModel || !sPath) {
                    return reject("Digital Forms was unable to bind to the service or locate the method as a member of the service.");
                } else {
                    if (sPath.includes("()")) sPath = sPath.replace("()", "(...)");
                }
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext, oParameters);
                if (["GW","HADM"].some(e => url.includes(e))) {
                    oOperation.setParameter("url", url);
                    oOperation.setParameter("params", many);
                }
                if (sPath.includes("getMyOrdersSAP")) {
                    let column = [];
                    let value = [];
                    many.forEach(item => column.push(item.TargetParameter) && value.push(item.ParameterValue));
                    this.database.ReadSome("OrdersSAP", column, value).then(data => {
                        if (data && data.length > 0) return resolve(data);
                    }).catch(error => {
                        reject(error);
                    })
                }
                if (navigator.onLine) {
                    oOperation.execute().then(() => {
                        let response = oOperation.getBoundContext().getObject();
                        this.newMessage({
                            message: this.i18n("OPB_OK"),
                            description: JSON.stringify(response),
                            type: "Success",
                        });
                        resolve(response.value);
                    }).catch((error) => {
                        this.newMessage({
                            message: this.i18n("OPB_ERR"),
                            description: error,
                            type: "Error",
                        });
                        reject(error);
                    });
                } else {
                    resolve("Device has no Internet.")
                }
            })
        },
        onDetected: function (e, input) {
            let mean;
            let decodedCodes = e.codeResult.decodedCodes;
            for (let decodedCode of decodedCodes) {
                mean ? mean += decodedCode.error : mean = decodedCode.error;
            }
            mean = mean / decodedCodes.length;
            if (mean < 0.1) {
                input.setValue(e.codeResult.code);
                this.oDialog_QR1.close();
                Quagga.stop();
                this.oZX_I1.reset();
            }
        },
        popQuagga: function (input) {
            Quagga.init(this.getQuaggaConfig(), (error) => {
                if (error) {
                    console.error(error);
                    return
                }
                Quagga.start();
            });
            Quagga.onProcessed(this.onProcessed.bind(this));
            Quagga.onDetected((e) => {
                this.onDetected(e, input);
            });
        },
        popZXing: function (input) {
            let target = document.querySelector("#idQuagga video");
            this.oZX_I1 = new ZXing.BrowserQRCodeReader();
            this.oZX_I1.decodeOnceFromVideoDevice(undefined, target).then(data => {
                input.setValue(data);
                this.oDialog_QR1.close();
                Quagga.stop();
                this.oZX_I1.reset();
            }).catch(error => {
                console.error(error);
            })
        },
        popQR: function (input) {
            if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
                this.popQuagga(input);
                this.popZXing(input);
            } else {
                MessageToast.show(this.i18n("QR_NO_SUPPORT"))
            }
        },
        getQRDialog: function (oEvent, field) {
            let target = field ? field : oEvent.getSource();
            if (!this.oDialog_QR1) {
                this.oDialog_QR1 = new Dialog({
                    title: this.i18n("QR_DIALOG_INSTRUCTION"),
                    content: new HTML({
                        content: `<div id="idQuagga"></div>`
                    }),
                    endButton: new Button({
                        text: this.i18n("Close"),
                        press: () => {
                            this.oDialog_QR1.close();
                            Quagga.stop();
                            this.oZX_I1.reset();
                        }
                    })
                }).addStyleClass("dxcUiDialog");
                this.getView().addDependent(this.oDialog_QR1);
            }
            this.oDialog_QR1.open();
            this.popQR(target);
        },
        getScript: function (id) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("script-read");
                let sPath = "/getWholeActiveScript(...)";
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                oOperation.setParameter("scriptId", id);
                oOperation.execute().then(() => {
                    let response = oOperation.getBoundContext().getObject();
                    resolve(response);
                }).catch((error) => {
                    this.fatal(error);
                    reject(error);
                });
            })
        },
        getRoute: function (model, value) {
            if (model === this.nModels.server) {
                return value.RunID ? this.nRoutes.run : this.nRoutes.new;
            } else if (model === this.nModels.downloads) {
                return this.nRoutes.local;
            } else {
                return this.nRoutes.new;
            }
        },
        getRun: function (id) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("script-read");
                let sPath = "/getRunResults(...)";
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                oOperation.setParameter("runId", id);
                oOperation.execute().then(() => {
                    let response = oOperation.getBoundContext().getObject();
                    resolve(response);
                }).catch((error) => {
                    this.fatal(error);
                    reject(error);
                });
            })
        },
        getDownload: function (id) {
            return new Promise((resolve, reject) => {
                this.database.Read("Downloads", id).then(data => {
                    resolve(data);
                }).catch(error => {
                    reject(error);
                })
            })
        },
        getPending: function (id) {
            return new Promise((resolve, reject) => {
                this.database.Read("Pending", id).then(data => {
                    let scriptId = data.ScriptID;
                    let description = data.Description;
                    data.formResults.Script = data.metadata.Script;
                    data.formResults.ScriptSteps = data.metadata.ScriptSteps;
                    data.formResults.StepsHierarchy = data.metadata.StepsHierarchy;
                    data.formResults.ScriptStepParameters = data.formResults.scriptResultsMultiParam;
                    data.formResults.ScriptStepFields = data.metadata.ScriptStepFields;
                    resolve(data);
                }).catch(error => {
                    reject(error);
                })
            })
        },
        getRunID: function (state = {}) {
            return new Promise((resolve, reject) => {
                if (window.navigator.onLine) {
                    let oModel = this.getModel("script-read");
                    let sPath = "/getNextRunID(...)";
                    let oBindingContext = oModel.createBindingContext("/");
                    let oOperation = oModel.bindContext(sPath, oBindingContext);
                    oOperation.execute().then(() => {
                        let response = oOperation.getBoundContext().getObject();
                        resolve(response.value.toString());
                    }).catch((error) => {
                        if (error.message && error.message.includes("401")) {
                            state.b401 = true;
                            resolve(`P${uuidv4()}`);
                        } else {
                            this.fatal(error);
                            reject(error);
                        }
                    });
                } else {
                    resolve(`P${uuidv4()}`);
                }
            })
        },
        getParameter: function (stepId, parameter) {
            let oModel = this.getModel("form");
            let data = oModel.getData();
            let value = data[stepId].parameters.find(e => e.ParameterType === parameter);
            return value;
        },
        getParameterIndex: function (stepId) {
            let oModel = this.getModel("form");
            let data = oModel.getData();
            let value = data[stepId].parameters.findIndex(e => e.ParameterType === parameter);
            return value;
        },
        popBinding: function (stepId, position, prefill) {
            return new Promise(resolve => {
                let form = this.getModel("form");
                let template = form.getProperty(`/${stepId}`);
                let index;
                let parameterId = Array.isArray(position) ? `${stepId}_${position[0]}_${position[1]}` : `${stepId}_${position}`;
                let stepType = this.getModel("script").getData().ScriptSteps.find(x => x.StepID === stepId).StepType;
                let that = this;
                function push(template) {
                    if (stepType === '28') {
                        //Fix for multiple checkboxes
                        if (that.oArgs.type === that.nRoutes.run) {
                            let param = that.getModel("script").getData().ScriptStepParameters.find(x => x.ParameterID === parameterId);
                            if (param && param.ParameterValue) {
                                template.parameters.push({
                                    ParameterValue: true,
                                    ParameterID: parameterId,
                                    ParameterType: "FieldParameter",
                                    TargetParameter: prefill,
                                    TargetOrder: null,
                                    Entity: null,
                                    Version: 2
                                });
                            } else {
                                template.parameters.push({
                                    ParameterValue: false,
                                    ParameterID: parameterId,
                                    ParameterType: "FieldParameter",
                                    TargetParameter: prefill,
                                    TargetOrder: null,
                                    Entity: null,
                                    Version: 2
                                });
                            }
                        } else {
                            template.parameters.push({
                                ParameterValue: false,
                                ParameterID: parameterId,
                                ParameterType: "FieldParameter",
                                TargetParameter: prefill,
                                TargetOrder: null,
                                Entity: null,
                                Version: 2
                            });
                        }
                        console.log("Added param ", template.parameters);
                    } else {
                        template.parameters.push({
                            ParameterValue: prefill ? prefill : "",
                            ParameterID: parameterId,
                            ParameterType: "FieldParameter",
                            TargetParameter: null,
                            TargetOrder: null,
                            Entity: null,
                            Version: 2
                        });
                    }
                    form.setProperty(`/${stepId}`, template);
                }
                if (template) {
                    index = template.parameters.findIndex(e => e.ParameterID === parameterId);
                    if (index === -1) {
                        index = template.parameters.length;
                        push(template);
                    } else {
                        push(template);
                    }
                } else {
                    index = 0;
                    template = {
                        parameters: []
                    }
                    push(template);
                }
                resolve(`/${stepId}/parameters/${index}/ParameterValue`);
            })

        },
        onClose: function (name) {
            this[name].close();
            this[name].destroy();
            this[name] = null;
        },
        //Fix for multiple checkboxes
        fixArrayForCheckboxes: function (data) {
            let params = data.formResults.scriptResultsMultiParam.filter(x => typeof (x.ParameterValue) === 'boolean');
            // let stepFields = this.getModel("script").getData().ScriptStepFields.filter(x => x.StepID === params[0].StepID && x.StepFieldName.indexOf("checkboxTextContent") >= 0);
            params.forEach((param, i) => {
                if (param.ParameterValue) {
                    console.log("Checkbox param (check target):", param);
                    param.ParameterValue = param.TargetParameter;//stepFields[i].StepFieldValue;
                    param.TargetParameter = null;
                    console.log("Checkbox Param after fix:", param);
                } else {
                    param.ParameterValue = null;
                    param.TargetParameter = null;
                }
            });
            return data;
        },
        postScript: function (data, origin) {
            return new Promise((resolve, reject) => {
                let oModel = this.getModel("results-write");
                let sPath = "/saveFormResults(...)";
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                data = this.fixArrayForCheckboxes(data);
                oOperation.setParameter("formResults", data.formResults);
                oOperation.execute().then(() => {
                    this.temp.runId = data.formResults.scriptResultsHeader.RunID;
                    this.temp.formId = data.formResults.scriptResultsHeader.ScriptID;
                    return Promise.all(this.pActions.map(f => f()));
                }).then(() => {
                    this.pActions = [];
                    let response = oOperation.getBoundContext().getObject();
                    resolve(response);
                }).catch((error) => {
                    if (error.message && error.message.includes("401")) {
                        origin.onSave(null, true);
                    } else {
                        this.pActions = [];
                        this.fatal(error);
                        reject(error);
                    }
                });
            })
        },
        onSelect: function (oEvent, path, fgid) {
            if (path.path) {
                let re = /(?:[\w]+>)?([\w\d\_\-\/]+)/g;
                // /(?:[\w]+>)?([\w\d\/]+)/g;
                let matches = re.exec(path.path);
                path = matches[1];
            }
            let bSelected = oEvent.getParameters().selected;
            let model = this.getModel("form");
            let source = oEvent.getSource();
            let data = source.data("value");
            let text = source.getText();
            let value = data ? data : text;
            let controls = sap.ui.getCore().byFieldGroupId(fgid);
            if (bSelected) {
                // controls.forEach(control => {
                //     let type = control.getMetadata().getName();
                //     if (type === "sap.m.CheckBox" && control !== source) {
                //         let selected = this.getSelected(path, value);
                //         control.setSelected(selected);
                //     }
                // });
                let currValue = model.getProperty(path);
                if (!currValue) {
                    currValue = value;
                } else {
                    currValue = currValue + "," + value;
                }
                model.setProperty(path, currValue);
            } else {
                let currValue = model.getProperty(path);
                if (!currValue) {
                    currValue = "";
                }
                currValue = currValue.replaceAll("," + value, "");
                currValue = currValue.replaceAll(value, "");
                if (currValue.indexOf(",") === 0) {
                    currValue = currValue.substr(1);
                }
                model.setProperty(path, currValue);
            }
        },
        getSelected: function (path, text) {
            if (path) {
                let model = this.getModel("form");
                let property = model.getProperty(path);
                return property ? property.indexOf(text) >= 0 : false;
            } else {
                return false;
            }
        },
        /**
         * Convenience method for getting the view model by name in every controller of the application.
         * @public
         * @param {string} sName the model name
         * @returns {sap.ui.model.Model} the model instance
         */
        getModel: function (sName) {
            return this.getView().getModel(sName);
        },
        /**
         * Convenience method for setting the view model in every controller of the application.
         * @public
         * @param {sap.ui.model.Model} oModel the model instance
         * @param {string} sName the model name
         * @returns {sap.ui.mvc.View} the view instance
         */
        setModel: function (oModel, sName) {
            return this.getView().setModel(oModel, sName);
        },
        /**
         * Convenience method for getting the resource bundle.
         * @public
         * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
         */
        getResourceBundle: function () {
            return this.getOwnerComponent().getModel("i18n").getResourceBundle();
        },
        /**
         * Method for navigation to specific view
         * @public
         * @param {string} psTarget Parameter containing the string for the target navigation
         * @param {mapping} pmParameters? Parameters for navigation
         * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
         */
        navTo: function (psTarget, pmParameters, pbReplace) {
            this.getRouter().navTo(psTarget, pmParameters, pbReplace);
        },

        getRouter: function () {
            return UIComponent.getRouterFor(this);
        },

        onNavBack: function () {
            var sPreviousHash = History.getInstance().getPreviousHash();
            if (sPreviousHash !== undefined) {
                window.history.back();
            } else {
                this.getRouter().navTo("appHome", {}, true /*no history*/);
            }
        },

        getParent: function (oEvent) {
            return oEvent.getSource().getParent();
        },

        refresh: function (model) {
            var oModel = this.getModel(model);
            oModel.refresh();
        },

        getParameterValue: function (pId) {
            let formModel = this.getModel("form");
            var keys = Object.keys(formModel.getData());
            let retValue = null;
            keys.forEach(stepId => {
                var step = formModel.getData()[stepId];
                step.parameters.forEach(param => {
                    if (param.ParameterID === pId && param.ParameterValue) {
                        console.log("Found");
                        retValue = param.ParameterValue;
                    }
                })
            });
            return retValue;
        },
        setParameterValue: function (pId, value) {
            let formModel = this.getModel("form");
            var keys = Object.keys(formModel.getData());
            keys.forEach(stepId => {
                var step = that.getModel("form").getData()[stepId];
                step.parameters.forEach(param => {
                    if (param.ParameterID === pId) {
                        param.ParameterValue = value;
                    }
                })
            });
        },
    });
});
