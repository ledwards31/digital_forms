/* global draw2d:true */
/* global Connection:true */
/* global InterceptorPolicy:true */
/* global TooltipFigure:true */
sap.ui.define([
    "com/ven/iris/FormRenderer/controller/script.controller",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel",
    "com/ven/iris/FormRenderer/common/ActionClass",
    "com/ven/iris/FormRenderer/common/ControlDictionary",
    "com/ven/iris/FormRenderer/common/Utilities",
    "sap/ui/layout/form/SimpleForm",
    "com/ven/iris/FormRenderer/common/control/dxc/ui/Wizard/WizardStep",
    "sap/ui/model/odata/v4/ODataModel",
], function (Controller, MessageBox, MessageToast, JSONModel, ActionClass, ControlDictionary, Utilities, SimpleForm, WizardStep, ODataModel) {
    "use strict";

    return Controller.extend("com.ven.iris.FormRenderer.controller.pdf", {
        hIconTabBar: function () {
            let element = document.querySelector('.sapUiFixFlexFixed');
            element.style.display = "none";
        },
        /**
         * Initialise the Wizard.
         */
        popWizard: function () {
            let oModel = this.getView().getModel("script");
            this.getTitle(oModel)
            let oContainer = this.byId("idPDF");
            let oWizard = this.byId("ContentWizard");
            oWizard.discardProgress();
            oWizard.destroySteps();
            oWizard.rerender();
            this.hIconTabBar();
            this.getSteps(oModel, oContainer).then(() => {
                this.setBusy(false);
            }).catch(error => {
                this.fatal(error);
            });
        },
        getFields: function (oModel, section) {
            return new Promise((resolve) => {
                //Method to getFields from ScriptStepFields.
                let sectionId = section.StepID;
                let data = oModel.getData();
                let steps = this.getOrder(oModel, sectionId);
                let jobs = []
                for (let step of steps) {
                    jobs.push(this.getControl(oModel, step));
                }
                Promise.all(jobs).then(res => {
                    res = res.filter(e => e).flat();
                    resolve(new SimpleForm({
                        title: section.DescriptionShort,
                        content: res,
                        layout: "ResponsiveGridLayout",
                        labelSpanXL: 4,
                        labelSpanL: 3,
                        labelSpanM: 4,
                        labelSpanS: 12,
                        adjustLabelSpan: false,
                        emptySpanXL: 0,
                        emptySpanL: 4,
                        emptySpanM: 0,
                        emptySpanS: 0,
                        columnsXL: 1,
                        columnsL: 1,
                        columnsM: 1,
                        singleContainerFullSize: false
                    }));
                });
            })
        },
        getSections: function (data) {
            return new Promise(resolve => {
                let sections = [];
                let initial = data.ScriptSteps.find(e => e.StepType === "3");
                function dig(step) {
                    let hierarchy = data.StepsHierarchy.find(e => e.ParentStepID === step.StepID);
                    let child = data.ScriptSteps.find(e => e.StepID === hierarchy.ChildStepID);
                    if (child.StepType !== "4") {
                        if (child.StepType === "19") sections.push(child);
                        dig(child);
                    } else {
                        resolve(sections);
                    }
                }
                dig(initial);
            })
        },
        /**
         * Initialise the sap.m.WizardStep\s. 
         * @param {sap.ui.mode.json.JSONModel} oModel 
         * @param {sap.m.Wizard} target 
         */
        getSteps: function (oModel, target) {
            return new Promise(async (resolve, reject) => {
                let data = oModel.getData();
                let sections = await this.getSections(data).catch(error => this.fatal(this.i18n("FATAL"), error));
                for (let section of sections) {
                    let sectionId = section.StepID;
                    let valid = data.StepsHierarchy.find(e => e.ParentStepID === sectionId);
                    if (valid) {
                        var oSimpleForm = await this.getFields(oModel, section).catch(error => this.fatal(this.i18n("FATAL"), error));
                        target.addItem(oSimpleForm);
                    }
                }
                resolve();
            });
        },

        print: function (oEvt) {
            var myWindow = window.open('', '', 'width=' + window.outerWidth + ',height=' + window.outerHeight + '');
            myWindow.document.write('<html><head><title>' + document.title + '</title>');
            myWindow.document.write('<link rel="stylesheet" href="css/style.css" type="text/css" />')
            myWindow.document.write(document.head.innerHTML);
            myWindow.document.write('</head><body >');
            myWindow.document.write('<h1>' + document.title + '</h1>');
            myWindow.document.write($("#container-Scripting_FormRenderer---pdf--idPage")[0].innerHTML);
            myWindow.document.write('</body></html>');
            myWindow.document.close();
            myWindow.focus();
            setTimeout(function () {
                myWindow.print();
            }, 5000)
        }
    });
});