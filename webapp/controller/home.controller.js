sap.ui.define([
    "com/ven/iris/FormRenderer/controller/BaseController",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel",
    "com/ven/iris/FormRenderer/common/ActionClass",
    "com/ven/iris/FormRenderer/common/ControlDictionary",
    "com/ven/iris/FormRenderer/common/Utilities"
], function (Controller, MessageBox, Toast, JSONModel, ActionClass, ControlDictionary, Utilities) {
    "use strict";

    return Controller.extend("com.ven.iris.FormRenderer.controller.home", {
        _onInit: function () {

        },
        onPressForms: function (oEvent) {
		    this.getRouter().navTo("list");
        },
         onWorkOrder: function (oEvent) {
            Toast.show('No action available yet');
        },
         onEquipment: function (oEvent) {
            Toast.show('No action available yet');
        }                    
	});
});