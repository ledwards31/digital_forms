/* global draw2d:true */
/* global Connection:true */
/* global InterceptorPolicy:true */
/* global TooltipFigure:true */
sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/ven/iris/FormRenderer/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("com.ven.iris.FormRenderer.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			UIComponent.prototype.init.apply(this, arguments);
			//Create the views based on the url/hash.
			this.getRouter().initialize();
			//Set the device model.
			this.setModel(models.createDeviceModel(), "device");
			this.hanaServiceURL = this.getManifestEntry("/sap.app/dataSources/HANA/uri");
			//this.sapServiceURL = this.getManifestEntry("/sap.app/dataSources/SAPGateway/uri");
		}
	});
});