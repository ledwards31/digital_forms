module.exports = {
  "maximumFileSizeToCacheInBytes": 1e+8,
  "globDirectory": "webapp/",
  "globPatterns": [
    "**/*.{xml,css,js,less,properties,html,json,webmanifest,png,ico,svg}"
  ],
  "globIgnores": [
    '**/index.html',
    '**/*-dbg.js',
    'resources/sap/**',
    'resources/sap-*.{xml,css,js,less,properties,html,json,webmanifest,png,ico,svg}',
    'resources/jquery.sap.*.{xml,css,js,less,properties,html,json,webmanifest,png,ico,svg}',
  ],
  "swDest": "webapp/swDest.js",
  "swSrc": "webapp/swSrc.js"
};
